#!/usr/bin/env bash

# Set keyboard backlight brightness. Acceptable values range 0-3 inclusive.

# if passed a number, set brightness directly

if [ "$1" -ge 0 ] && [ "$1" -le 3 ]; then
    echo "$1" | tee '/sys/class/leds/asus::kbd_backlight/brightness'
    exit 0
fi

# if passed 'up' or 'down', make a relative adjustment

brightness=$(cat "/sys/class/leds/asus::kbd_backlight/brightness")

if [ "$1" == 'up' ] && [ "$brightness" -le 3 ]; then
    echo $((brightness + 1)) | tee '/sys/class/leds/asus::kbd_backlight/brightness'
elif [ "$1" == 'down' ] && [ "$brightness" -gt 0 ]; then
    echo $((brightness - 1)) | tee '/sys/class/leds/asus::kbd_backlight/brightness'
fi
