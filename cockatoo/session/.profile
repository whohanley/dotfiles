#!/usr/bin/env sh

# ~/.profile: executed by the command interpreter for login shells. Bash will
# use ~/.bash_profile instead, but my ~/.bash_profile sources this explicitly.

# Arch problems, see https://www.reddit.com/r/archlinux/comments/u7d23c/visual_studio_code_live_share_broken_all_of/
export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1

# Nix stuff. Note /etc/profile.d/nix.sh sets NIX_PROFILES and modfies PATH
export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}
#export NIX_PATH=$HOME/.nix-defexpr/channels${NIX_PATH:+:}$NIX_PATH # nixpkgs unstable
#export NIX_PATH="nixpkgs=$HOME/code/nixpkgs" # local nixpkgs
export NIX_REMOTE=daemon
# include home-manager variables
. "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"

# PLs configuration

export JAVA_HOME="/lib/jvm/default"
export JDK_HOME="/lib/jvm/default"
# Java doesn't like i3
export _JAVA_AWT_WM_NONREPARENTING=1
# Java doesn't anti-alias text by default, which produces god awful ugly text. These options sort of fix it
export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dsun.java2d.xrender=true"

export GOPATH="$HOME/opt/go"
export PYTHONPATH="$PYTHONPATH:$HOME/lib/python"

[ -f /opt/miniconda3/etc/profile.d/conda.sh ] && . /opt/miniconda3/etc/profile.d/conda.sh

# Karma (?) needs this to use headless Chromium as JS test runner
export CHROME_BIN=chromium

# LaTeX stuff
export PATH="/usr/local/texlive/2023/bin/x86_64-linux:$PATH"
export MANPATH="/usr/local/texlive/2023/texmf-dist/doc/man:$MANPATH"
export INFOPATH="/usr/local/texlive/2023/texmf-dist/doc/info:$MANPATH"
export TEXINPUTS="$HOME/.config/texmf:$TEXINPUTS"

# gpg-agent boilerplate, see https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html
GPG_TTY=$(tty)
export GPG_TTY

export QT_QPA_PLATFORMTHEME='gtk2'

export RIPGREP_CONFIG_PATH="$HOME/.config/ripgrep/config"

export TERMINAL=kitty
export EDITOR=nano

# $GOPATH/bin contains oauth2l, so it goes at the front.
#
# Note /etc/profile.d/nix.sh puts Nix bins at the front of PATH. AFAIK
# ~/.profile should execute after /etc/profile.d/*, but that doesn't seem to be
# the case here. cf
# ~/.config/environment.d/20-home-bin-precedence.conf
export PATH="$GOPATH/bin:$PATH"
export PATH="$HOME/bin:$PATH"
export PATH="$HOME/scripts:$PATH"
export PATH="$HOME/.local/bin:$PATH"
export PATH="$HOME/Applications/bin:$PATH"
export PATH="$HOME/opt/node/node_modules/.bin:$PATH"
export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="$HOME/.dotnet/tools:$PATH"
export PATH="/usr/local/heroku/bin:$PATH"

# Make environment variables (particularly Nix paths) available to systemd user units
systemctl --user import-environment GPG_TTY PATH MSMTP_LOG MSMTP_QUEUE NMBGIT NOTMUCH_CONFIG

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
