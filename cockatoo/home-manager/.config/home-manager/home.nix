# inputs: with inputs; homeDirectory: { config, pkgs, ... }:

{ config, pkgs, ... }:

let
  homeDir = "/home/wohanley";
  scripts = "${homeDir}/scripts";
  # oauth2-lib = {
  #   url = "github:robn/sasl2-oauth";
  #   flake = false;
  # };
  # oauth2Lib = with pkgs; stdenv.mkDerivation {
  #   name = "sasl2-oauth";
  #   src = oauth2-lib;
  #   nativeBuildInputs = [
  #     autoreconfHook
  #     nixpkgs.legacyPackages."${system}".cyrus_sasl
  #   ];
  # };
in
{
  # nixpkgs.overlays = [
  #   (
  #     final: prev:
  #     {
  #       cyrus_sasl = prev.cyrus_sasl.overrideAttrs (div: rec {
  #         postInstall = ''
  #             for lib in ${oauth2Lib}/lib/sasl2/*; do
  #               ln -sf $lib $out/lib/sasl2/
  #             done
  #           '';
  #       });
  #     }
  #   )
  # ];

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "wohanley";
  home.homeDirectory = "${homeDir}";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";

  targets.genericLinux.enable = true;

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # see https://github.com/NixOS/nixpkgs/issues/88067#issuecomment-1592592840
  nixpkgs.config = {
    packageOverrides = pkgs: with pkgs; {
      biber = (import (builtins.fetchTarball {
        url = "https://github.com/NixOS/nixpkgs/archive/40f79f003b6377bd2f4ed4027dde1f8f922995dd.tar.gz";
        sha256 = "1javsbaxf04fjygyp5b9c9hb9dkh5gb4m4h9gf9gvqlanlnms4n5";
      }) {}).biber;
    };
  };

  home.packages = [
    pkgs.atuin
    pkgs.biber # overridden to 2.17 for tectonic compatibility
    pkgs.bogofilter
    pkgs.clj-kondo
    pkgs.emacs-all-the-icons-fonts
    pkgs.entr
    pkgs.fd
    pkgs.feh
    pkgs.fzf
    pkgs.glibcLocales # for LOCALE_ARCHIVE
    pkgs.go
    pkgs.htop
    pkgs.i3
    pkgs.i3blocks
    pkgs.inklecate
    pkgs.jq
    pkgs.lyx
    pkgs.magic-wormhole
    pkgs.mpack
    pkgs.ncdu
    pkgs.nix-index
    pkgs.numlockx
    pkgs.ocrmypdf
    pkgs.pandoc
    pkgs.parallel
    pkgs.pass
    pkgs.pdfarranger
    pkgs.pdfsandwich
    pkgs.pdftk
    pkgs.playerctl
    pkgs.proselint
    pkgs.recoll
    pkgs.ripgrep
    pkgs.rofi
    pkgs.ruby
    pkgs.shellcheck
    pkgs.silver-searcher
    pkgs.soupault
    pkgs.sysstat
    pkgs.tesseract
    pkgs.trashy
    pkgs.typst
    pkgs.unzip
    pkgs.vmtouch
    pkgs.volctl
    pkgs.xcalib
    pkgs.xidlehook
    pkgs.xbindkeys
    pkgs.xsettingsd
    pkgs.xss-lock
    pkgs.zip
  ];

  # https://github.com/NixOS/nix/issues/599
  # https://github.com/rycee/home-manager/issues/354
  home.sessionVariables.LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";

  home.file.parallel-will-cite = {
    text = "";
    target = ".parallel/will-cite";
  };

  programs.emacs = {
    enable = false;
    package = pkgs.emacs29;
    extraPackages = (epkgs: [ epkgs.sqlite3 ]);
  };

  programs.git = {
    enable = true;
    userName = "William O'Hanley";
    userEmail = "me@wohanley.com";
    delta.enable = true;
    lfs.enable = true;
    extraConfig = {
      core.excludesfile = "${homeDir}/.config/git/ignore";
      core.quotepath = "off";
      delta = {
        line-numbers = false; # Work around https://github.com/dandavison/magit-delta/issues/13
        hunk-header-style = "line-number syntax";
        side-by-side = true;
        file-style = "yellow";
      };
      diff.algorithm = "histogram";
      diff.colorMoved = "default";
      rerere.enabled = true;
    };
  };

  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;
    pinentryPackage = pkgs.pinentry-gtk2;
    grabKeyboardAndMouse = false; # interferes with password manager
    defaultCacheTtl = 86400;
    maxCacheTtl = 86400;
    defaultCacheTtlSsh = 86400;
    maxCacheTtlSsh = 86400;
  };

  services.syncthing.enable = true;

  ###
  # Email
  ###

  accounts.email = {
    maildirBasePath = "/home/wohanley/.mail";
    accounts.me = {
      address = "me@wohanley.com";
      primary = true;
      aliases = [ "william@wohanley.com" ];
      userName = "me@wohanley.com";
      passwordCommand = "${pkgs.pass}/bin/pass email/me";
      realName = "William O'Hanley";
      imap.host = "mail.wohanley.com";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "Drafts" "Junk" "Sent" ];
        onNotify = "${scripts}/mail/on-new-mail.sh me";
      };
      smtp = {
        host = "mail.wohanley.com";
        port = 587;
        tls.useStartTls = true;
      };
      msmtp.enable = true;
      notmuch.enable = true;
    };
    accounts.gmail-william = {
      address = "william.ohanley@gmail.com";
      userName = "william.ohanley@gmail.com";
      passwordCommand = "${scripts}/mail/auth-gmail-william.sh";
      realName = "William O'Hanley";
      flavor = "gmail.com";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
        extraConfig.account.authMechs = "XOAUTH2";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "[Gmail]/All Mail" "[Gmail]/Sent Mail" "[Gmail]/Spam" ];
        onNotify = "${scripts}/mail/on-new-mail.sh gmail-william";
        extraConfig.xoauth2 = true;
      };
      msmtp = {
        enable = true;
        extraConfig.auth = "oauthbearer";
      };
    };
    accounts.gmail = {
      address = "willy.ohanley@gmail.com";
      userName = "willy.ohanley@gmail.com";
      passwordCommand = "${scripts}/mail/auth-gmail.sh";
      realName = "William O'Hanley";
      flavor = "gmail.com";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
        extraConfig.account.authMechs = "XOAUTH2";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "Junk" "Sent" "Willy.OHanley@dal.ca" "[Gmail]/All Mail" "[Gmail]/Sent Mail" "[Gmail]/Spam" ];
        onNotify = "${scripts}/mail/on-new-mail.sh gmail";
        extraConfig.xoauth2 = true;
      };
      msmtp = {
        enable = true;
        extraConfig.auth = "oauthbearer";
      };
    };
  };

  # I need XOAUTH2 to read Gmail. mbsync needs to be built locally to pick up
  # the cyrus-sasl XOAUTH2 plugin (???) so it lives at ~/bin/mbsync and I update
  # it manually when necessary. mbsync isn't updated that frequently so this
  # isn't so bad.
  # I can't get home-manager to build my mbsync config file without also
  # installing mbsync, so I give the full path to ~/bin/mbsync in mail/sync.sh.
  # See:
  # - https://unix.stackexchange.com/questions/625637/configuring-mbsync-with-authmech-xoauth2
  # - https://bbs.archlinux.org/viewtopic.php?id=238727
  # - http://blog.onodera.asia/2020/06/how-to-use-google-g-suite-oauth2-with.html
  programs.mbsync.enable = true;

  services.imapnotify.enable = true;

  # Advantages of systemd service for mail fetching:
  #  - Timer/restart infrastructure
  #  - Environment control (good for NM dispatcher (runs as root), Nix)
  # This service is started:
  #  - by the timer below
  #  - by NetworkManager dispatcher on connection events
  #  - by me, ad hoc
  systemd.user.services.mail-fetch = {
    Unit = {
      Description = "Fetch mail";
      Wants = "mail-notify.service";
      # Retry a couple of times, but failures are probably due to network
      # issues. NetworkManager will reset and retry once connected
      StartLimitIntervalSec = 480; # (TimeoutSec + RestartSec) * 3 + fudge
      StartLimitBurst = 4; # not 3: could be an extra start request if timer and
                           # NM happen to run at the same time
    };
    Service = {
      Type = "oneshot";
      ExecStart = "${scripts}/mail/sync.sh";
      TimeoutSec = 120; # auth is flaky and tends to hang rather than fail
      Restart = "on-failure";
      RestartSec = 30;
    };
  };

  # notify.sh blocks until the notification gets interaction, which means a
  # oneshot service would time out. Separate exec service kicked off when
  # mail-fetch finishes. Bit convoluted but it works.
  systemd.user.services.mail-notify = {
    Unit = {
      Description = "Notify of new mail";
      After = "mail-fetch.service";
    };
    Service = {
      Type = "exec";
      ExecStart = "${scripts}/mail/notify.sh";
    };
  };

  # imapnotify is nice but not perfect. Also check every 20 minutes as a fallback
  systemd.user.timers.mail-fetch = {
    Unit = {
      Description = "Check mail every 20 minutes";
    };
    Timer = {
      OnCalendar = "*:0,20,40";
    };
    Install = {
      WantedBy = [ "timers.target" ];
    };
  };

  programs.msmtp.enable = true;

  programs.notmuch = {
    # unfortunately notmuch is also installed through pacman, because I can't
    # figure out how to make the shared lib available to python if it's
    # installed through nix
    enable = true;
    new.ignore = [ "drafts" ]; # something also adds .uidvalidity and .mbsyncstate to this list
    new.tags = [ "new" ];
    # this doesn't actually work, needs to be added to the DB but the config set
    # command fails because notmuchrc is unwritable. also not sure this is the
    # right format anyway
    extraConfig = {
      headers = {
        XSpamFlag = "X-Spam-Flag";
        XBogosity = "X-Bogosity";
      };
    };
    hooks.postNew = ''
      # Apply initial tags (sent, notify, etc)
      ${pkgs.notmuch}/bin/notmuch tag --input ${scripts}/mail/notmuch-post-new-tags
      # Train bogofilter on my own sent mail (ham)
      ${pkgs.notmuch}/bin/notmuch search --output=files tag:new and tag:sent | xargs bogofilter -nB
      # Remove new, we're done
      ${pkgs.notmuch}/bin/notmuch tag -new -- tag:new
    '';
    search.excludeTags = ["deleted" "spam"];
  };

  # notmuch-show-view-part uses .mailcap for file associations. not sure what
  # mechanism I'm using in general or why it doesn't apply here - investigate
  home.file.".mailcap".text = ''
    application/vnd.openxmlformats-officedocument.wordprocessingml.document; /usr/bin/xdg-open %s
  '';

  systemd.user.services.i3-ipc = {
    Unit = {
      Description = "i3 IPC handler";
    };
    Service = {
      ExecStart = "${scripts}/i3/ipc.sh";
      Restart = "always";
    };
  };

  # Check wohanley.com guestbook

  systemd.user.services.check-guestbook = {
    Unit = {
      Description = "Check wohanley.com guestbook for changes";
      # stop retrying after a while, might just not have network
      StartLimitIntervalSec = 300;
      StartLimitBurst = 6;
    };
    Service = {
      Type = "oneshot";
      WorkingDirectory = "${scripts}/guestbook";
      ExecStart = "${scripts}/guestbook/check-for-updates.sh";
      # failures likely, network is slow to come up
      Restart = "on-failure";
      RestartSec = 20;
    };
  };

  systemd.user.timers.check-guestbook = {
    Unit = {
      Description = "Check wohanley.com guestbook for changes occasionally";
    };
    Timer = {
      OnBootSec = "5min";
      # I think I got rate-limited by Etherpad at some point for checking too
      # often, but surely every two hours is OK...
      OnUnitActiveSec = "120min";
    };
    Install = {
      WantedBy = [ "timers.target" ];
    };
  };

  services.darkman = {
    enable = true;
    settings = {
      lat = 37.4;
      lng = -122.1;
    };
  };

  services.dunst = {
    enable = true;
    settings = {
      global = {
        indicate_hidden = "yes";
        sort = "yes";
        idle_threshold = 120;
        font = "sans-serif 10";
        show_age_threshold = 60;
        mouse_left_click = "do_action";
        mouse_right_click = "close_current";
        mouse_middle_click = "close_all";
      };
    };
  };

  services.redshift = {
    enable = true;
    latitude = 37.4;
    longitude = -122.1;
    temperature.day = 6500; # neutral (no adjustment)
    temperature.night = 3700;
  };

  systemd.user.services.volctl = {
    Service = {
      ExecStart = "${pkgs.volctl}/bin/volctl";
    };
    Install = {
      WantedBy = [ "graphical-session.target" ];
    };
  };
}
