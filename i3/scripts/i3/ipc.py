from i3ipc import Connection, Event
import json
import Xlib.display

# workaround for https://github.com/i3/i3/issues/2855

# Could probably use xlib to grab the new window event too, but the i3ipc API
# is simpler

def on_window_title(i3, e):
    if e.container.name == "Zoom Meeting":
        display = Xlib.display.Display()
        win = display.create_resource_object('window', e.container.window)
        protocols = win.get_wm_protocols()
        win.set_wm_protocols([p for p in protocols if p != display.intern_atom('WM_TAKE_FOCUS')])

i3 = Connection()
i3.on(Event.WINDOW_TITLE, on_window_title)
i3.main()
