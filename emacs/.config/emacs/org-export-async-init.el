;; (require 'package)
;; (setq package-enable-at-startup nil)
;; (setq package-user-dir (expand-file-name "~/.config/emacs/.local/straight/build-29.4/"))
;; ;; (setq package-archives '(("elpa" . "https://elpa.gnu.org/packages/")
;; ;;                          ("melpa" . "https://melpa.org/packages/")))
;; (package-initialize)

(setq package-enable-at-startup nil)

(let ((default-directory (expand-file-name "~/.config/emacs/.local/straight/build-29.4/")))
  (normal-top-level-add-subdirs-to-load-path))

(add-to-list 'load-path (expand-file-name "~/.config/emacs/.local/straight/build-29.4/org/"))

(require 'org)
(require 'org-contrib)
(require 'org-archive)
(require 'org-id)
(require 'ox)
(require 'ox-beamer)
(require 'ox-latex)
(require 'cl-lib)

(require 'dash)
(require 'esxml)
(require 'f)
(require 'org-roam)
(require 's)

(load-file "~/.config/emacs/who/org-funcs.el")

(defun who/rg-files-with-matches (match search-dir)
  (->> (format "rg --files-with-matches '%s' '%s'" match search-dir)
       (shell-command-to-string)
       (s-lines)
       (-filter (lambda (line) (not (s-blank? line))))))


(setq org-id-locations-file (expand-file-name "~/.config/emacs/.local/cache/.org-id-locations"))
(setq org-roam-directory (expand-file-name "~/org/zettelkasten"))
(setq org-roam-db-location (expand-file-name "~/.local/share/org-roam/org-roam.db"))
(setq org-todo-keywords '((sequence "TODO(t)" "WAIT(w)" "NEXT(n)" "|" "DONE(d)" "NOPE(f)")))


(setq org-cite-global-bibliography '("~/org/master.bib"))
(setq org-cite-csl-styles-dir (expand-file-name "~/Zotero/styles"))
(setq org-cite-export-processors '((t . (csl "chicago-fullnote-bibliography.csl"))))

(org-export-define-derived-backend 'willopedia-html 'html
  :translate-alist '((template . who/willopedia-zettel-template)
                     (link . who/willopedia-link)
                     (headline . who/willopedia-headline)
                     (src-block . who/willopedia-src-block)))

(setq org-export-exclude-tags '("private" "noexport"))
(setq org-export-with-properties '("NOTER_PAGE"))
(setq org-export-preserve-breaks t)
(setq org-export-with-entities t)
(setq org-export-headline-levels 6)
(setq org-export-with-tasks nil)
(setq org-export-with-toc nil)
(setq org-export-with-section-numbers nil)
(setq org-export-with-sub-superscripts nil)
(setq org-export-with-tags nil)
(setq org-export-with-broken-links 'mark)
;; (setq org-export-with-smart-quotes t) ;; needs some work on escaping

(setq org-html-metadata-timestamp-format "%Y-%m-%d")
(setq org-html-checkbox-type 'unicode)
(setq org-html-html5-fancy t)
(setq org-html-htmlize-output-type 'css)
;; (setq org-html-inline-images t)
;; (setq org-html-self-link-headlines t)
;; (setq org-html-doctype "html5")
;; (setq org-html-validation-link nil)

;; need to rebuild all to get the backlinks right anyway
(setq org-publish-use-timestamps-flag nil)

(org-id-locations-load)

(setq who/willopedia-publish-all t)
(who/willopedia-update-publish-files)

(setq org-export-async-debug t)
