// source for "or" bookmarklet
(function () {
  const capture = (template, args) => {
    let href = `org-protocol://roam-any?template=${template}`;
    for (const k in args) {
      href += `&${k}=${encodeURIComponent(args[k])}`;
    }
    location.href = href;
  };

  if (location.origin === 'https://www.canlii.org') {
    const segments = document.title.split(' | ');
    capture('canlii', {
      ref: location.origin + location.pathname,
      title: segments[1] + ', ' + segments[0].replace(' (CanLII)', '')
    });
  } else if (location.href.startsWith('https://app.qase.net/matters/')) {
    location.href =
      'org-protocol://roam-any?template=q' +
      '&qase=' + encodeURIComponent(location.href) +
      '&title=' + encodeURIComponent(document.querySelector('.card-body label').textContent) +
      '&email=' + encodeURIComponent(document.querySelectorAll('.card-body p')[3].textContent) +
      '&phone=' + encodeURIComponent(document.querySelectorAll('.card-body p')[1].textContent) +
      '&referral_description=' + encodeURIComponent(document.querySelectorAll('.card-body p')[5].textContent);
  } else {
    location.href =
      'org-protocol://capture?template=c' +
      '&url=' + encodeURIComponent(location.href) +
      '&title=' + encodeURIComponent(document.title) +
      '&body=' + encodeURIComponent(window.getSelection());
  }
})();
