;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys

(setq custom-file (expand-file-name "~/.config/emacs/custom.el"))
(load custom-file t)

;; Good for performance, see https://tony-zorman.com/posts/2022-10-22-emacs-potpourri.html
(setq frame-inhibit-implied-resize t)

(setq warning-minimum-level :error)

(defun who/path-expand (&rest segments)
  (expand-file-name (apply #'file-name-concat segments)))

(defun who/read-file-contents (filename)
  "Return the contents of the file named FILENAME."
  (with-temp-buffer
    (insert-file-contents filename)
    (buffer-string)))

(load! (expand-file-name "~/.config/emacs/who/elfeed-funcs.el"))
(load! (expand-file-name "~/.config/emacs/who/notmuch-funcs.el"))
(load! (expand-file-name "~/.config/emacs/who/org-funcs.el"))

;; Doom tweaks

;; I do like the spellcheck module, but I don't want it enabled automatically
(remove-hook! '(org-mode-hook
                markdown-mode-hook
                TeX-mode-hook
                rst-mode-hook
                mu4e-compose-mode-hook
                message-mode-hook
                git-commit-mode-hook)
  #'flyspell-mode)

;; Doom adds advice to make auto-save and backup file names hashes of the
;; original file name, to avoid long paths. Undo that here; long paths have
;; never caused a problem for me and legible backup file names are valuable
(advice-remove #'make-auto-save-file-name #'doom-make-hashed-auto-save-file-name-a)
(advice-remove #'make-backup-file-name-1 #'doom-make-hashed-backup-file-name-a)

;; GC after Emacs loses focus

(defvar who/blur-gc-timer nil)
(defun who/focus-change-gc ()
  "If Emacs loses focus, set a timer to collect garbage in 15 seconds. If Emacs
gains focus, cancel that timer.

See https://config.phundrak.com/emacs/basic-config.html#better-garbage-collection"
  (if (frame-focus-state)
      (when (timerp who/blur-gc-timer)
        (cancel-timer who/blur-gc-timer))
    (setq who/blur-gc-timer (run-with-idle-timer 15 nil #'garbage-collect))))
(add-function :after after-focus-change-function #'who/focus-change-gc)

;; Personal info etc

(setq user-full-name "William O'Hanley")
(setq user-mail-address "wohanley@wohanley.com")
(setq mail-host-address "wohanley.com")

(setq calendar-location-name "Vancouver, BC")
(setq calendar-latitude 49.2827)
(setq calendar-longitude -123.1207)
;; (setenv "TZ" "Canada/Pacific")

(setq oauth2-auto-plstore (expand-file-name "~/.config/emacs/oauth2-auto.plist"))
(setq plstore-cache-passphrase-for-symmetric-encryption t)

;; don't explode when opening files with long lines
(global-so-long-mode 1)
(setq-default bidi-paragraph-direction 'left-to-right) ;; https://200ok.ch/posts/2020-09-29_comprehensive_guide_on_handling_long_lines_in_emacs.html

;; Disable undo for some files

(defvar who/undo-blocklist
  (list (expand-file-name "~/.elfeed")
        (concat user-emacs-directory ".cache"))
  "List of files or directories in which to disable undo-tree.")

(defun who/undo-disable ()
  (when (or (member (buffer-file-name) who/undo-blocklist)
            (-some (lambda (blocked) (f-ancestor-of? blocked (buffer-file-name)))
                   who/undo-blocklist))
    (buffer-disable-undo)))

(add-hook 'find-file-hook #'who/undo-disable)

;; Misc editor tweaks and conveniences

(setq display-line-numbers-type t)
(setq fill-column 100)
(setq kill-whole-line t)
;; follow links to VCed files without asking
(setq vc-follow-symlinks t)
;; anything that writes to the buffer while the region is active will
;; overwrite it (like in most editors)
(delete-selection-mode 1)
;; see https://www.masteringemacs.org/article/demystifying-emacs-window-manager
(setq switch-to-buffer-in-dedicated-window 'pop)

;; Dired

(after! dirvish
  (setq dirvish-open-with-programs `((,dirvish-audio-exts . ("xdg-open" "%f"))
                                     (,dirvish-video-exts . ("xdg-open" "%f")))))

(setq delete-by-moving-to-trash t)

(defun who/dired-no-confirm-when-trash (fn &rest args)
  (let ((dired-no-confirm (if delete-by-moving-to-trash t dired-no-confirm)))
    (apply fn args)))

(advice-add #'dired-do-delete         :around #'who/dired-no-confirm-when-trash)
(advice-add #'dired-do-flagged-delete :around #'who/dired-no-confirm-when-trash)

(defun who/dired-do-delete (prefix)
  "`dired-do-delete', but skip sending to trash with prefix argument."
  (interactive "P")
  (let ((delete-by-moving-to-trash (if prefix nil delete-by-moving-to-trash)))
    (dired-do-delete)))

(defun who/dired-do-flagged-delete (prefix)
  "`dired-do-flagged-delete', but skip sending to trash with prefix argument."
  (interactive "P")
  (let ((delete-by-moving-to-trash (if prefix nil delete-by-moving-to-trash)))
    (dired-do-flagged-delete)))

(defun who/dired-dwim ()
  (interactive)
  (let ((entry (dired-get-filename nil t)))
    (if (f-ancestor-of? "~/Sync/feeds" entry)
        ;; Play the podcast episode corresponding to dired entry, then trash it when finished.
        (let* ((fname (expand-file-name entry))
               (proc
                ;; Invoke VLC directly. Sometimes XDG is fucky, content types
                ;; aren't set or read correctly, etc etc
                (start-process "" nil "/usr/bin/vlc" fname)))
          (set-process-sentinel proc
                                (lambda (_proc ev)
                                  (when (equal ev "finished\n")
                                    (dired-delete-file fname nil 'trash)))))
      (dired-find-file))))

(map!
  :after dired
  :map dired-mode-map
  "RET" #'who/dired-dwim
  "D"   #'who/dired-do-delete
  "x"   #'who/dired-do-flagged-delete)

;; Notdeft

(use-package! notdeft
  :commands (notdeft notdeft-list-files-by-query)
  :config
  (setq notdeft-directories '("~/org/zettelkasten"))
  (setq notdeft-xapian-program (expand-file-name "~/.config/emacs/private/notdeft/local/notdeft/xapian/notdeft-xapian"))
  ;; track changes to org files
  (add-hook 'org-mode-hook 'notdeft-note-mode))

(map!
  :leader
  "nd" #'notdeft)

;; Programming modes

;; Elisp

;; use default fill-column, elisp ain't special
(setq emacs-lisp-docstring-fill-column nil)

;; suppress annoying flycheck messages
(defun who/flycheck-filter-errors-emacs-lisp (args)
  (if (eq (nth 1 args) 'emacs-lisp)
      (progn
        (setcar args
                (cl-remove-if (lambda (err)
                                (string-match-p "fails to specify containing group" (flycheck-error-message err)))
                              (nth 0 args)))
        args)
    args))
(advice-add #'flycheck-filter-errors :filter-args #'who/flycheck-filter-errors-emacs-lisp)

(after! smartparens
  ;; use elisp quoting rules in minibuffer
  (sp-local-pair '(minibuffer-mode minibuffer-inactive-mode) "'" nil :actions nil)
  (sp-local-pair '(minibuffer-mode minibuffer-inactive-mode) "`" nil :actions nil))

(map!
  :after smartparens
  :map smartparens-mode-map
  "C-M-k" #'kill-sexp ;; override sp-kill-sexp, which I don't really get
)

;; Highlight occurrences of symbol at point. LSP handles this for other langs
(add-hook 'emacs-lisp-mode-hook #'idle-highlight-mode)
(after! idle-highlight-mode
  (setq idle-highight-idle-time 0.2)
  (setq idle-highlight-exclude-point t))

;; Ink

(add-hook 'ink-mode-hook (lambda () (setq tab-width 2)))
(add-hook 'ink-mode-hook
          (lambda () (if (not ink-inklecate-path)
                         (setq ink-inklecate-path (executable-find "inklecate")))))
(add-hook 'ink-mode-hook #'flymake-mode)
(add-hook 'ink-mode-hook #'+word-wrap-mode)

;; Org

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;; (setq org-directory "~/org/")

(add-to-list 'org-modules 'org-habit)

(defvar who/org-context 'personal)

(after! org
  ;; Doom resets
  ;; these don't play nicely with `who/org-return'
  (setq org-M-RET-may-split-line '((default . t)))
  (setq org-insert-heading-respect-content nil)
  ;; unclear on why Doom changes this...
  (setq org-id-locations-file (expand-file-name "~/.config/emacs/.local/cache/.org-id-locations"))

  (setq org-export-async-init-file "~/.config/emacs/org-export-async-init.el")

  (setq org-blank-before-new-entry '((heading . nil)
                                     (plain-list-item . nil)))
  (setq org-cliplink-max-length 120)
  (setq org-clock-display-default-range 'untilnow)

  (setq org-special-ctrl-a/e t)
  (setq org-startup-folded 'showeverything) ;; sometimes interesting stuff in the property drawers
  (setq org-startup-with-inline-images t)
  (setq org-adapt-indentation nil)
  (setq org-indent-indentation-per-level 0)
  (setq org-list-indent-offset 2)
  (setq org-return-follows-link t)
  (setq org-link-frame-setup '((file . find-file))) ;; follow links in same window
  (setq org-fold-catch-invisible-edits 'show)

  (add-hook 'org-after-todo-state-change-hook #'who/org-clock-out-on-state)
  (add-hook 'org-mode-hook #'who/style-org)
  (add-hook 'org-mode-hook #'who/ef-themes-custom-faces-org)
  (add-hook 'ef-themes-post-load-hook #'who/ef-themes-custom-faces-org)
  (setq org-agenda-before-sorting-filter-function #'who/org-agenda-before-sorting-filter)

  (defface who/org-agenda-category
    '((t :weight light))
    "Face for category prefix in agenda.")

  (defface who/org-priority-low
    '((t :weight light))
    "Face for #C priority in agenda."))

;; (setq org-attach-id-dir "attach/")
;; (setq org-attach-id-to-path-function-list '(who/org-attach-uuid-to-path
;;                                             org-attach-id-uuid-folder-format
;;                                             org-attach-id-ts-folder-format
;;                                             org-attach-id-fallback-folder-format))

;; Doom docs: Org-protocol has been lazy loaded (see +org-init-protocol-lazy-loader-h); loaded when the server receives a request for an org-protocol:// url.

;;   (require 'org-protocol)
;;   (add-to-list 'org-modules 'org-protocol)

;;   (add-hook 'auto-save-hook (lambda () (quiet! (org-save-all-org-buffers)))))

(after! ol
  ;; link to notmuch search
  (org-link-set-parameters "notmuch-tree" :follow (lambda (query _) (notmuch-tree query)))
  ;; citations are not links, but I'd like to be able to act like they are (e.g. [[cite:@whatever][Whatever]])
  ;; grotesque abuse of org link handling... whatever
  (org-link-set-parameters "cite"
                           :follow (lambda (query _)
                                     (helm-bibtex-follow (list nil (list :key (substring query 1)))))))

(after! org-agenda
  (defun who-org/find-projects ()
    (notdeft-list-files-by-query "!all tag:\"project\" AND NOT tag:\"archive\""))

  (defun who-org/find-areas-of-responsibility ()
    (notdeft-list-files-by-query "!all tag:\"aor\" AND NOT tag:\"archive\""))

  (defun who/find-org-files (directory)
    (->> (format "fd '\.org$' %s" directory)
         (shell-command-to-string)
         (s-lines)
         (-filter (lambda (line) (not (s-blank? line))))))

  (defun who/org-agenda-files-update (&rest _)
    "Update `org-agenda-files' from org files.
Includes files with open TODOs. Would be nice to also include files with active
timestamps."
    (let ((todo-zettels (->> (s-concat "rg --files-with-matches '(^\\*+ TODO)|(^\\*+ NEXT)|(^\\*+ HOLD)|(^\\*+ WAIT)' " org-roam-directory)
                             (shell-command-to-string)
                             (s-lines)
                             (-filter (lambda (line) (not (or (s-blank? line)
                                                              (s-starts-with? ".#" (file-name-nondirectory line)))))))))
      (setq org-agenda-files (seq-uniq (append (who/find-org-files who/org-agenda-directory)
                                               todo-zettels
                                               who/org-agenda-files-extra)))))
  (advice-add 'org-agenda :before #'who/org-agenda-files-update)

  (defun who/switch-to-agenda ()
    (interactive)
    (org-agenda nil " "))

  (setq org-agenda-log-mode-items '(closed clock state)) ;; adding 'state includes habits in log
  (setq org-agenda-todo-list-sublevels nil) ;; don't show TODOs that are the child of another TODO
  (setq org-agenda-dim-blocked-tasks nil) ;; this is slow and I don't need it (https://orgmode.org/worg/agenda-optimization.html)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-log-state-notes-insert-after-drawers nil)

  (setq org-todo-keywords
        '((sequence "TODO(t@)" "HOLD(h@)" "WAIT(w@)" "NEXT(n)" "|" "DONE(d)" "NOPE(f)")))

  (setq org-agenda-block-separator nil)
  (setq org-agenda-start-with-log-mode t)
  (setq org-columns-default-format "%40ITEM(Task) %Effort(EE){:} %CLOCKSUM(Time Spent) %SCHEDULED(Scheduled) %DEADLINE(Deadline)")
  (setq org-agenda-prefix-format '((agenda . " %i %-10 c%?-12t% s")
                                   (todo . " %i %-10 c")
                                   (tags . " %i %-10 c")
                                   (search . " %i %-10 c")))
  (setq org-agenda-window-setup 'current-window)
  (setq org-agenda-restore-windows-after-quit nil)

  ;; https://github.com/syl20bnr/spacemacs/issues/3094
  (setq org-refile-use-outline-path 'file
        org-outline-path-complete-in-steps nil)
  (setq org-refile-allow-creating-parent-nodes 'confirm)

  (defvar who/org-current-effort "1:00" "Current effort for agenda items.")

  ;; Handle tasks with both scheduled and deadline timestamps in the agenda:
  ;;  - Don't show the deadline warning before the scheduled date
  ;;  - Don't show a duplicate line for the scheduled timestamp if the task is already shown because
  ;;    of its deadline
  ;;  - Don't show DONEs at all
  (setq org-agenda-skip-deadline-prewarning-if-scheduled 'pre-scheduled)
  (setq org-agenda-skip-scheduled-if-deadline-is-shown t)
  (setq org-agenda-skip-deadline-if-done t)

  ;; XXX is this the right place for this? forget what it's supposed to do
  (set-face-attribute 'org-agenda-done nil :foreground 'unspecified)

  ;; `auto-save-visited-mode' will save visited Org files in-place every 5 seconds
  (add-hook 'org-mode-hook #'auto-save-visited-mode)
  ;; quiet `save-some-buffers' when autosaving, but keep it for interactive use
  (advice-add #'save-some-buffers
              :around
              (lambda (f &rest args)
                (let ((inhibit-message (not (called-interactively-p 'any)))
                      (save-silently   (not (called-interactively-p 'any))))
                  (apply f args))))

  (who/org-agenda-context-personal))

(global-set-key (kbd "<f1>") #'who/switch-to-agenda)

(map!
  :after org-agenda
  :map org-agenda-mode-map
  "i" #'org-agenda-clock-in
  "o" #'org-agenda-clock-out
  "s" #'who/org-agenda-process-inbox-item
  "r" #'who/org-process-inbox
  "R" #'org-agenda-refile
  "y" #'org-agenda-todo-yesterday ;; overriding org-agenda-year-view
)

(after! org-cite
  (setq org-cite-global-bibliography '("~/org/master.bib"))
  ;; open helm-bibtex when clicking an org cite: link (instead of opening bibliography)
  (setq org-cite-follow-processor #'helm-bibtex-org-cite-follow)
  (setq org-cite-csl-styles-dir (expand-file-name "~/Zotero/styles"))
  (setq org-cite-export-processors '((t . (csl "chicago-fullnote-bibliography.csl")))))

(after! org-download
  (if (memq window-system '(mac ns))
      (setq org-download-screenshot-method "screencapture -i %s")
    (setq org-download-screenshot-method "import %s"))
  (setq who/org-download-directory (expand-file-name "~/org/download"))
  (setq org-download-method #'who/org-download))

(map!
  :after org-download
  :map org-mode-map
  :localleader
  "s-Y" #'org-download-screenshot
  "s-y" #'org-download-yank)

(after! org-export
  (org-export-define-derived-backend 'willopedia-html 'html
    :translate-alist '((template . who/willopedia-zettel-template)
                       (link . who/willopedia-link)
                       (headline . who/willopedia-headline)
                       (src-block . who/willopedia-src-block)))

  (setq org-export-exclude-tags '("private" "noexport"))
  (setq org-export-with-properties '("NOTER_PAGE"))
  (setq org-export-preserve-breaks t)
  (setq org-export-with-entities t)
  (setq org-export-headline-levels 6)
  (setq org-export-with-toc nil)
  (setq org-export-with-section-numbers nil)
  (setq org-export-with-sub-superscripts nil)
  (setq org-export-with-tags nil)
  (setq org-export-with-broken-links 'mark)
  ;; (setq org-export-with-smart-quotes t) ;; needs some work on escaping

  (setq org-html-doctype "html5")
  (setq org-html-metadata-timestamp-format "%Y-%m-%d")
  (setq org-html-checkbox-type 'unicode)
  (setq org-html-html5-fancy t)
  (setq org-html-htmlize-output-type 'css)
  ;; (setq org-html-inline-images t)
  ;; (setq org-html-self-link-headlines t)
  ;; (setq org-html-doctype "html5")
  ;; (setq org-html-validation-link nil)

  ;; need to rebuild all to get the backlinks right anyway
  (setq org-publish-use-timestamps-flag nil))

(setq org-gcal-token-file "~/.config/org-gcal/.org-gcal-token"
      org-gcal-client-id (who/read-file-contents "~/.config/org-gcal/.org-gcal-client-id")
      org-gcal-client-secret (who/read-file-contents "~/.config/org-gcal/.org-gcal-client-secret")
      org-gcal-fetch-file-alist '(("willy.ohanley@gmail.com" . "~/org/gtd/calendars/personal.org")
                                  ("wohanley@everyonelegal.ca" . "~/org/gtd/calendars/elc.org")
                                  ("c_8ui5all7mct0ts559nu72l6t88@group.calendar.google.com" . "~/org/gtd/calendars/elc-shared.org"))
      ;; '(-25200 "PDT") is the format that comes out of
      ;; #'current-time-zone, and #'format-time-string seems to only work
      ;; with this format. supposedly you can pass a string like
      ;; "Canada/Pacific" to #'format-time-string, but that always returns a
      ;; time in UTC in my experience. I dunno wtf is going on, but this
      ;; seems to do what I want. see https://github.com/kidd/org-gcal.el/issues/164
      org-gcal-local-timezone nil)

(defun who/org-gcal-add-at-point ()
  "Post the org item at point to gcal."
  (interactive)
  (org-entry-put (point) "calendar-id" "willy.ohanley@gmail.com")
  (org-gcal-post-at-point))

(defun who/org-schedule-incl-gcal-at-point ()
  "Schedule the org item at point and post it to gcal."
  (interactive)
  (org-schedule nil)
  (who/org-gcal-add-at-point))

(use-package! org-gcal
  :commands (org-gcal-post-at-point)
  :config
  ;; see https://github.com/kidd/org-gcal.el/issues/164
  (defun who/org-gcal--convert-time-to-local-timezone (date-time local-timezone)
    (format-time-string "%Y-%m-%dT%H:%M:%S%z" (parse-iso8601-time-string date-time) local-timezone))

  (advice-add #'org-gcal--convert-time-to-local-timezone :override #'who/org-gcal--convert-time-to-local-timezone))

(after! org-habit
  ;; Remove unnecessary Doom fuckery
  (remove-hook 'org-agenda-mode-hook #'+org-habit-resize-graph-h)

  (setq org-habit-preceding-days 21)
  (setq org-habit-following-days 7))

(after! org-noter
  (defun who/org-noter-insert-highlighted-note ()
    "Highlight the active region and add a precise note at its position."
    (interactive)
    ;; Adding an annotation will deactivate the region, so we reset it afterward
    (let ((region (pdf-view-active-region)))
      (call-interactively 'pdf-annot-add-highlight-markup-annotation)
      (setq pdf-view-active-region region))
    (call-interactively 'org-noter-insert-precise-note))

  (defun who/org-noter-extend-highlighted-note ()
    "Highlight the active region and add its text to the previous note.

This is useful when a note extends over a page boundary."
    (interactive)
    (let ((selected (mapconcat 'identity (pdf-view-active-region-text) ? )))
      (call-interactively 'pdf-annot-add-highlight-markup-annotation)
      (select-window (org-noter--get-notes-window 'force))
      (save-excursion
        (call-interactively 'org-previous-visible-heading)
        (move-end-of-line nil)
        (insert " " (replace-regexp-in-string "\n" " " selected)))
      (select-window (org-noter--get-doc-window))))

  (add-hook 'pdf-view-mode-hook 'pdf-view-fit-width-to-window)

  (setq org-noter-insert-note-no-questions t)
  (setq org-noter-hide-other nil)
  ;; treat all notes as short
  (setq org-noter-max-short-selected-text-length most-positive-fixnum))

(map!
  :after org-noter
  :map org-noter-doc-mode-map
  "i"         #'org-noter-insert-precise-note
  "<mouse-9>" #'org-noter-insert-precise-note
  "M-i"       #'org-noter-insert-note
  "e"         #'who/org-noter-extend-highlighted-note
  "d"         #'who/org-noter-insert-highlighted-note
  "<mouse-8>" #'who/org-noter-insert-highlighted-note
  "e"         #'who/org-noter-extend-highlighted-note
  "h"         #'pdf-annot-add-highlight-markup-annotation)

(after! org-protocol
  (push '("org-roam-any" :protocol "roam-any" :function who/org-roam-protocol-open-any)
        org-protocol-protocol-alist))

(after! org-roam
  (setq org-roam-v2-ack t)

  ;; (require 'org-roam-protocol) ;; FIXME?

  ;; Doom will try to defer compilation of emacsql-sqlite for me, which I don't
  ;; need to compile at all. Just enable autosync normally
  (advice-remove #'emacsql-sqlite-ensure-binary #'+org-roam-suppress-sqlite-build-a)
  (advice-remove #'org-roam-db-query #'+org-roam-try-init-db-a)
  (defun +org-init-roam-h ()
    (org-roam-db-autosync-enable))

  ;; Also a Doom reset
  (setq org-roam-completion-everywhere nil)

  ;; Cannot convince org-mode not to split windows when visiting a link from the
  ;; backlinks side-window, so a hacky workaround. Seems to be some strange
  ;; Doom/org-roam interaction.
  ;; See: https://github.com/org-roam/org-roam/issues/920
  ;; See: https://github.com/doomemacs/doomemacs/issues/4524
  ;; See: https://org-roam.discourse.group/t/help-implementing-the-opening-internal-links-in-the-same-window-frame-work-around-fix/1151
  (advice-add #'org-roam-preview-visit :after (lambda (&rest _args) (delete-other-windows)))

  (define-key org-roam-mode-map [mouse-1] #'org-roam-preview-visit)

  ;; Sadly I am affected by https://github.com/doomemacs/doomemacs/issues/7676
  (setq +org-roam-auto-backlinks-buffer nil)

  (setq org-journal-dir "~/org/zettelkasten")
  (setq org-journal-date-prefix "#+TITLE: ")
  (setq org-journal-file-format "journal-%Y-%m-%d.org")
  (setq org-journal-date-format "%Y-%m-%d")

  (add-hook 'org-roam-preview-postprocess-functions
            (lambda (s)
              (s-join "\n"
                      (-take 3 (s-lines s)))))

  (who/org-roam-context-personal)

  ;; Don't try to find an org-roam node while in agenda, it won't work (e.g.
  ;; calling helm-bibtex from the agenda will blow up without this advice)
  (advice-add #'org-roam-node-at-point :around
              (lambda (fn &rest args)
                (if (eq major-mode 'org-agenda-mode)
                    nil
                  (apply fn args))))

  ;; FIXME? See https://github.com/org-roam/org-roam/issues/1744#issuecomment-894649347
  (setq org-roam-node-display-template
        (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))

  ;; Memoize `org-roam-node-read--completions', which is quite slow (~4s).

  (defvar who/org-roam-completions-cache nil)

  (defun who/org-roam-completions-cache-invalidate (&rest _args)
    (setq who/org-roam-completions-cache nil))

  (defun who/org-roam-node-read--completions-memoize (fn &rest args)
    (if (cl-some #'identity args) ;; Pass through if filter or sort fns were supplied
        (apply fn args)
      (or who/org-roam-completions-cache
          (setq who/org-roam-completions-cache (apply fn args)))))

  ;; XXX Are there other occasions to invalidate this cache?
  (advice-add #'org-roam-capture-       :after #'who/org-roam-completions-cache-invalidate)
  (advice-add #'org-roam-db-clear-file  :after #'who/org-roam-completions-cache-invalidate)
  (advice-add #'org-roam-db-update-file :after #'who/org-roam-completions-cache-invalidate)
  (advice-add #'org-roam-node-read--completions :around #'who/org-roam-node-read--completions-memoize))

(map!
  :after org-roam
  :map org-mode-map
  :leader
  "nra"  #'org-roam-alias-add
  "nrc"  #'org-roam-node-capture
  "nrl"  #'org-roam-buffer-toggle
  "nrp"  #'who/org-roam-mark-public
  "nrta" #'org-roam-tag-add
  "nrtd" #'org-roam-tag-delete)

;; (map!
;;   :map org-mode-map
;;   :localleader
;;   "rb"  'org-roam-switch-to-buffer
;;   "rdy" 'org-roam-dailies-goto-yesterday
;;   "rdt" 'org-roam-dailies-goto-today
;;   "rdT" 'org-roam-dailies-goto-tomorrow
;;   "rdd" 'org-roam-dailies-goto-date
;;   "rf"  'org-roam-node-find
;;   "rm"  'who/org-roam-matter-create
;;   "rg"  'org-roam-graph
;;   "ri"  'org-roam-node-insert
;;   "rl"  'org-roam-buffer-toggle
;;   "rta" 'org-roam-tag-add
;;   "rp"  'who/org-roam-mark-public
;;   "rtd" 'org-roam-tag-delete
;;   "ra"  'org-roam-alias-add)

(use-package! org-roam-ui
  :config
  ;; Don't try to sync general Emacs theme with ORUI (I have custom ORUI themes)
  (setq org-roam-ui-sync-theme nil)

  ;; Stop ORUI when closing browser tab
  (advice-add 'org-roam-ui--ws-on-close
              :after
              (lambda (_ws)
                (org-roam-ui-mode -1)))

  ;; Sync theme after activating ORUI
  ;; (advice-add 'org-roam-ui--ws-on-open
  ;;             :after
  ;;             (lambda (_ws)
  ;;               ;; I have no idea why but '(org-roam-ui-mode t) gets pushed to
  ;;               ;; my custom theme settings on (org-roam-ui-mode 1), so undo
  ;;               ;; that here. ??????????
  ;;               (custom-push-theme 'theme-value 'org-roam-ui-mode 'user 'reset)
  ;;               (when org-roam-ui-custom-theme
  ;;                 (org-roam-ui-sync-theme))))
)

(use-package! org-superstar
  :hook (org-mode . org-superstar-mode)
  :config
  (setq org-superstar-leading-bullet " ‧")
  (setq org-superstar-headline-bullets-list (list ?🟎))

  ;; Using variable-pitch fonts in org-mode (with ef-themes-mixed-fonts)
  ;; includes leading asterisks (or whatever org-superstar puts in there);
  ;; override to keep headings indented. I don't think this is really the right
  ;; way to override a face attribute but whatever
  (advice-add #'org-superstar-mode
              :after
              (lambda ()
                (set-face-attribute 'org-superstar-leading nil :inherit 'fixed-pitch))))

(use-package! org-wild-notifier
  :hook (org-agenda-mode . org-wild-notifier-mode)
  :config
  (setq org-wild-notifier-alert-time '(60 10 2))
  (setq org-wild-notifier-keyword-whitelist nil)
  (setq org-wild-notifier-keyword-blacklist '("DONE" "NOPE"))
  (setq org-wild-notifier-predicate-whitelist nil)
  (setq org-wild-notifier-predicate-blacklist
        (list
         ;; don't notify about habits
         (lambda (marker)
           (-contains? (org-entry-properties marker 'all)
                       '("STYLE" . "habit")))
         ;; don't notify about events with no specific time set
         (lambda (marker)
           (let ((timestamps (->> (list (org-entry-get marker "TIMESTAMP")
                                        (org-entry-get marker "SCHEDULED")
                                        (org-entry-get marker "DEADLINE"))
                                  (-remove #'null))))
             (if (null timestamps)
                 nil
               (-none? (lambda (ts)
                         (org-timestamp-has-time-p (org-timestamp-from-string ts)))
                       timestamps)))))))

(after! alert
  (alert-define-style 'who/alert-style-reminder
    :title "Agenda reminder"
    :notifier
    (lambda (info)
      (let* ((message (plist-get info :message))
             (info (plist-put info :persistent t))
             (info (if (or (s-contains? "in 2 minutes" message)
                           (s-contains? "right now" message))
                       (plist-put info :severity 'high)
                     info)))
        (alert-libnotify-notify info))))

  (add-to-list 'alert-user-configuration
               '(((:title . "Agenda")) who/alert-style-reminder)))

(map!
  :after org
  :map org-mode-map
  "*"   #'who/org-asterisk
  "RET" #'who/org-return)

(map!
  :after org
  :map org-mode-map
  :localleader
  "n"   #'org-noter
  "i r" #'who/org-insert-link-to-latest)

(map!
  :after org-ref-core
  :map org-mode-map
  :localleader
  ;; overwrite `org-ref-insert-link' binding
  "ic" #'citar-insert-citation)

(after! citar
  (setq citar-bibliography '("~/org/master.bib"))
  (setq citar-library-paths '("~/org/library/"))
  (setq citar-notes-paths (list (expand-file-name "~/org/zettelkasten"))))

(map!
  :leader
  "nb"  nil ;; Doom's default binding for `citar-open-notes'
  "nbl" #'citar-open-notes)

(map!
  :map org-mode-map
  :leader
  "nbi" #'citar-insert-citation)

(after! citar-org-roam
  (setq citar-org-roam-note-title-template "${title}")
  (setq citar-org-roam-capture-template-key "n") ;; see `who/org-roam-template-bib'
)

;; Mail

(add-hook 'message-mode-hook #'turn-off-auto-fill)

(after! notmuch
  (setq notmuch-identities '("William O'Hanley <me@wohanley.com>"
                             "William O'Hanley <william@wohanley.com>"
                             "William O'Hanley <wohanley@law.stanford.edu>"
                             "William O'Hanley <wohanley@stanford.edu>"
                             "William O'Hanley <whohanley@uvic.ca>"
                             "William O'Hanley <willy.ohanley@gmail.com>"
                             "William O'Hanley <william.ohanley@gmail.com>"
                             "William O'Hanley <wohanley@everyonelegal.ca>"))

  ;; not sure why but completions don't work with company
  (setq notmuch-address-use-company nil)

  ;; check for text signalling that there should be an attachment
  (setq notmuch-mua-attachment-regexp "\\(attach\\|\\bhere's\\b\\)")
  (add-hook 'notmuch-mua-send-hook 'notmuch-mua-attachment-check)

  ;; Cosmetics

  (setq notmuch-tree-result-format '(("date" . "%12s  ")
                                     ("authors" . "%-20s")
                                     ((("tree" . "%s")
                                       ("subject" . "%s"))
                                      . " %-54s ")
                                     ("tags" . "%s")))
  (setq notmuch-search-result-format '(("date" . "%12s ")
                                       ("count" . "%-7s ")
                                       ("authors" . "%-20s ")
                                       ("subject" . "%s ")
                                       ("tags" . "%s")))


  ;; Use proportional fonts
  (add-hook 'notmuch-message-mode-hook #'who/buffer-face-mode-human)
  (add-hook 'notmuch-show-mode-hook (lambda () (buffer-face-set 'variable-pitch)))

  (add-hook 'ef-themes-post-load-hook #'who/ef-themes-custom-faces-notmuch)

  (advice-add #'notmuch-tree-process-sentinel
              :around
              (lambda (f &rest args)
                (advice-add #'insert :around #'who/notmuch-insert-end-of-results)
                (apply f args)
                (advice-remove #'insert #'who/notmuch-insert-end-of-results)))

  (setq message-auto-save-directory (expand-file-name "~/.mail/drafts/"))

  (setq message-send-mail-function 'message-send-mail-with-sendmail)
  (setq sendmail-program (expand-file-name "~/scripts/mail/msmtpq"))

  ;; pick SMTP server based on envelope from: per https://notmuchmail.org/emacstips/#index11h2
  (setq message-sendmail-envelope-from 'header)
  (setq mail-envelope-from 'header)
  (setq mail-specify-envelope-from t)

  ;; where to save sent mail
  (setq notmuch-fcc-dirs '((".*@wohanley.com" . "me/Sent")
                           ("willy.ohanley@gmail.com" . "gmail/Sent")
                           ("william.ohanley@gmail.com" . "gmail-william/Sent")
                           ("wohanley@everyonelegal.ca" . "elc/Sent")
                           ("whohanley@uvic.ca" . "uvic/Sent")))

  (setq message-sendmail-f-is-evil nil)
  (setq message-kill-buffer-on-exit t)
  (setq notmuch-always-prompt-for-sender t)
  (setq notmuch-crypto-process-mime t)
  (setq notmuch-search-oldest-first nil)
  (setq notmuch-archive-tags '("+archived" "-inbox" "-unread"))
  (setq notmuch-message-headers '("To" "Cc" "Subject" "Bcc"))
  (setq notmuch-saved-searches '((:name "inbox" :query "tag:inbox" :key "j" :search-type tree)
                                 (:name "unread" :query "tag:inbox and tag:unread" :key "u")
                                 (:name "spam" :query "tag:spam" :key "s")
                                 (:name "drafts" :query "tag:draft" :key "d")))
  (setq notmuch-hello-sections '(notmuch-hello-insert-saved-searches)))

(global-set-key (kbd "<f2>") #'who/open-email-inbox)

(map!
  :after notmuch
  :map notmuch-search-mode-map
  "G" #'who/mail-fetch-start
  "d" #'who/notmuch-search-uninbox
  "y" #'who/notmuch-search-toggle-unread
  "a" #'notmuch-search-archive-thread
  "H" #'who/notmuch-search-ham
  "h" #'who/notmuch-search-spam
  "c" #'who/org-capture-email
  "r" #'notmuch-search-reply-to-thread
  "R" #'notmuch-search-reply-to-thread-sender)

(map!
  :after notmuch
  :map notmuch-tree-mode-map
  "G" #'who/mail-fetch-start
  "d" #'who/notmuch-tree-uninbox
  "H" #'who/notmuch-tree-ham
  "h" #'who/notmuch-tree-spam
  "y" #'who/notmuch-tree-toggle-unread
  "c" #'who/org-capture-email
  "r" #'who/notmuch-tree-reply-sender
  "R" #'who/notmuch-tree-reply)

(map!
  :after notmuch
  :map notmuch-show-mode-map
  "d"          #'who/notmuch-show-uninbox
  "l"          #'who/notmuch-show-jump-to-latest
  "<tab>"      #'org-next-link
  "<backtab>"  #'org-previous-link
  "C-<return>" #'browse-url-at-point
  "c"          #'who/org-capture-email
  "f"          #'who/notmuch-show-forward-inline
  "F"          #'notmuch-show-forward-message
  "r"          #'who/notmuch-show-reply-sender
  "R"          #'who/notmuch-show-reply)

;; Elfeed

(use-package! elfeed
  :commands elfeed
  :config
  (setq elfeed-curl-max-connections 16)
  (setq elfeed-enclosure-default-dir (expand-file-name "~/Sync/feeds"))

  (add-hook 'elfeed-new-entry-hook #'who/elfeed-on-new-entry)

  ;; search customizations
  (setq elfeed-search-filter "@2-months-ago +unread")
  (setq elfeed-search-date-format '("%m-%d" 5 :left))
  (setq elfeed-search-title-max-width 120)
  (setq elfeed-show-entry-switch #'switch-to-buffer)
  (setq elfeed-search-header-function #'who/elfeed-search-header)
  (setq elfeed-search-print-entry-function #'who/elfeed-search-print-entry)

  ;; Will load elfeed after ef-themes, but might also load a new ef-theme
  ;; after loading elfeed. Account for both by calling
  ;; `who/ef-themes-custom-faces-elfeed' immediately and adding to the hook
  (add-hook 'ef-themes-post-load-hook #'who/ef-themes-custom-faces-elfeed)
  (who/ef-themes-custom-faces-elfeed)

  ;; feed definitions
  (load (expand-file-name "~/.config/elfeed/elfeed-feeds.el"))

  ;; https://github.com/skeeto/elfeed/issues/466#issuecomment-1275327427
  (define-advice elfeed-search--header (:around (oldfun &rest args))
    (if elfeed-db
        (apply oldfun args)
      "No database loaded yet")))

(map! :leader "oe" #'elfeed)

(map!
  :after elfeed
  :map elfeed-search-mode-map
  "RET" #'who/elfeed-search-peek-entry
  "d"   #'elfeed-search-untag-all-unread
  "e"   #'who/elfeed-search-save-entry-audio
  "v"   #'elfeed-search-browse-url)

(map!
  :after elfeed
  :map elfeed-show-mode-map
  "d" #'who/elfeed-show-untag-unread
  "e" #'who/elfeed-show-save-entry-audio
  "v" #'elfeed-show-visit)

;; Magit

(after! magit
  ;; add --allow-empty-message switch to magit-commit
  (transient-append-suffix 'magit-commit "-A" ;; not sure what -A is for, comes from https://github.com/magit/magit/wiki/Converting-popup-modifications-to-transient-modifications
    '("-M" "Allow empty message" "--allow-empty-message")))

;; Theming

(after! unicode-fonts
  ;; not sure how best to deal with kanji, but they look fine for now
  (push "Noto Sans CJK JP" (cadr (assoc "Hiragana" unicode-fonts-block-font-mapping)))
  (push "Noto Sans CJK JP" (cadr (assoc "Katakana" unicode-fonts-block-font-mapping))))

(use-package! ef-themes
  :config
  (setq ef-themes-headings
        '((0 regular 1.5)
          (agenda-date light)
          (agenda-structure light)
          (t regular)))
  (setq ef-themes-mixed-fonts t)
  ;; (setq ef-themes-region '(intense))
  (setq ef-dream-palette-overrides
        '((bg-main "#1a171c"))) ;; make ef-dream background a little more contrasting
)

(use-package! gsettings
  :commands gsettings-get)

(defun who/personalize-fonts ()
  "Personal font selections.

Some stuff tries to do its own fucking around with fonts, e.g. Org mode, so this
needs to be called at init and again afterwards on some occasions."
  ;; Iosevka as main font (system default mono is Commit)
  (set-face-attribute 'default               nil :font "Iosevka" :width 'normal :weight 'normal :height 120)
  (set-face-attribute 'fixed-pitch           nil :font "Iosevka" :width 'normal :weight 'normal :height 120)
  (set-face-attribute 'ef-themes-fixed-pitch nil :font "Iosevka" :width 'normal :weight 'normal :height 120)
  ;; (set-face-attribute 'line-number    nil :font "Iosevka" :width 'normal :weight 'normal :height 120)
  (set-face-attribute 'variable-pitch        nil :height 140) ;; I like proportionals a bit bigger
)

(defun who/buffer-face-mode-human ()
  (interactive)
  ;; I like writing in Alegreya
  (buffer-face-set '(:family "Alegreya Sans" :height 140)))

(defun who/pdf-view-midnight (&optional dark)
  (let ((dark (or dark
                  (string-equal (gsettings-get "org.gnome.desktop.interface" "color-scheme")
                                "prefer-dark"))))
    (pdf-view-midnight-minor-mode dark)))

(defun who/pdf-view-midnight-all (dark)
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when (eq major-mode 'pdf-view-mode)
        (who/pdf-view-midnight dark)))))

(add-hook 'pdf-view-mode-hook #'who/pdf-view-midnight)

(defun who/load-theme (&optional scheme)
  (if (or (equal scheme 'light)
          (string-equal (gsettings-get "org.gnome.desktop.interface" "color-scheme")
                        "prefer-light"))
      (who/load-theme-light)
    (who/load-theme-dark)))

;; The final load-theme call in these dark/light theme loaders will throw an
;; error if org-roam-ui-mode is active, because org-roam-ui-mode t gets saved in
;; Customize as a theme variable, which means when you load a new theme it tries
;; to initialize org-roam-ui-mode again and the server is already running.
;; Doesn't seem to really break anything so I guess it's fine but annoying

(defun who/load-theme-dark ()
  (interactive)
  (setq org-roam-ui-custom-theme
        '((base0 . "#16161c")
          (base1 . "#1a1c23")
          (base2 . "#1d1f27")
          (base3 . "#232530")
          (base4 . "#6a6a6a")
          (base5 . "#f9cec3")
          (base6 . "#f9cbbe")
          (base7 . "#fadad1")
          (base8 . "#fdf0ed")
          (bg . "#232530")
          (bg-alt . "#1c1e26")
          (blue . "#21bfc2")
          (cyan . "#59e3e3")
          (dark-blue . "#25b2bc")
          (dark-cyan . "#27d797")
          (fg . "#c7c9cb")
          (fg-alt . "#fdf0ed")
          (green . "#09f7a0")
          (grey . "#6a6a6a")
          (magenta . "#6c6f93")
          (orange . "#f09383")
          (red . "#e95678")
          (teal . "#87ceeb")
          (violet . "#b877db")
          (yellow . "#fab795")))
  (mapc #'disable-theme custom-enabled-themes)
  (if (equal who/org-context 'work)
      (ef-themes-select 'ef-cherie)
    (ef-themes-select 'ef-dream))
  (who/pdf-view-midnight-all 'dark)
  (when (bound-and-true-p org-roam-ui-mode)
    (org-roam-ui-sync-theme)))

(defun who/load-theme-light ()
  (interactive)
  (setq org-roam-ui-custom-theme
        '((bg . "#fafafa")
          (bg-alt . "#eeeeee")
          (fg . "#2a2a2a")
          (fg-alt . "#454545")
          (red . "#99324b")
          (orange . "#ac4426")
          (yellow . "#9a7500")
          (green . "#4f894c")
          (cyan . "#398eac")
          (blue . "#3b6ea8")
          (violet . "#842879")
          (magenta . "#97365b")
          (base0 . "#fafafa")
          (base1 . "#f5f5f5")
          (base2 . "#eeeeee")
          (base3 . "#e0e0e0")
          (base4 . "#bdbdbd")
          (base5 . "#9e9e9e")
          (base6 . "#757575")
          (base7 . "#616161")
          (base8 . "#424242")
          (teal . "#29838d")
          (grey . "#bdbdbd")
          (dark-blue . "#5272AF")
          (dark-cyan . "#2c7088")))
  (mapc #'disable-theme custom-enabled-themes)
  (if (equal who/org-context 'work)
      (ef-themes-select 'ef-summer)
    (ef-themes-select 'ef-duo-light))
  (who/pdf-view-midnight-all nil)
  (when (bound-and-true-p org-roam-ui-mode)
    (org-roam-ui-sync-theme)))

(who/load-theme)

(who/personalize-fonts)

(setq show-trailing-whitespace t)

;; (setq-default cursor-type 'bar) ;; this doesn't work, I think because of evil mode
(setq-default blink-cursor-delay 0.2)
(setq-default blink-cursor-interval 0.8)
(setq-default blink-cursor-blinks 0)
(blink-cursor-mode 1)

(pixel-scroll-precision-mode 1)
;; pixel-scroll-precision-mode scrolling doesn't respect `next-screen-context-lines', so use my
;; custom scrolling function that does
;; (setq pixel-scroll-precision-interpolate-page t)
(setq next-screen-context-lines 10)
(define-key pixel-scroll-precision-mode-map (kbd "<next>")  #'who/pixel-scroll-interpolate-page-down)
(define-key pixel-scroll-precision-mode-map (kbd "<prior>") #'who/pixel-scroll-interpolate-page-up)
;; Not sure, but feels like this reduces GC pauses while scrolling. GCMH might be a better option
;; in the long run
;; (setq pixel-scroll-precision-interpolation-between-scroll 0.02)
;; (setq mwheel-coalesce-scroll-events t) ;; throttle scroll events. breaks pixel scrolling with touchpad

;; Junk drawer

;; See http://xahlee.info/emacs/misc/emacs_lisp_curly_quote_controversy.html for
;; an account of one of the greatest shitshows in Emacs history
(setq text-quoting-style 'grave)

;; flash the frame to represent a bell
(setq visible-bell t)

(after! alert
  (setq alert-default-style 'libnotify))

(after! flycheck
  (setq-default flycheck-disabled-checkers '(proselint)))

;; Bindings

;; <home> and <end> should do The Right Thing
(global-set-key (kbd "<home>") #'back-to-indentation)
(global-set-key (kbd "<end>")  #'move-end-of-line)

;; line-by-line scrolling
(global-set-key [M-up]   (lambda () (interactive) (who/pixel-scroll-interpolate-lines  8)))
(global-set-key [M-down] (lambda () (interactive) (who/pixel-scroll-interpolate-lines -8)))

;; slurp and barf
(global-set-key (kbd "C-s-<right>")   #'sp-forward-slurp-sexp)
(global-set-key (kbd "C-s-<left>")    #'sp-backward-slurp-sexp)
(global-set-key (kbd "C-S-s-<right>") #'sp-forward-barf-sexp)
(global-set-key (kbd "C-S-s-<left>")  #'sp-backward-barf-sexp)

;; give isearch's binding to consult-line, I never use isearch
(global-set-key (kbd "C-s") #'consult-line)

;; save and kill current buffer (rebound from kill-buffer)
(global-set-key (kbd "C-x k") #'who/save-and-kill-this-buffer)

;; reopen last closed file (like undo close tab in Firefox)
(global-set-key (kbd "C-S-t") #'who/reopen-closed-file)

(map!
  :leader
  "iq"  #'who/quote-curly
  "ldt" #'toggle-debug-on-error
  ;; these two kill-emacs bindings are swapped from Doom defaults
  "qq"  #'save-buffers-kill-emacs
  "qQ"  #'kill-emacs)

;; (defun dotspacemacs/user-config ()

;;   (setq display-line-numbers-width-start t)
;;   (setq display-line-numbers-grow-only t)

;;   (setq epa-pinentry-mode 'loopback)
;;   ;; (pinentry-start)

;;   (add-hook 'text-mode-hook 'spacemacs/toggle-visual-line-navigation-on)

;;   ;; don't look for completions on tab
;;   (setq tab-always-indent t)

;;   ;; magit
;;   ;; disable touchpad scrolling in Magit buffers (lags like hell)
;;   (add-hook 'magit-mode-hook (lambda () (setq-local mwheel-coalesce-scroll-events t)))
;;   (spacemacs/toggle-automatic-symbol-highlight-on)

;;   ;; dtrt-indent does nice dynamic tab length
;;   ;; (dtrt-indent-mode t)

;;   ;; Programming stuff

;;   (with-eval-after-load 'dap-mode
;;     (require 'dap-chrome))

;;   ;; C#
;;   (setq c-basic-offset 4)

;;   ;; Clojure

;;   ;; Get useful stack traces out of the REPL
;;   (setq cider-clojure-cli-global-options "-J-XX:-OmitStackTraceInFastThrow")

;;   ;; Disable CIDER eldoc display, clojure-lsp can handle it
;;   (setq cider-eldoc-display-for-symbol-at-point nil)

;;   (setq clojure-indent-style 'align-arguments)

;;   ;; Turn off font-lock syntax highlighting (mostly) and use rainbow-blocks
;;   ;;(setq clojure-font-lock-keywords) ;; what is this supposed to be set to?
;;   (require 'rainbow-blocks)
;;   (add-hook 'clojure-mode-hook 'rainbow-blocks-mode)

;;   ;; use local eslint
;;   (add-hook 'flycheck-mode-hook 'who/flycheck-use-eslint-from-node-modules)

;;   ;; Go

;;   (setq flycheck-gometalinter-fast t)
;;   (setq flycheck-gometalinter-deadline "10s")
;;   (setq flycheck-gometalinter-disable-linters '("gocyclo"))

;;   ;; Less

;;   (add-to-list 'auto-mode-alist '("\\.less\\'" . less-css-mode))
;;   (add-to-list 'auto-mode-alist '("\\.less\\'" . flycheck-mode))

;;   ;; Prolog

;;   (add-to-list 'auto-mode-alist '("\\.pl\\'" . prolog-mode))

;;   ;; Python

;;   ;; anaconda-mode can fuck itself
;;   (remove-hook 'anaconda-mode-response-read-fail-hook
;;                'anaconda-mode-show-unreadable-response)

;;   ;; Typst

;;   (use-package typst-mode
;;     :commands typst-mode)

;;   ;; Web

;;   (setq js-indent-level 2)
;;   (setq css-indent-offset 2)
;;   (setq web-mode-code-indent-offset 2)
;;   (setq web-mode-css-indent-offset 2)
;;   (setq web-mode-markup-indent-offset 2)
;;   (setq lsp-html-format-enable nil)
;;   ;; (setq lsp-html-format-wrap-line-length 0)

;;   ;; (use-package web-mode
;;   ;;   :config
;;   ;;   (setq web-mode-enable-auto-indentation nil)
;;   ;;   (setq web-mode-code-indent-offset 2)
;;   ;;   (setq web-mode-script-padding 2)
;;   ;;   (setq web-mode-css-indent-offset 2)
;;   ;;   (setq web-mode-style-padding 2)
;;   ;;   (setq web-mode-markup-indent-offset 2)
;;   ;;   (define-key web-mode-map (kbd "M-m m i l") #'who/html-insert-link))

;;   ;; (add-to-list 'auto-mode-alist '("\\.mak\\'" . web-mode))

;;   ;; PDF

;;   ;; store pdf-view-restore data in central location, keyed by full paths
;;   (setq pdf-view-restore-filename "~/.emacs.d/pdf-view-restore")
;;   (setq use-file-base-name-flag nil)

;;   ;; cursor blinking is annoying in pdf-view-mode, and turning it off needs some
;;   ;; finagling thanks to Spacemacs/Evil. See
;;   ;; https://github.com/syl20bnr/spacemacs/issues/1543,
;;   ;; https://github.com/syl20bnr/spacemacs/issues/13393
;;   (add-hook 'pdf-view-mode-hook
;;             (lambda ()
;;               (set (make-local-variable
;;                     'evil-emacs-state-cursor)
;;                    (list nil))))

;;   ;; PDFs open too small without this
;;   (add-hook 'pdf-view-mode-hook 'pdf-view-fit-width-to-window)
;;   ;; swoop doesn't work in pdf-view-mode, use isearch instead. Need to create a
;;   ;; whole ass minor mode to do this because of Spacemacs fucking with keymaps
;;   ;; via evil - emulation-mode-map-alists is the only thing I can find that will
;;   ;; actually override Spacemacs globals.
;;   (defvar who/pdf-view-override-mode-map (make-sparse-keymap))
;;   (define-key who/pdf-view-override-mode-map (kbd "C-s")     #'isearch-forward)
;;   (define-key who/pdf-view-override-mode-map (kbd "M-m s s") #'isearch-forward)
;;   (define-minor-mode who/pdf-view-override-mode
;;     "A minor mode to override Spacemacs keybindings in pdf-view-mode."
;;     :lighter ""
;;     :keymap who/pdf-view-override-mode-map)
;;   (add-to-list 'emulation-mode-map-alists (list (cons 'who/pdf-view-override-mode who/pdf-view-override-mode-map)))
;;   (add-hook 'pdf-view-mode-hook #'who/pdf-view-override-mode)

;;   ;; so "# -*- coding: utf8 -*-" works
;;   (define-coding-system-alias 'utf8 'utf-8)

;;   ;; bug fix: https://github.com/syl20bnr/spacemacs/issues/5435#issuecomment-195862080
;;   (add-hook 'spacemacs-buffer-mode-hook
;;             (lambda ()
;;               (set (make-local-variable 'mouse-1-click-follows-link) nil)))

;;   ;; https://github.com/tkf/emacs-request/issues/92
;;   (setq request-backend 'url-retrieve))

(defun who/pixel-scroll-interpolate-lines (lines)
  (interactive)
  (pixel-scroll-precision-interpolate (* lines (frame-char-height)) nil 1))

(defun who/pixel-scroll-interpolate-page (up)
  "Interpolate a scroll downward (or, with `up', upward) by almost one page."
  (pixel-scroll-precision-interpolate (* (- (window-text-height nil t)
                                            (* next-screen-context-lines
                                               (frame-char-height)))
                                         (if up 1 -1))
                                      nil 1))

(defun who/pixel-scroll-interpolate-page-down ()
  (interactive)
  (who/pixel-scroll-interpolate-page nil))

(defun who/pixel-scroll-interpolate-page-up ()
  (interactive)
  (who/pixel-scroll-interpolate-page 'up))

(defun who/save-and-kill-this-buffer (arg)
  "Kill the current buffer. Save modifications first unless prefix argument passed."
  (interactive "P")
  (if (and (not arg) (buffer-file-name) (buffer-modified-p)) (save-buffer))
  (kill-buffer (current-buffer)))

(defun who/reopen-closed-file ()
  "Re-open the last closed file.

See https://stackoverflow.com/questions/10394213/emacs-reopen-previous-killed-buffer"
  (interactive)
  (if recentf-list
      (let ((active-files (cl-loop for buf in (buffer-list)
                                   when (buffer-file-name buf) collect it)))
        (cl-loop for file in recentf-list
                 unless (member file active-files) return (find-file file)))
    (message "recentf-list empty")))

(defun who/sanitize-file-name (fname)
  "Remove awkward or illegal characters from file name."
  (replace-regexp-in-string "/\\|[^[:alnum:]\\.-_ ]" "_" (s-replace "?" "_" fname)))

(defun who/quote-curly (prefix)
  "Surround region, else current word, else empty string, with “curly quotes”. With
prefix, use ‘single quotes’."
  (interactive "P")
  (let ((left-quote  (if prefix "‘" "“"))
        (right-quote (if prefix "’" "”")))
    (cond
     ;; surround active region
     ((use-region-p)
      (let ((beg (region-beginning))
            (end (region-end)))
        (goto-char beg)
        (insert left-quote)
        (goto-char end)
        (forward-char 1)
        (insert right-quote)))

     ;; surround the current word
     ((looking-at "\\w")
      (save-excursion
        (forward-word)
        (insert right-quote)
        (backward-word)
        (insert left-quote)))

     ;; insert empty quote pair if not on a word
     (t
      (progn
        (insert left-quote)
        (insert right-quote)
        (backward-char))))))

(defun who/replace-ligatures ()
  "Replace common English ligatures with their multi-character representations.
Note that there are some non-English ligatures this doesn't replace."
  (interactive)
  (replace-string "ﬀ" "ff")
  (replace-string "ﬃ" "ffi")
  (replace-string "ﬄ" "ffl")
  (replace-string "ﬁ" "fi")
  (replace-string "ﬂ" "fl")
  (replace-string "ﬆ" "st"))

(defun who/replace-newlines (start end)
  (interactive "r")
  (replace-string-in-region "\n" " " start end))

(defun who/replace-with-unknown ()
  (interactive)
  (if (not (use-region-p))
      (message "No selection.")
    (let ((beg (region-beginning))
          (end (region-end)))
      (replace-regexp-in-region "." "�" beg end))))

(defun who/html-insert-link (start end)
  (interactive "r")
  (if (not (use-region-p))
      (message "No selection.")
    (progn
      (goto-char end)
      (insert "</a>")
      (goto-char start)
      (insert "<a href=\"\">")
      (backward-char 2))))

;; xdg-open and advising find-file per https://emacs.stackexchange.com/questions/3105/how-to-use-an-external-program-as-the-default-way-to-open-pdfs-from-emacs

(defun who/xdg-open (filename)
  (interactive "Filename: ")
  (let ((process-connection-type))
    (start-process "" nil "xdg-open" (expand-file-name filename))))

(defun who/find-file-auto (orig-fun &rest args)
  (let ((filename (car args)))
    (if (cl-find-if
         (lambda (regexp) (string-match regexp filename))
         '("\\.docx?\\'")
         '("\\.xlsx?\\'"))
        (who/xdg-open filename)
      (apply orig-fun args))))

(defun who/rg-files-with-matches (match search-dir)
  (->> (format "rg --no-hidden --files-with-matches '%s' '%s'" match search-dir)
       (shell-command-to-string)
       (s-lines)
       (-filter (lambda (line) (not (s-blank? line))))))

;; Output suppression lifted from Doom
(defmacro quiet! (&rest forms)
  "Run FORMS without generating any output.
This silences calls to `message', `load', `write-region' and anything that
writes to `standard-output'. In interactive sessions this inhibits output to the
echo-area, but not to *Messages*."
  `(if init-file-debug
       (progn ,@forms)
     ,(if noninteractive
          `(letf! ((standard-output (lambda (&rest _)))
                   (defun message (&rest _))
                   (defun load (file &optional noerror nomessage nosuffix must-suffix)
                     (funcall load file noerror t nosuffix must-suffix))
                   (defun write-region (start end filename &optional append visit lockname mustbenew)
                     (unless visit (setq visit 'no-message))
                     (funcall write-region start end filename append visit lockname mustbenew)))
                  ,@forms)
        `(let ((inhibit-message t)
               (save-silently t))
           (prog1 ,@forms (message ""))))))

(defun who/advise-silence (fn &rest args)
  "Generic advisor for silencing noisy functions.
In interactive Emacs, this just inhibits messages from appearing in the
minibuffer. They are still logged to *Messages*.
In tty Emacs, messages are suppressed completely."
  (quiet! (apply fn args)))

(defmacro who/measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time)))
     ,@body
     (message "%.06f" (float-time (time-since time)))))

(defun who/flycheck-use-eslint-from-node-modules ()
  "Configure flycheck to use eslint from node_modules/.bin, if such an executable
exists at such a path somewhere in the parent of the buffer's file. See
http://emacs.stackexchange.com/questions/21205/flycheck-with-file-relative-eslint-executable"
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      (expand-file-name "node_modules/eslint/bin/eslint.js"
                                        root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))

;; Autokey

(defun who/autokey-phrase-add ()
  "Add a phrase to AutoKey configuration."
  (interactive)
  (let* ((phrase (read-string "Phrase: "))
         (phrase-title-case (s-capitalized-words phrase))
         (abbr (read-string "Abbreviation: "))
         (abbr-title-case (s-capitalized-words abbr)))
    (with-temp-file (f-join "~/.config/autokey/data/phrases/" (concat phrase-title-case ".txt"))
      (insert phrase))
    (with-temp-file (f-join "~/.config/autokey/data/phrases/" (concat phrase-title-case ".json"))
      (insert (format "{
    \"modes\": [
        1
    ],
    \"usageCount\": 1,
    \"showInTrayMenu\": false,
    \"abbreviation\": {
        \"abbreviations\": [
            \"%s\",
            \"%s\"
        ],
        \"backspace\": true,
        \"ignoreCase\": false,
        \"immediate\": false,
        \"triggerInside\": false,
        \"wordChars\": \"[\\\\w]\"
    },
    \"hotkey\": {
        \"modifiers\": [],
        \"hotKey\": null
    },
    \"filter\": {
        \"regex\": null,
        \"isRecursive\": false
    },
    \"description\": \"%s\",
    \"prompt\": false,
    \"omitTrigger\": false,
    \"type\": \"phrase\",
    \"matchCase\": true,
    \"sendMode\": \"<shift>+<insert>\"
}"
                      abbr
                      abbr-title-case
                      phrase-title-case)))))

;; Hypothesis

(defun who/hyp-get-annotations (params)
  (let* ((data (map-merge 'alist
                          `(("limit" . ,200))
                          params)))
    (promise-chain
      (promise:request-with-args "https://api.hypothes.is/api/search"
        (list :type "GET"
              :params data
              :headers `(("Authorization" . ,(format "Bearer %s" (f-read-text "~/.config/hypothesis/api-token")))
                         ("Content-Type" . "application/json")
                         ("Accept" . "application/vnd.hypothesis.v1+json"))
              :parser 'json-read))
      (then
        (lambda (response)
          (let ((rows (-map #'identity (alist-get 'rows response))))
            (-map
              (lambda (row)
                (let* ((selectors (-as-> row x
                                         (alist-get 'target x)
                                         (aref x 0)
                                         (alist-get 'selector x)))
                       (position-selector (-first
                                            (lambda (selector)
                                              (string= "TextPositionSelector" (alist-get 'type selector)))
                                            (-map #'identity selectors)))
                       (quote-selector (-first
                                         (lambda (selector)
                                           (string= "TextQuoteSelector" (alist-get 'type selector)))
                                         (-map #'identity selectors))))
                  `((id . ,(alist-get 'id row))
                    (title . ,(alist-get 'exact quote-selector))
                    (location-start . ,(alist-get 'start position-selector)))))
              rows)))))))

(defun who/hyp-to-org (uri)
  "Download data from hypothes.is and insert it into an `org-mode' buffer."
  (promise-chain (who/hyp-get-annotations `(("uri" . ,uri)))
    (then
      (lambda (annotations)
        (with-current-buffer (get-buffer-create "*hypothesis*")
          (delete-region (point-min) (point-max))
          (org-mode)
          (insert "* Notes")
          (org-entry-put nil "HYPOTHESIS_URI" uri)
          (org-end-of-subtree)
          (insert "\n\n")
          (dolist (annot (sort annotations
                               (lambda (a1 a2)
                                 (< (or (alist-get 'location-start a1) 0)
                                    (or (alist-get 'location-start a2) 0)))))
            (insert "** " (org-trim (replace-regexp-in-string "\n" " " (alist-get 'title annot))))
            (org-entry-put nil "HYPOTHESIS_ID" (alist-get 'id annot))
            (org-end-of-subtree)
            (insert "\n\n")))
        (switch-to-buffer "*hypothesis*")
        (goto-char (point-min))))
    (promise-catch
      (lambda (reason)
        (message "failed to retrieve annotations: %s" reason)))))

(defun who/org-roam-ref-hyp-get-annotations ()
  (interactive)
  (require 'promise)
  (when-let ((ref-url (org-entry-get-with-inheritance "ROAM_REFS")))
    (who/hyp-to-org ref-url)))
