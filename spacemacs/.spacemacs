;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. "~/.mycontribs/")
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(lua
     rust
     (auto-completion :variables
                      auto-completion-return-key-behavior nil
                      auto-completion-tab-key-behavior 'complete)
     (bibtex :variables
             bibtex-dialect 'biblatex)
     (clojure :variables
              clojure-enable-linters 'clj-kondo)
     (csharp :variables csharp-backend nil)
     csv
     dap
     ;; deft
     ;; elixir
     emacs-lisp
     epub
     (git :variables
          git-enable-magit-delta-plugin t)
     helpful
     python
     (elm :variables
          elm-format-on-save t)
     (go :variables
         go-use-gometalinter t
         go-tab-width 4)
     (helm :variables
           helm-move-to-line-cycle-in-source nil)
     (html :variables
           css-enable-lsp t
           html-enable-lsp t
           less-enable-lsp t
           scss-enable-lsp t)
     ;; ivy
     (latex :variables
            latex-enable-auto-fill nil)
     lsp
     (lua :variables
          lua-backend 'lsp
          lua-lsp-server 'lua-language-server)
     markdown
     (notdeft :variables
              notdeft-directories '("~/org/zettelkasten"
                                    "~/lawyer/org/zettelkasten/pages"
                                    "~/lawyer/org/zettelkasten/journals")
              ;; Ignore property drawers at the start of files (as created by org-roam)
              notdeft-allow-org-property-drawers t)
     (org :variables
          org-extend-today-until 3
          org-id-link-to-org-use-id 'create-if-interactive
          org-enable-notifications t
          ;; org-start-notification-daemon-on-startup t
          org-enable-roam-support t
          org-enable-roam-ui t)
     pdf
     python
     react
     ruby
     shell-scripts
     (spell-checking :variables
                     spell-checking-enable-by-default nil)
     sql
     syntax-checking
     systemd
     tern
     ;; theming ;; just use ef-themes stuff directly, it's fine
     ;; (treemacs :variables
     ;;           treemacs-use-git-mode 'deferred
     ;;           treemacs-use-all-the-icons-theme t)
     typescript
     version-control
     who-dired
     who-elfeed
     who-email
     who-language-models
     who-nix
     who-org
     yaml
     zotxt
     ;; ----------------------------------------------------------------
     ;; Example of useful layers you may want to use right away.
     ;; Uncomment some layer names and press `SPC f e R' (Vim style) or
     ;; `M-m f e R' (Emacs style) to install them.
     ;; ----------------------------------------------------------------
     ;; auto-completion
     ;; better-defaults
     ;; lsp
     ;; multiple-cursors
     ;; (shell :variables
     ;;        shell-default-height 30
     ;;        shell-default-position 'bottom)
     ;; treemacs
     )

   ;; List of additional packages that will be installed without being wrapped
   ;; in a layer (generally the packages are installed only and should still be
   ;; loaded using load/require/use-package in the user-config section below in
   ;; this file). If you need some configuration for these packages, then
   ;; consider creating a layer. You can also put the configuration in
   ;; `dotspacemacs/user-config'. To use a local version of a package, use the
   ;; `:location' property: '(your-package :location "~/path/to/your-package/")
   ;; Also include the dependencies as they will not be resolved automatically.
   dotspacemacs-additional-packages '(casual-dired
                                      daemons
                                      dtrt-indent
                                      ef-themes
                                      ;; (elfeed :location (recipe :fetcher file
                                      ;;                           :path "~/.emacs.d/private/local/elfeed/elfeed.el"))
                                      esxml
                                      gsettings
                                      ink-mode
                                      (minimap :variables
                                               minimap-window-location 'right)
                                      org-wild-notifier
                                      rainbow-blocks
                                      sqlite3
                                      sudo-edit
                                      suggest
                                      (typst-mode :location (recipe :fetcher github
                                                                    :repo "Ziqi-Yang/typst-mode.el"))
                                      web-server)

   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '()

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need to
   ;; compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;;
   ;; WARNING: pdumper does not work with Native Compilation, so it's disabled
   ;; regardless of the following setting when native compilation is in effect.
   ;;
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; Name of executable file pointing to emacs 27+. This executable must be
   ;; in your PATH.
   ;; (default "emacs")
   dotspacemacs-emacs-pdumper-executable-file "emacs"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=$HOME/.emacs.d/.cache/dumps/spacemacs-27.1.pdmp
   ;; (default (format "spacemacs-%s.pdmp" emacs-version))
   dotspacemacs-emacs-dumper-dump-file (format "spacemacs-%s.pdmp" emacs-version)

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; Set `read-process-output-max' when startup finishes.
   ;; This defines how much data is read from a foreign process.
   ;; Setting this >= 1 MB should increase performance for lsp servers
   ;; in emacs 27.
   ;; (default (* 1024 1024))
   dotspacemacs-read-process-output-max (* 1024 1024)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. Spacelpa is currently in
   ;; experimental state please use only for testing purposes.
   ;; (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default t)
   dotspacemacs-verify-spacelpa-archives t

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'emacs

   ;; If non-nil show the version string in the Spacemacs buffer. It will
   ;; appear as (spacemacs version)@(emacs version)
   ;; (default t)
   dotspacemacs-startup-buffer-show-version t

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official

   ;; Scale factor controls the scaling (size) of the startup banner. Default
   ;; value is `auto' for scaling the logo automatically to fit all buffer
   ;; contents, to a maximum of the full image height and a minimum of 3 line
   ;; heights. If set to a number (int or float) it is used as a constant
   ;; scaling factor for the default logo size.
   dotspacemacs-startup-banner-scale 'auto

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `recents-by-project' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   ;; The exceptional case is `recents-by-project', where list-type must be a
   ;; pair of numbers, e.g. `(recents-by-project . (7 .  5))', where the first
   ;; number is the project limit and the second the limit on the recent files
   ;; within a project.
   dotspacemacs-startup-lists '((recents . 10)
                                (projects . 6))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Show numbers before the startup list lines. (default t)
   dotspacemacs-show-startup-list-numbers t

   ;; The minimum delay in seconds between number key presses. (default 0.4)
   dotspacemacs-startup-buffer-multi-digit-delay 0.4

   ;; If non-nil, show file icons for entries and headings on Spacemacs home buffer.
   ;; This has no effect in terminal or if "all-the-icons" package or the font
   ;; is not installed. (default nil)
   dotspacemacs-startup-buffer-show-icons t

   ;; Default major mode for a new empty buffer. Possible values are mode
   ;; names such as `text-mode'; and `nil' to use Fundamental mode.
   ;; (default `text-mode')
   dotspacemacs-new-empty-buffer-major-mode 'text-mode

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; If non-nil, *scratch* buffer will be persistent. Things you write down in
   ;; *scratch* buffer will be saved and restored automatically.
   dotspacemacs-scratch-buffer-persistent nil

   ;; If non-nil, `kill-buffer' on *scratch* buffer
   ;; will bury it instead of killing.
   dotspacemacs-scratch-buffer-unkillable nil

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great with 2
   ;; themes variants, one dark and one light)
   dotspacemacs-themes (if (string-equal "'prefer-light'"
                                         (string-trim
                                          (shell-command-to-string "gsettings get org.gnome.desktop.interface color-scheme")))
                           '(ef-duo-light ef-dream)
                         '(ef-dream ef-duo-light))

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator wave :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font or prioritized list of fonts. The `:size' can be specified as
   ;; a non-negative integer (pixel size), or a floating-point (point size).
   ;; Point size is recommended, because it's device independent. (default 10.0)
   dotspacemacs-default-font '("Iosevka"
                               :size 12.0
                               :weight normal
                               :width normal)
   ;;                             ;; :powerline-scale 1.1)

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m" for terminal mode, "<M-return>" for GUI mode).
   ;; Thus M-RET should work as leader key in both GUI and terminal modes.
   ;; C-M-m also should work in terminal mode, but not in GUI mode.
   dotspacemacs-major-mode-emacs-leader-key (if window-system "<M-return>" "C-M-m")

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; It is also possible to use a posframe with the following cons cell
   ;; `(posframe . position)' where position can be one of `center',
   ;; `top-center', `bottom-center', `top-left-corner', `top-right-corner',
   ;; `top-right-corner', `bottom-left-corner' or `bottom-right-corner'
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default t) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup t

   ;; If non-nil the frame is undecorated when Emacs starts up. Combine this
   ;; variable with `dotspacemacs-maximized-at-startup' to obtain fullscreen
   ;; without external boxes. Also disables the internal border. (default nil)
   dotspacemacs-undecorated-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes the
   ;; transparency level of a frame background when it's active or selected. Transparency
   ;; can be toggled through `toggle-background-transparency'. (default 90)
   dotspacemacs-background-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Show the scroll bar while scrolling. The auto hide time can be configured
   ;; by setting this variable to a number. (default t)
   dotspacemacs-scroll-bar-while-scrolling nil

   ;; Control line numbers activation.
   ;; If set to `t', `relative' or `visual' then line numbers are enabled in all
   ;; `prog-mode' and `text-mode' derivatives. If set to `relative', line
   ;; numbers are relative. If set to `visual', line numbers are also relative,
   ;; but only visual lines are counted. For example, folded lines will not be
   ;; counted and wrapped lines are counted as multiple lines.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :visual nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; When used in a plist, `visual' takes precedence over `relative'.
   ;; (default nil)
   dotspacemacs-line-numbers '(:relative nil
                               :visual nil
                               ;; line numbering breaks org-roam buffers for some reason, see https://github.com/syl20bnr/spacemacs/issues/14969
                               :disabled-for-modes org-mode org-roam-mode)

   ;; Code folding method. Possible values are `evil', `origami' and `vimish'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil and `dotspacemacs-activate-smartparens-mode' is also non-nil,
   ;; `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil smartparens-mode will be enabled in programming modes.
   ;; (default t)
   dotspacemacs-activate-smartparens-mode t

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc...
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; The backend used for undo/redo functionality. Possible values are
   ;; `undo-tree', `undo-fu' and `undo-redo', see also `evil-undo-system'.
   ;; Note that saved undo history does not get transferred when changing
   ;; from undo-tree to undo-fu or undo-redo.
   ;; The default is currently 'undo-tree, but it will likely be changed
   ;; and at some point removed because undo-tree is not maintained anymore.
   dotspacemacs-undo-system 'undo-tree

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; If nil then Spacemacs uses default `frame-title-format' to avoid
   ;; performance issues, instead of calculating the frame title by
   ;; `spacemacs/title-prepare' all the time.
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Color highlight trailing whitespace in all prog-mode and text-mode derived
   ;; modes such as c++-mode, python-mode, emacs-lisp, html-mode, rst-mode etc.
   ;; (default t)
   dotspacemacs-show-trailing-whitespace nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'changed

   ;; If non-nil activate `clean-aindent-mode' which tries to correct
   ;; virtual indentation of simple modes. This can interfere with mode specific
   ;; indent handling like has been reported for `go-mode'.
   ;; If it does deactivate it here.
   ;; (default t)
   dotspacemacs-use-clean-aindent-mode nil

   ;; Accept SPC as y for prompts if non-nil. (default nil)
   dotspacemacs-use-SPC-as-y nil

   ;; If non-nil shift your number row to match the entered keyboard layout
   ;; (only in insert state). Currently supported keyboard layouts are:
   ;; `qwerty-us', `qwertz-de' and `querty-ca-fr'.
   ;; New layouts can be added in `spacemacs-editing' layer.
   ;; (default nil)
   dotspacemacs-swap-number-row nil

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil

   ;; If nil the home buffer shows the full path of agenda items
   ;; and todos. If non-nil only the file name is shown.
   dotspacemacs-home-shorten-agenda-source nil

   ;; If non-nil then byte-compile some of Spacemacs files.
   dotspacemacs-byte-compile t))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
  (setq custom-file "~/.config/spacemacs/custom.el")
  (load custom-file)
  ;; Good for performance, see https://tony-zorman.com/posts/2022-10-22-emacs-potpourri.html
  (setq frame-inhibit-implied-resize t))

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump.")

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."
  (setq warning-minimum-level :error)

  ;; GC after Emacs loses focus
  (defvar who/blur-gc-timer nil)
  (add-function :after after-focus-change-function #'who/focus-change-gc)

  (setq calendar-location-name "Vancouver, BC")
  (setq calendar-latitude 49.2827)
  (setq calendar-longitude -123.1207)
  ;; (setenv "TZ" "Canada/Pacific")

  (setq alert-default-style 'libnotify)

  ;; (setq-default cursor-type 'bar) ;; this doesn't work, I think because of evil mode
  (setq-default blink-cursor-delay 0.2)
  (setq-default blink-cursor-interval 0.8)
  (setq-default blink-cursor-blinks 0)
  (blink-cursor-mode)

  ;; see https://www.masteringemacs.org/article/demystifying-emacs-window-manager
  (setq switch-to-buffer-in-dedicated-window 'pop)

  (setq undo-tree-auto-save-history t)
  ;; Centralize undo tree history files (instead of making them neighbours)
  (setq undo-tree-history-directory-alist '(("." . (locate-user-emacs-file "undo-tree-hist/"))))
  (advice-add #'undo-tree-save-history :around #'who/advise-silence)
  (add-hook 'find-file-hook #'who/undo-disable)

  (setq oauth2-auto-plstore "/home/wohanley/.config/emacs/oauth2-auto.plist")
  (setq plstore-cache-passphrase-for-symmetric-encryption t)

  (setq display-line-numbers-width-start t)
  (setq display-line-numbers-grow-only t)

  ;; don't explode when opening files with long lines
  (global-so-long-mode 1)
  (setq-default bidi-paragraph-direction 'left-to-right) ;; https://200ok.ch/posts/2020-09-29_comprehensive_guide_on_handling_long_lines_in_emacs.html

  (setq show-trailing-whitespace t)

  (who/personalize-fonts)

  (require 'ef-themes)
  (setq ef-themes-headings
        '((0 regular 1.5)
          (agenda-date light)
          (agenda-structure light)
          (t regular)))
  (setq ef-themes-mixed-fonts t)
  (setq ef-themes-region '(intense))
  (setq ef-dream-palette-overrides
        '((bg-main "#1a171c"))) ;; make ef-dream background a little more contrasting

  (use-package gsettings
    :commands gsettings-get)

  (setq mail-host-address "wohanley.com")
  (add-hook 'message-mode-hook #'turn-off-auto-fill)

  (setq epa-pinentry-mode 'loopback)
  ;; (pinentry-start)

  (add-hook 'text-mode-hook 'spacemacs/toggle-visual-line-navigation-on)

  ;; flash the frame to represent a bell
  (setq visible-bell t)

  ;; kill frame but keep daemon running per https://medium.com/@bobbypriambodo/blazingly-fast-spacemacs-with-persistent-server-92260f2118b7
  (spacemacs/set-leader-keys
    "q q" 'spacemacs/prompt-kill-emacs
    "q f" 'who/save-buffers-kill-frame
    "q F" 'spacemacs/frame-killer)

  (setq fill-column 100)

  (setq kill-whole-line t)

  ;; don't look for completions on tab
  (setq tab-always-indent t)

  ;; follow symlinks to source-controlled files without asking
  (setq vc-follow-symlinks t)

  ;; anything that writes to the buffer while the region is active will
  ;; overwrite it (like in most editors)
  (delete-selection-mode 1)

  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier 'super)

  ;; spacemacs increases undo-limit to avoid frequent GCs, but parsing undo-tree
  ;; history gets quite slow if it's too big. set undo-tree-limit lower
  (setq undo-tree-limit 320000)

  ;; <home> and <end> should do The Right Thing
  (global-unset-key (kbd "<end>"))
  (global-set-key (kbd "<end>") 'move-end-of-line)
  (global-unset-key (kbd "<home>"))
  (global-set-key (kbd "<home>") 'back-to-indentation)

  ;; line-by-line scrolling
  (global-set-key [M-up] (lambda () (interactive) (scroll-down 5)))
  (global-set-key [M-down] (lambda () (interactive) (scroll-up 5)))

  ;; give isearch's binding to swoop, I never use isearch
  (global-set-key (kbd "C-s") #'helm-swoop)

  ;; slurp and barf
  (global-set-key (kbd "C-s-<right>") 'sp-forward-slurp-sexp)
  (global-set-key (kbd "C-s-<left>") 'sp-backward-slurp-sexp)
  (global-set-key (kbd "C-S-s-<right>") 'sp-forward-barf-sexp)
  (global-set-key (kbd "C-S-s-<left>") 'sp-backward-barf-sexp)

  ;; save and kill current buffer (rebound from kill-buffer)
  (global-set-key (kbd "C-x k") 'who/save-and-kill-this-buffer)

  ;; reopen last closed file (like undo close tab in Firefox)
  (global-set-key (kbd "C-S-t") #'who/reopen-closed-file)

  (spacemacs/set-leader-keys "w q" #'quit-window)

  ;; replace default buffer list with more complete list
  (global-set-key (kbd "C-x b") 'helm-mini)
  ;; sometimes I'm slow off C and I don't care about the default C-x C-b binding
  (global-set-key (kbd "C-x C-b") 'helm-mini)

  ;; magit
  (global-set-key (kbd "C-x g") 'magit-status)
  ;; disable touchpad scrolling in Magit buffers (lags like hell)
  (add-hook 'magit-mode-hook (lambda () (setq-local mwheel-coalesce-scroll-events t)))

  ;; add --allow-empty-message switch to magit-commit
  (spacemacs|use-package-add-hook magit
    :post-config
    (setq magit-bury-buffer-function 'magit-mode-quit-window)
    (transient-append-suffix 'magit-commit "-A" ;; not sure what -A is for, comes from https://github.com/magit/magit/wiki/Converting-popup-modifications-to-transient-modifications
      '("-M" "Allow empty message" "--allow-empty-message")))

  ;; fold bindings
  (spacemacs/set-leader-keys "z c" 'evil-close-fold)
  (spacemacs/set-leader-keys "z o" 'evil-open-fold)

  ;; highlight symbol at point
  (spacemacs/toggle-automatic-symbol-highlight-on)

  ;; dtrt-indent does nice dynamic tab length
  ;; (dtrt-indent-mode t)

  (use-package casual-dired
    :bind (:map dired-mode-map ("C-o" . 'casual-dired-tmenu)))

  (with-eval-after-load 'dired
    (setq dired-create-destination-dirs 'ask)
    (setq dired-create-destination-dirs-on-trailing-dirsep t))

  ;; Programming stuff

  (with-eval-after-load 'dap-mode
    (require 'dap-chrome))

  ;; C#
  (setq c-basic-offset 4)

  ;; Clojure

  ;; Get useful stack traces out of the REPL
  (setq cider-clojure-cli-global-options "-J-XX:-OmitStackTraceInFastThrow")

  ;; Disable CIDER eldoc display, clojure-lsp can handle it
  (setq cider-eldoc-display-for-symbol-at-point nil)

  (setq clojure-indent-style 'align-arguments)

  ;; Turn off font-lock syntax highlighting (mostly) and use rainbow-blocks
  ;;(setq clojure-font-lock-keywords) ;; what is this supposed to be set to?
  (require 'rainbow-blocks)
  (add-hook 'clojure-mode-hook 'rainbow-blocks-mode)

  ;; use local eslint
  (add-hook 'flycheck-mode-hook 'who/flycheck-use-eslint-from-node-modules)

  ;; Elisp

  (setq emacs-lisp-docstring-fill-column nil) ;; use default fill-column, elisp ain't special
  ;; elisp autoformat is garbage, but I do like whitespace cleanup
  (remove-hook 'emacs-lisp-mode-hook #'spacemacs//make-elisp-buffer-format-on-save)
  (add-hook 'emacs-lisp-mode-hook
            (lambda ()
              (add-hook 'before-save-hook #'whitespace-cleanup)))

  ;; Use Elisp quoting rules in minibuffer
  (with-eval-after-load 'smartparens
    (sp-local-pair '(minibuffer-mode minibuffer-inactive-mode) "'" nil :actions nil)
    (sp-local-pair '(minibuffer-mode minibuffer-inactive-mode) "`" nil :actions nil))

  ;; Go

  (setq flycheck-gometalinter-fast t)
  (setq flycheck-gometalinter-deadline "10s")
  (setq flycheck-gometalinter-disable-linters '("gocyclo"))

  ;; Ink

  (add-hook 'ink-mode-hook (lambda () (setq tab-width 2)))
  (add-hook 'ink-mode-hook
            (lambda () (if (not ink-inklecate-path)
                           (setq ink-inklecate-path (executable-find "inklecate")))))
  (add-hook 'ink-mode-hook 'flymake-mode)

  ;; Less

  (add-to-list 'auto-mode-alist '("\\.less\\'" . less-css-mode))
  (add-to-list 'auto-mode-alist '("\\.less\\'" . flycheck-mode))

  ;; Prolog

  (add-to-list 'auto-mode-alist '("\\.pl\\'" . prolog-mode))

  ;; Python

  ;; anaconda-mode can fuck itself
  (remove-hook 'anaconda-mode-response-read-fail-hook
               'anaconda-mode-show-unreadable-response)

  ;; Typst

  (use-package typst-mode
    :commands typst-mode)

  ;; Web

  (setq js-indent-level 2)
  (setq css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-markup-indent-offset 2)
  (setq lsp-html-format-enable nil)
  ;; (setq lsp-html-format-wrap-line-length 0)

  ;; (use-package web-mode
  ;;   :config
  ;;   (setq web-mode-enable-auto-indentation nil)
  ;;   (setq web-mode-code-indent-offset 2)
  ;;   (setq web-mode-script-padding 2)
  ;;   (setq web-mode-css-indent-offset 2)
  ;;   (setq web-mode-style-padding 2)
  ;;   (setq web-mode-markup-indent-offset 2)
  ;;   (define-key web-mode-map (kbd "M-m m i l") #'who/html-insert-link))

  ;; (add-to-list 'auto-mode-alist '("\\.mak\\'" . web-mode))

  ;; PDF

  ;; store pdf-view-restore data in central location, keyed by full paths
  (setq pdf-view-restore-filename "~/.emacs.d/pdf-view-restore")
  (setq use-file-base-name-flag nil)

  ;; cursor blinking is annoying in pdf-view-mode, and turning it off needs some
  ;; finagling thanks to Spacemacs/Evil. See
  ;; https://github.com/syl20bnr/spacemacs/issues/1543,
  ;; https://github.com/syl20bnr/spacemacs/issues/13393
  (add-hook 'pdf-view-mode-hook
            (lambda ()
              (set (make-local-variable
                    'evil-emacs-state-cursor)
                   (list nil))))

  ;; PDFs open too small without this
  (add-hook 'pdf-view-mode-hook 'pdf-view-fit-width-to-window)
  ;; swoop doesn't work in pdf-view-mode, use isearch instead. Need to create a
  ;; whole ass minor mode to do this because of Spacemacs fucking with keymaps
  ;; via evil - emulation-mode-map-alists is the only thing I can find that will
  ;; actually override Spacemacs globals.
  (defvar who/pdf-view-override-mode-map (make-sparse-keymap))
  (define-key who/pdf-view-override-mode-map (kbd "C-s")     #'isearch-forward)
  (define-key who/pdf-view-override-mode-map (kbd "M-m s s") #'isearch-forward)
  (define-minor-mode who/pdf-view-override-mode
    "A minor mode to override Spacemacs keybindings in pdf-view-mode."
    :lighter ""
    :keymap who/pdf-view-override-mode-map)
  (add-to-list 'emulation-mode-map-alists (list (cons 'who/pdf-view-override-mode who/pdf-view-override-mode-map)))
  (add-hook 'pdf-view-mode-hook #'who/pdf-view-override-mode)

  ;; so "# -*- coding: utf8 -*-" works
  (define-coding-system-alias 'utf8 'utf-8)

  ;; bug fix: https://github.com/syl20bnr/spacemacs/issues/5435#issuecomment-195862080
  (add-hook 'spacemacs-buffer-mode-hook
            (lambda ()
              (set (make-local-variable 'mouse-1-click-follows-link) nil)))

  (pixel-scroll-precision-mode 1)
  ;; pixel-scroll-precision-mode scrolling doesn't respect `next-screen-context-lines', so use my
  ;; custom scrolling function that does
  ;; (setq pixel-scroll-precision-interpolate-page t)
  (setq next-screen-context-lines 10)
  (define-key pixel-scroll-precision-mode-map (kbd "<next>")  #'who/pixel-scroll-interpolate-down)
  (define-key pixel-scroll-precision-mode-map (kbd "<prior>") #'who/pixel-scroll-interpolate-up)
  (setq evil-move-beyond-eol t) ;; https://github.com/doomemacs/doomemacs/issues/6977#issuecomment-1672088806
  ;; Not sure, but feels like this reduces GC pauses while scrolling. GCMH might be a better option
  ;; in the long run
  (setq pixel-scroll-precision-interpolation-between-scroll 0.02)
  ;; (setq mwheel-coalesce-scroll-events t) ;; throttle scroll events. breaks pixel scrolling with touchpad

  ;; https://github.com/tkf/emacs-request/issues/92
  (setq request-backend 'url-retrieve)

  ;; misc keybindings
  (spacemacs/set-leader-keys "aa" #'who/autokey-phrase-add)
  (spacemacs/set-leader-keys "xq" #'who/quote-curly))

(defun who/pixel-scroll-interpolate-down ()
  "Interpolate a scroll downwards by one almost full page."
  (interactive)
  (pixel-scroll-precision-interpolate (- (- (window-text-height nil t)
                                            (* next-screen-context-lines
                                               (frame-char-height))))
                                      nil 1))

(defun who/pixel-scroll-interpolate-up ()
  "Interpolate a scroll upwards by one almost full page."
  (interactive)
  (pixel-scroll-precision-interpolate (- (window-text-height nil t)
                                         (* next-screen-context-lines
                                            (frame-char-height)))
                                      nil 1))

(defun who/focus-change-gc ()
  "If Emacs loses focus, set a timer to collect garbage in 15 seconds. If Emacs
gains focus, cancel that timer.

See https://config.phundrak.com/emacs/basic-config.html#better-garbage-collection"
  (if (frame-focus-state)
      (when (timerp who/blur-gc-timer)
        (cancel-timer who/blur-gc-timer))
    (setq who/blur-gc-timer (run-with-idle-timer 15 nil #'garbage-collect))))

(defvar who/undo-blocklist
  (list (expand-file-name "~/.elfeed")
        (expand-file-name "~/.emacs.d/.cache"))
  "List of files or directories in which to disable undo-tree.")

(defun who/undo-disable ()
  (when (or (member (buffer-file-name) who/undo-blocklist)
            (-some (lambda (blocked) (f-ancestor-of? blocked (buffer-file-name))) who/undo-blocklist))
    (buffer-disable-undo)))

(defun who/save-and-kill-this-buffer (arg)
  "Kill the current buffer. Save modifications first unless prefix argument passed."
  (interactive "P")
  (if (and (not arg) (buffer-file-name) (buffer-modified-p)) (save-buffer))
  (kill-buffer (current-buffer)))

(defun who/save-buffers-kill-frame ()
  (interactive)
  (save-some-buffers)
  (spacemacs/frame-killer))

(defun who/reopen-closed-file ()
  "Re-open the last closed file.

See https://stackoverflow.com/questions/10394213/emacs-reopen-previous-killed-buffer"
  (interactive)
  (if recentf-list
      (let ((active-files (loop for buf in (buffer-list)
                                when (buffer-file-name buf) collect it)))
        (loop for file in recentf-list
              unless (member file active-files) return (find-file file)))
    (message "recentf-list empty")))

(defun who/sanitize-file-name (fname)
  "Remove awkward or illegal characters from file name."
  (replace-regexp-in-string "/\\|[^[:alnum:]\\.-_ ]" "_" fname))

(defun who/quote-curly (prefix)
  "Surround region, else current word, else empty string, with “curly quotes”. With
prefix, use ‘single quotes’."
  (interactive "P")
  (let ((left-quote  (if prefix "‘" "“"))
        (right-quote (if prefix "’" "”")))
    (cond
     ;; surround active region
     ((use-region-p)
      (let ((beg (region-beginning))
            (end (region-end)))
        (goto-char beg)
        (insert left-quote)
        (goto-char end)
        (forward-char 1)
        (insert right-quote)))

     ;; surround the current word
     ((looking-at "\\w")
      (save-excursion
        (forward-word)
        (insert right-quote)
        (backward-word)
        (insert left-quote)))

     ;; insert empty quote pair if not on a word
     (t
      (progn
        (insert left-quote)
        (insert right-quote)
        (backward-char))))))

(defun who/replace-ligatures ()
  "Replace common English ligatures with their multi-character representations.
Note that there are some non-English ligatures this doesn't replace."
  (interactive)
  (replace-string "ﬀ" "ff")
  (replace-string "ﬃ" "ffi")
  (replace-string "ﬄ" "ffl")
  (replace-string "ﬁ" "fi")
  (replace-string "ﬂ" "fl")
  (replace-string "ﬆ" "st"))

(defun who/replace-newlines (start end)
  (interactive "r")
  (replace-string-in-region "\n" " " start end))

(defun who/html-insert-link (start end)
  (interactive "r")
  (if (not (use-region-p))
      (message "No selection.")
    (progn
      (goto-char end)
      (insert "</a>")
      (goto-char start)
      (insert "<a href=\"\">")
      (backward-char 2))))

;; xdg-open and advising find-file per https://emacs.stackexchange.com/questions/3105/how-to-use-an-external-program-as-the-default-way-to-open-pdfs-from-emacs

(defun who/xdg-open (filename)
  (interactive "Filename: ")
  (let ((process-connection-type))
    (start-process "" nil "xdg-open" (expand-file-name filename))))

(defun who/find-file-auto (orig-fun &rest args)
  (let ((filename (car args)))
    (if (cl-find-if
         (lambda (regexp) (string-match regexp filename))
         '("\\.docx?\\'")
         '("\\.xlsx?\\'"))
        (who/xdg-open filename)
      (apply orig-fun args))))

(defun who/rg-files-with-matches (match search-dir)
  (->> (format "rg --no-hidden --files-with-matches '%s' '%s'" match search-dir)
       (shell-command-to-string)
       (s-lines)
       (-filter (lambda (line) (not (s-blank? line))))))

;; Theme stuff

(defun who/personalize-fonts ()
  "Personal font selections.

Some stuff tries to do its own fucking around with fonts, e.g. Org mode, so this
needs to be called at init and again afterwards on some occasions."
  ;; Iosevka as main font (system default mono is Commit)
  (set-face-attribute 'default               nil :font "Iosevka" :width 'normal :weight 'normal :height 120)
  (set-face-attribute 'fixed-pitch           nil :font "Iosevka" :width 'normal :weight 'normal :height 120)
  (set-face-attribute 'ef-themes-fixed-pitch nil :font "Iosevka" :width 'normal :weight 'normal :height 120)
  ;; (set-face-attribute 'line-number    nil :font "Iosevka" :width 'normal :weight 'normal :height 120)
  (set-face-attribute 'variable-pitch        nil :height 140) ;; I like proportionals a bit bigger
 )

(defun who/buffer-face-mode-human ()
  (interactive)
  ;; I like writing in Alegreya
  (buffer-face-set '(:family "Alegreya Sans" :height 140)))

;; Output suppression lifted from Doom
(defmacro quiet! (&rest forms)
  "Run FORMS without generating any output.
This silences calls to `message', `load', `write-region' and anything that
writes to `standard-output'. In interactive sessions this inhibits output to the
echo-area, but not to *Messages*."
  `(if init-file-debug
       (progn ,@forms)
     ,(if noninteractive
          `(letf! ((standard-output (lambda (&rest _)))
                   (defun message (&rest _))
                   (defun load (file &optional noerror nomessage nosuffix must-suffix)
                     (funcall load file noerror t nosuffix must-suffix))
                   (defun write-region (start end filename &optional append visit lockname mustbenew)
                     (unless visit (setq visit 'no-message))
                     (funcall write-region start end filename append visit lockname mustbenew)))
                  ,@forms)
        `(let ((inhibit-message t)
               (save-silently t))
           (prog1 ,@forms (message ""))))))

(defun who/advise-silence (fn &rest args)
  "Generic advisor for silencing noisy functions.
In interactive Emacs, this just inhibits messages from appearing in the
minibuffer. They are still logged to *Messages*.
In tty Emacs, messages are suppressed completely."
  (quiet! (apply fn args)))

(defmacro who/measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time)))
     ,@body
     (message "%.06f" (float-time (time-since time)))))

(defun who/flycheck-use-eslint-from-node-modules ()
  "Configure flycheck to use eslint from node_modules/.bin, if such an executable
exists at such a path somewhere in the parent of the buffer's file. See
http://emacs.stackexchange.com/questions/21205/flycheck-with-file-relative-eslint-executable"
  (let* ((root (locate-dominating-file
                (or (buffer-file-name) default-directory)
                "node_modules"))
         (eslint (and root
                      (expand-file-name "node_modules/eslint/bin/eslint.js"
                                        root))))
    (when (and eslint (file-executable-p eslint))
      (setq-local flycheck-javascript-eslint-executable eslint))))

(defun who/read-file-contents (filename)
  "Return the contents of the file named FILENAME."
  (with-temp-buffer
    (insert-file-contents filename)
    (buffer-string)))

;; Autokey

(defun who/autokey-phrase-add ()
  "Add a phrase to AutoKey configuration."
  (interactive)
  (let* ((phrase (read-string "Phrase: "))
         (phrase-title-case (s-capitalized-words phrase))
         (abbr (read-string "Abbreviation: "))
         (abbr-title-case (s-capitalized-words abbr)))
    (with-temp-file (f-join "~/.config/autokey/data/phrases/" (concat phrase-title-case ".txt"))
      (insert phrase))
    (with-temp-file (f-join "~/.config/autokey/data/phrases/" (concat phrase-title-case ".json"))
      (insert (format "{
    \"modes\": [
        1
    ],
    \"usageCount\": 1,
    \"showInTrayMenu\": false,
    \"abbreviation\": {
        \"abbreviations\": [
            \"%s\",
            \"%s\"
        ],
        \"backspace\": true,
        \"ignoreCase\": false,
        \"immediate\": false,
        \"triggerInside\": false,
        \"wordChars\": \"[\\\\w]\"
    },
    \"hotkey\": {
        \"modifiers\": [],
        \"hotKey\": null
    },
    \"filter\": {
        \"regex\": null,
        \"isRecursive\": false
    },
    \"description\": \"%s\",
    \"prompt\": false,
    \"omitTrigger\": false,
    \"type\": \"phrase\",
    \"matchCase\": true,
    \"sendMode\": \"<shift>+<insert>\"
}"
                      abbr
                      abbr-title-case
                      phrase-title-case)))))

;; Hypothesis

(defun who/hyp-get-annotations (params)
  (let* ((data (map-merge 'alist
                          `(("limit" . ,200))
                          params)))
    (promise-chain
      (promise:request-with-args "https://api.hypothes.is/api/search"
        (list :type "GET"
              :params data
              :headers `(("Authorization" . ,(format "Bearer %s" (f-read-text "~/.config/hypothesis/api-token")))
                         ("Content-Type" . "application/json")
                         ("Accept" . "application/vnd.hypothesis.v1+json"))
              :parser 'json-read))
      (then
        (lambda (response)
          (let ((rows (-map #'identity (alist-get 'rows response))))
            (-map
              (lambda (row)
                (let* ((selectors (-as-> row x
                                         (alist-get 'target x)
                                         (aref x 0)
                                         (alist-get 'selector x)))
                       (position-selector (-first
                                            (lambda (selector)
                                              (string= "TextPositionSelector" (alist-get 'type selector)))
                                            (-map #'identity selectors)))
                       (quote-selector (-first
                                         (lambda (selector)
                                           (string= "TextQuoteSelector" (alist-get 'type selector)))
                                         (-map #'identity selectors))))
                  `((id . ,(alist-get 'id row))
                    (title . ,(alist-get 'exact quote-selector))
                    (location-start . ,(alist-get 'start position-selector)))))
              rows)))))))

(defun who/hyp-to-org (uri)
  "Download data from hypothes.is and insert it into an `org-mode' buffer."
  (promise-chain (who/hyp-get-annotations `(("uri" . ,uri)))
    (then
      (lambda (annotations)
        (with-current-buffer (get-buffer-create "*hypothesis*")
          (delete-region (point-min) (point-max))
          (org-mode)
          (insert "* Notes")
          (org-entry-put nil "HYPOTHESIS_URI" uri)
          (org-end-of-subtree)
          (insert "\n\n")
          (dolist (annot (sort annotations
                               (lambda (a1 a2)
                                 (< (or (alist-get 'location-start a1) 0)
                                    (or (alist-get 'location-start a2) 0)))))
            (insert "** " (org-trim (replace-regexp-in-string "\n" " " (alist-get 'title annot))))
            (org-entry-put nil "HYPOTHESIS_ID" (alist-get 'id annot))
            (org-end-of-subtree)
            (insert "\n\n")))
        (switch-to-buffer "*hypothesis*")
        (goto-char (point-min))))
    (promise-catch
      (lambda (reason)
        (message "%s" "failed to retrieve annotations: " reason)))))

(defun who/org-roam-ref-hyp-get-annotations ()
  (interactive)
  (when-let ((ref-url (org-entry-get-with-inheritance "ROAM_REFS")))
    (who/hyp-to-org ref-url)))
