(spacemacs/declare-prefix
  "al" "language"
  "alc" "code"
  "alt" "text")

(spacemacs/set-leader-keys
  "ale" #'who/instruct-derive-calendar-event
  "alce" #'who/lm-edit-code
  "alci" #'who/lm-implement-skeleton
  "alte" #'who/lm-edit-text)

(define-key complete-mode-map (kbd "C-c C-c") #'who/instruct-finalize)
(define-key complete-mode-map (kbd "C-c C-k") #'quit-window)
