(require 'savehist)
(savehist-mode 1)

(defvar who/gpt-max-tokens 2000)
(defvar who/gpt-temperature 0)
(defvar who/openai-model-chat "gpt-3.5-turbo")
(defvar who/openai-model-completion "text-davinci-003")
(defvar who/openai-model-edit-code "code-davinci-edit-001")
(defvar who/openai-model-edit-text "text-davinci-edit-001")

;;;
;; Porcelain
;;;

(defun who/lm-edit-code ()
  (interactive)
  (let ((instruction (who/lm-edit-code-read-instruction)))
    (who/lm-run `((:model . :edit-code)
                  (:instruction . ,instruction)))))

(defun who/lm-edit-text ()
  (interactive)
  (let ((instruction (who/lm-edit-text-read-instruction)))
    (who/lm-run `((:model . :edit-text)
                  (:instruction . ,instruction)))))

(defun who/lm-implement-skeleton ()
  (interactive)
  (who/lm-run `((:model . :edit-code)
                (:instruction . "Finish the implementation of the function."))))

(defun who/lm-derive-calendar-event ()
  (interactive)
  (let* ((prompt (who/instruct-chat-make-prompt
                  "Here is an example org-mode calendar event:


* Event title
<2023-04-02 Sun 17:49>
A short summary of the event, including a URL if available.


n.b. The timestamp includes no time zone information.

The user will describe an event to you. You will respond with a corresponding org-mode calendar event. Assume the year is 2023."
                  (buffer-substring-no-properties (region-beginning) (region-end))))
         (answer (who/instruct-openai-chat prompt))
         (calendar-buffer (find-file-other-window "~/org/gtd/calendars/personal.org")))
    (with-current-buffer calendar-buffer
      (goto-char (point-max))
      (insert answer))))

(defun who/lm-orgify ()
  (interactive)
  (let ((instruction
         "org-mode uses * to indicate line depth. Here is an example of org-mode:

* Topic 1
* Topic 2: A
** On the topic of A: B
*** Something about B
** On the topic of A: C

The user will provide you with some free-form text. You will format it reasonably in org-mode. The user may have done a poor job of formatting the text. In particular, if you find OCR errors, unnecessary whitespace, or unnecessary line breaks, remove them."))
    (who/lm-run `((:model . :edit-code)
                  (:instruction . ,instruction)))))

;;;
;; Dispatch
;;;

(defun who/lm-run (command)
  (cl-case (alist-get :model command :complete)
    (:complete (who/lm-run-complete command))
    (:edit-code (who/lm-run-edit command))
    (:edit-text (who/lm-run-edit command))))

;;;
;; Chat
;;;

(defun who/instruct-chat-make-prompt (command input)
  `((("role" . "system")
     ("content" . ,command))
    (("role" . "user")
     ("content" . ,input))))

(defun who/instruct-openai-chat (prompt)
  "Send `prompt' to OpenAI chat API and return the answer string."
  (let ((result nil))
    (request
      "https://api.openai.com/v1/chat/completions"
      :type "POST"
      :data (json-encode `(("messages" . ,prompt)
                           ("model"  . ,who/openai-model-chat)
                           ("max_tokens" . ,who/gpt-max-tokens)
                           ("temperature" . ,who/gpt-temperature)))
      :headers (who/openai-headers)
      :sync t
      :parser 'json-read
      :success (cl-function
                (lambda (&key data &allow-other-keys)
                  (setq result
                        (-as-> data x
                             (alist-get 'choices x)
                             (elt x 0)
                             (alist-get 'message x)
                             (alist-get 'content x)))))
      :error (cl-function (lambda (&rest args &key error-thrown &key data &allow-other-keys)
                 (message "%S: %S" error-thrown data))))
    result))


;;;
;; Completions
;;;

(defun who/instruct-make-completion-prompt (command input)
  (concat "### Instruction ###\n" command "\n\n### Input ###\n" input "\n\n### Output ###\n"))

(defun who/instruct-create-prompt-buffer (command)
  "Create and return a `complete-mode' buffer with current region as input."
  (let* ((input (if (use-region-p)
                    (buffer-substring-no-properties (region-beginning) (region-end))
                  ""))
         (prompt-buffer (generate-new-buffer "*instruct*")))
    (with-current-buffer prompt-buffer
      (complete-mode)
      (insert (who/instruct-make-prompt command input)))
    (switch-to-buffer-other-window prompt-buffer)
    prompt-buffer))

(define-derived-mode complete-mode text-mode "Complete"
  "A mode for preparing and executing LLM completion prompts."
  (setq-local word-wrap t))

(defun who/instruct-finalize ()
  "Send the contents of the prompt buffer to the OpenAI API, then insert the
response."
  (interactive)
  (let ((prompt (buffer-string))
        (output-start (point-max)))
    (when (or (< (length prompt) 2000)
              (yes-or-no-p (format "Prompt is long (%d chars), really send?" (length prompt))))
      (insert (who/instruct-openai prompt))
      (evil-visual-select output-start (point-max)))))

(defun who/instruct-openai-completion (prompt)
  "Send `prompt' string to OpenAI completion API and return the answer string."
  (let ((result nil))
    (request
      "https://api.openai.com/v1/completions"
      :type "POST"
      :data (json-encode `(("prompt" . ,prompt)
                           ("model"  . ,who/openai-model-completion)
                           ("max_tokens" . ,who/gpt-max-tokens)
                           ("temperature" . ,who/gpt-temperature)))
      :headers (who/openai-headers)
      :sync t
      :parser 'json-read
      :success (cl-function
                (lambda (&key data &allow-other-keys)
                  (setq result (alist-get 'text (elt (alist-get 'choices data) 0)))))
      :error (cl-function (lambda (&rest args &key error-thrown &allow-other-keys)
                 (message "Got error: %S" error-thrown))))
    result))

;;;
;; Edit
;;;

(defun who/lm-run-edit (command)
  (if (not (use-region-p))
      (message "Select a region first.")
    (let ((edit-buffer (current-buffer))
          (region-begin (region-beginning))
          (region-end (region-end)))
      (promise-chain
          (who/lm-edit-openai
           `((:input . ,(buffer-substring-no-properties region-begin region-end))
             (:instruction . ,(alist-get :instruction command))))
        (then
         (lambda (result)
           (with-current-buffer (get-buffer-create "*who-language-models*")
             (insert "response: " result))
           (message "now to insert")
           (with-current-buffer edit-buffer
             (message "inserting result")
             (when (not (s-blank? result))
               (message "selecting: " region-begin region-end)
               (evil-visual-select region-begin region-end)
               (insert result)))))))))

(defun who/lm-edit-openai (command)
  "Send command to OpenAI code editing API and return a promise for the edited text."
  (let ((result nil)
        (model (cl-case (alist-get :model command)
                 (:edit-code who/openai-model-edit-code)
                 (t          who/openai-model-edit-text))))
    (promise-chain (promise:request-with-args "https://api.openai.com/v1/edits"
                     (list :type "POST"
                           :data (json-encode `(("input" . ,(alist-get :input command))
                                                ("instruction" . ,(alist-get :instruction command))
                                                ("model" . ,model)
                                                ("temperature" . ,who/gpt-temperature)
                                                ("max_tokens" . ,who/gpt-max-tokens)))
                           :headers (who/openai-headers)
                           :parser 'json-read))
      (then (lambda (data)
              (-as-> data x
                     (alist-get 'choices x)
                     (elt x 0)
                     (alist-get 'text x)))

            (lambda (err)
              (message "error: " (str err))
              err)))))

(defun who/lm-edit-code-read-instruction ()
  "Read an editing instruction with history and completion."
  (let ((cmd (who/completing-read-space "Instruction: " who/lm-edit-code-history nil nil nil 'who/lm-edit-code-history)))
    (if (string-equal cmd "n/a")
        ""
      (string-trim cmd))))

(defvar who/lm-edit-code-history nil
  "A list of instructions previously entered for `who/lm-edit-code'.")
(add-to-list 'savehist-additional-variables 'who/lm-edit-code)

(defun who/lm-edit-text-read-instruction ()
  "Read an editing instruction with history and completion."
  (let ((cmd (who/completing-read-space "Instruction: " who/lm-edit-text-history nil nil nil 'who/lm-edit-text-history)))
    (if (string-equal cmd "n/a")
        ""
      (string-trim cmd))))

(defvar who/lm-edit-text-history nil
  "A list of instructions previously entered for `who/lm-edit-text'.")

;;;
;; Utilities
;;;

(defun who/openai-headers ()
  `(("Authorization" . ,(format "Bearer %s" (f-read-text "~/.config/openai/api-key")))
    ("Content-Type" . "application/json")))

(defun who/completing-read-space (prompt collection &optional predicate require-match initial-input hist def inherit-input-method)
  "Read string in minibuffer with completion, treating space literally.
The arguments are the same as for `completing-read', except that space does not
trigger completion or cycling, but inserts a space character. PROMPT is the
prompt to display, COLLECTION is the list of possible completions, and the
optional arguments PREDICATE REQUIRE-MATCH INITIAL-INPUT HIST DEF and
INHERIT-INPUT-METHOD have the same meaning as for `completing-read'."
  (let ((minibuffer-local-completion-map
         (let ((map (copy-keymap minibuffer-local-completion-map)))
           (define-key map " " 'self-insert-command)
           map)))
    (completing-read prompt collection predicate require-match initial-input hist def inherit-input-method)))
