(defun who/on-zettels-committed ()
  "Mark 'Commit zettels' habit as done. Mainly called from post-commit hook."
  (let ((marker (org-id-find "ca0c0e67-142e-4c41-854c-89ab1ab0f0e3" 'marker)))
    (with-current-buffer (marker-buffer marker)
      (goto-char marker)
      (unless
          ;; skip transition if already zeroed today
          (org-time>= (org-entry-get marker "LAST_REPEAT")
                      ;; see https://emacs.stackexchange.com/questions/78438/converting-day-value-to-a-timestamp
                      (time-to-seconds (days-to-time (- (org-today)
                                                        (date-to-day "1970-01-01")))))
        (org-todo 'done))
      (move-marker marker nil))))

(defun who/org-element-contents (element)
  "Get contents of `element'. This is, for some reason, not what
`org-element-contents' does.

See https://emacs.stackexchange.com/questions/72539/getting-content-of-paragraph-element-using-org-element-api"
  (buffer-substring-no-properties (org-element-property :contents-begin element)
                                  (org-element-property :contents-end   element)))

(defun who/org-return (&optional ignore)
  "Add new list item, heading or table row with RET.
A double return on an empty element deletes it.
Use a prefix arg to get regular RET.
See: https://kitchingroup.cheme.cmu.edu/blog/2017/04/09/A-better-return-in-org-mode/"
  (interactive "P")
  (if ignore
      (org-return)
    (let ((context (org-element-context))
          (element (org-element-at-point)))
      (cond
       ;; at beginning of line, just do a regular org-return
       ((bolp)
        (org-return))

       ;; open links like usual, unless point is at the end of a line
       ((and (not (eolp))
             (eq 'link (car context)))
        (org-return))

       ((eq 'line-break (car context))
        (org-return-indent))

       ;; delete empty heading or item
       ((or (and (org-at-heading-p) (string= "" (org-get-heading)))
            (and (org-at-item-p) (not (org-element-property :contents-begin element)))
            ;; empty items of the form TAG-TEXT ::
            (and (org-at-item-p) (string= "::" (s-trim (who/org-element-contents element)))))
        (delete-region (line-beginning-position) (line-end-position)))

       ;; at a checkbox item, add another checkbox item
       ((org-at-item-checkbox-p)
        (org-insert-todo-heading nil))

       ;; at a non-checkbox item, add another non-checkbox item
       ((org-at-item-p)
        (org-insert-item))

       ;; lists end with two blank lines, so we need to make sure we are also not
       ;; at the beginning of a line to avoid a loop where a new entry gets
       ;; created with only one blank line.
       ;; ((org-in-item-p)
       ;;  (if (save-excursion (beginning-of-line) (org-element-property :contents-begin (org-element-context)))
       ;;      (org-insert-heading)
       ;;    (beginning-of-line)
       ;;    (delete-region (line-beginning-position) (line-end-position))
       ;;    (org-return)))

       ;; from heading middle, split line and insert sibling
       ((org-at-heading-p)
        (org-insert-heading))

       ;; from EOL, insert heading at same subtree level
       ;; ((and (org-at-heading-p) (eolp))
       ;;  (org-end-of-meta-data)
       ;;  (org-insert-heading-respect-content)
       ;;  ;; (org-insert-heading)
       ;;  (outline-show-entry))

       ;; tables
       ;; ((org-at-table-p)
       ;;  (if (-any?
       ;;       (lambda (x) (not (string= "" x)))
       ;;       (nth
       ;;        (- (org-table-current-dline) 1)
       ;;        (org-table-to-lisp)))
       ;;      (org-return)
       ;;    ;; empty row
       ;;    (delete-region (line-beginning-position) (line-end-position))
       ;;    (org-return)))

       ;; fall-through case
       (t
        (org-return))))))

(defun who/org-asterisk ()
  "Surround region or current word with **, or if neither applies then just insert *."
  (interactive)
  (cond
   ((use-region-p)
    (save-excursion
      (let ((beg (region-beginning))
            (end (region-end)))
        (goto-char beg)
        (insert "*")
        (goto-char end)
        (forward-char 1)
        (insert "*"))))

   ;; at beginning of line, just insert * (probably making a headline)
   ((bolp)
    (insert "*"))

   ((looking-at "\\w")
    (save-excursion
      (forward-word)
      (insert "*")
      (backward-word)
      (insert "*")
      (forward-word)
      (forward-char)))

   (t
    (insert "*"))))

(defun who/org-lint-dir (directory)
  (interactive "D")
  (-each (directory-files directory t ".*\\.org$")
    (lambda (file)
      (with-temp-buffer
        (insert-file-contents file)
        (org-mode)
        (let ((lint (org-lint)))
          (when lint
            (print (list file lint))))))))

(defun who/org-attach-uuid-to-path (uuid)
  "`org-attach-id-uuid-folder-format' will split the UUID into two path segments
for no reason that I can fathom. Instead just return the entire UUID."
  uuid)

(defun who/mtime (file) (let ((attrs (file-attributes file))) (nth 5 attrs)))

(defun who/latest-file (path)
  (car (who/latest-files path)))

(defun who/latest-files (path)
  (let ((e (f-entries path)))
    (sort e (lambda (a b)
              (not (time-less-p (who/mtime a)
                                (who/mtime b)))))))

(defun who/org-insert-link-to-latest ()
  (interactive)
  (let ((file (who/latest-file "~/org/library")))
    (funcall 'org-insert-link nil (concat "file:" file) (f-filename file))))

(defun who/org-download (link)
  "Place org-downloaded files in a folder named after the current org file.
Inspired by https://github.com/daviderestivo/emacs-config/blob/6086a7013020e19c0bc532770e9533b4fc549438/init.el#L701"
  (let ((filename
         (file-name-nondirectory
          (car (url-path-and-query
                (url-generic-parse-url link)))))
        ;; Create folder name with current buffer name, and place in root dir
        (dirname (f-join who/org-download-directory
                         (replace-regexp-in-string " " "_" (downcase (file-name-base buffer-file-name))))))

    ;; Add timestamp to filename
    (setq filename-with-timestamp (format "%s%s.%s"
                                          (file-name-sans-extension filename)
                                          (format-time-string org-download-timestamp)
                                          (file-name-extension filename)))
    ;; Create folder if necessary
    (unless (file-exists-p dirname)
      (make-directory dirname))
    (expand-file-name filename-with-timestamp dirname)))

(defun who/org-noter-latest ()
  (interactive)
  (org-roam-tag-add (list "bib"))
  (insert "* Notes\n")
  (let* ((file (who/latest-file "~/org/library"))
         (relative-file (file-relative-name file "~/org/zettelkasten")))
    (org-set-property "NOTER_DOCUMENT" relative-file)
    (org-noter)))

(defun who/org-player-insert-link-to-position ()
  (interactive)
  (org-player-insert-link-to-position 7))

;; styling

(defun who/style-org ()
  (who/personalize-fonts)
  (who/buffer-face-mode-human)
  (mapc
   (lambda (face)
     (set-face-attribute face nil :inherit 'fixed-pitch))
   (list 'org-checkbox
         'org-code
         'org-block
         'org-table
         'org-verbatim
         'org-block-begin-line
         'org-block-end-line
         'org-meta-line
         'org-document-info-keyword)))

(defun who/ef-themes-custom-faces-org ()
  (ef-themes-with-colors
    (custom-set-faces
     `(org-agenda-calendar-event ((,c :foreground ,fg-main)))
     `(org-agenda-current-time ((,c :foreground ,accent-0)))
     `(org-agenda-date ((,c :foreground ,fg-alt)))
     `(org-link ((,c :slant italic
                     :underline nil)))
     `(org-scheduled-previously ((,c :foreground ,fg-main)))
     `(org-scheduled-today ((,c :foreground ,fg-main)))
     `(org-time-grid ((,c :weight light)))
     `(who/org-agenda-category ((,c :foreground ,fg-dim
                                    :weight light)))
     `(who/org-priority-low ((,c :weight light))))))

(defun who/org-agenda-before-sorting-filter (line)
  "Remove [] from around priority on agenda items (it's just noise)."
  ;; 1 10 hardcoded to match `org-agenda-prefix-format'
  (add-text-properties 1 10 (list 'face 'who/org-agenda-category) line)
  (cond
   ((s-contains? "[#A]" line)
    (->> line
         (s-replace "[#A]" (propertize "#A" 'face '(bold org-priority)))
         (s-replace "TODO" (propertize "TODO" 'face '(bold org-todo)))))
   ((s-contains? "[#B]" line)
    (s-replace "[#B]" (propertize "#B" 'face 'org-priority) line))
   ((s-contains? "[#C]" line)
    (s-replace "[#C]" (propertize "#C" 'face '(org-priority who/org-priority-low)) line))
   (t line)))

;;;
;; org-roam utilities
;;;

(defun who/advise-orb-templates (fn &rest args)
  (let ((org-roam-capture-templates (list who/org-roam-template-bib)))
    (apply fn args)))

(setq who/org-roam-capture-any-templates
      `(("canlii" "canlii" plain "%?"
         :target (file+head "pages/${slug}.org" ":PROPERTIES:
:ROAM_REFS: ${ref}
:END:
#+title: ${title}
#+filetags: :bib:case:
")
         :unnarrowed t)
        ("q" "qase" plain "%?"
         :target (file+head "pages/${slug}.org" ":PROPERTIES:
:email:    ${email}
:phone:    ${phone}
:qase:     ${qase}
:END:
#+title: ${title}
#+filetags: :human:person:

${referral_description}

# * HOLD [#C] Bill
# * HOLD [#A] Closing letter
# * TODO [#B] ???
# * HOLD [#C] Bill quote fee
* HOLD [#B] Scope and quote
* HOLD [#C] Pre-auth quote fee
* TODO [#C] Intro email
")
         :unnarrowed t)))

(defun who/org-roam-protocol-open-any (info)
  (raise-frame)
  (org-roam-capture-
   :keys (plist-get info :template)
   :node (org-roam-node-create :title (plist-get info :title))
   :info info
   :templates who/org-roam-capture-any-templates)
  nil)

(defun who/org-roam-matter-create ()
  (interactive)
  (org-link-store-props
   :type "id"
   :link (org-roam-id-at-point))
  (org-store-link nil)
  (org-roam-capture-
   :templates `(("m" "matter" plain "%?"
                 :target (file+head "pages/elc-%<%Y%m%d%H%M%S>.org" ":PROPERTIES:
:client:   ${client}
:start:    %u
:END:
#+title: ELC-%<%Y%m%d%H%M%S>
#+filetags: :elc:matter:

# * HOLD [#C] Bill
# * HOLD [#A] Closing letter
# * TODO [#B] ???
# * HOLD [#C] Bill quote fee
* HOLD [#B] Scope and quote
* HOLD [#C] Pre-auth quote fee
")
                 :unnarrowed t))
   :keys "m"
   :info `(:client ,(org-link-make-string (nth 0 (car org-stored-links))
                                          (nth 1 (car org-stored-links))))
   :props '(:immediate-finish nil)
   :node (org-roam-node-create)
   :goto nil))

;; cf org-roam-tag-add
(defun who/org-roam-filetag-add (tags)
  "Add FILETAGS to the current org-roam file."
  (interactive
   (list (let ((crm-separator "[  ]*:[  ]*"))
           (completing-read-multiple "Tag: " (org-roam-tag-completions)))))
  (let ((current-tags (split-string (or (cadr (assoc "FILETAGS"
                                                     (org-collect-keywords '("filetags"))))
                                        "")
                                    ":" 'omit-nulls)))
    (org-roam-set-keyword "filetags" (org-make-tag-string (seq-uniq (append tags current-tags))))))

(defun who/org-roam-mark-public (neighbors-too)
  "Add :public: to the current org-roam buffer's filetags, and maybe its neighbors."
  (interactive (list (y-or-n-p "Include neighbors? ")))
  (when (org-roam-buffer-p)
    (who/org-roam-filetag-add '("public"))
    (when neighbors-too
      (save-current-buffer
        (-each (who/org-roam-links-paths)
          (lambda (neighbor-file)
            (find-file neighbor-file)
            (who/org-roam-filetag-add '("public"))))))))

(defun who/org-roam-links-paths ()
  "Get paths for the files containing nodes that have links to the node at point."
  (->> (append
        ;; backlinks
        (org-roam-db-query
         [:select source
          :from links
          :where (= dest $s1) :and (= type "id")]
         (org-roam-node-id (org-roam-node-at-point)))
        ;; forward links
        (org-roam-db-query
         [:select dest
          :from links
          :where (= source $s1) :and (= type "id")]
         (org-roam-node-id (org-roam-node-at-point))))
       (-map #'car)
       (-map (lambda (backlink-id)
               (org-roam-node-file (org-roam-node-from-id backlink-id))))
       (-uniq)))

(defun who/org-roam-capture-link-at-point ()
  (interactive)
  (with-current-buffer (plist-get org-capture-plist :original-buffer)
    (let ((node (org-roam-node-at-point)))
      (org-link-make-string
       (concat "id:" (org-roam-node-id node))
       (org-roam-node-formatted node)))))

;;;
;; Web publishing
;; cf https://notes.alex-miller.co/publish/
;; cf https://github.com/alexkehayias/emacs.d/blob/60edaa6cd5cc4876b489fc8f2b57d2ac4726645b/init.el
;;;

(defun who/willopedia-publish-zettel (plist file-name pub-dir)
  (let ((out-path (who/zettel-output-path file-name pub-dir)))
    (cl-letf (((symbol-function 'org-export-output-file-name)
               (lambda (extension &optional subtreep pub-dir)
                 (concat out-path "index" extension))))
      (org-publish-org-to 'willopedia-html
                          file-name
                          ".html"
                          (plist-put plist :backlinks (who/org-roam-public-backlinks file-name))
                          ;; (plist-put plist :backlinks-html (who/willopedia-backlinks-html (who/org-roam-public-backlinks file-name)))
                          out-path))))

(defun who/slugify-zettel-path (path)
  (->> (file-relative-name path org-roam-directory)
       (file-name-sans-extension)
       (downcase)
       (replace-regexp-in-string "[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][-_]" "")))

(defun who/path-to-href (path)
  (cond
   ;; downloaded files live at /media in the export
   ((s-starts-with? "../download" path)
    (s-replace "../download" "/media" path))

   ;; zettel by default
   (t
    (->> path
         (who/slugify-zettel-path)
         (concat "/")))))

(defun who/zettel-output-path (org-file pub-dir)
  (let ((out-dir (concat pub-dir
                         (->> org-file
                              (who/slugify-zettel-path)
                              (file-name-as-directory)))))
    (progn
      (unless (file-directory-p out-dir)
        (make-directory out-dir t))
      out-dir)))

(defun who/willopedia-zettel-template (contents info)
  (concat
   "<!DOCTYPE html>"
   (sxml-to-xml
    `(html
      (@ (lang "en"))
      (head
       (meta (@ (charset "utf-8")))
       (meta (@ (author "William O'Hanley")))
       (meta (@ (name "viewport")
                (content "width=device-width, initial-scale=1, shrink-to-fit=no")))
       (link (@ (rel "preconnect")
                (href "https://fonts.googleapis.com")))
       (link (@ (rel "preconnect")
                (href "https://fonts.gstatic.com")
                (crossorigin "anonymous")))
       (link (@ (rel "stylesheet")
                (href "https://fonts.googleapis.com/css2?family=Alegreya+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap")))
       (link (@ (rel "stylesheet")
                (href "https://cdnjs.cloudflare.com/ajax/libs/Iosevka/6.0.0/iosevka/iosevka.min.css")))
       (script (@ (type "text/javascript"))
               (*RAW-STRING*
                "window.goatcounter = {
  path: function (p) { return 'notes' + p; }
}"))
       (script (@ (type "text/javascript")
                  (data-goatcounter "https://wohanley.goatcounter.com/count")
                  (async)
                  (src "//gc.zgo.at/count.js"))
               "")
       ;; TODO
       ;; (link (@ (rel "stylesheet")
       ;;          (href "/css/code.css")))
       (link (@ (rel "stylesheet")
                (href "/css/index.css")))
       (title ,(concat (org-export-data (plist-get info :title) info) " | Will O'Pedia")))
      (body
       (header (h1 (@ (class "zettel__title")) ,(org-export-data (plist-get info :title) info)))
       (div (@ (class "zettel"))
            (*RAW-STRING* ,contents)
            ,@(who/willopedia-backlinks-sxml (plist-get info :backlinks))))
      (footer
       (h1 (@ (class "willopedia-title"))
           (a (@ (class "willopedia-title__main")
                 (href "/about"))
              "Will O'Pedia")
           (span (@ (class "willopedia-title__sub")) "Will O'Hanley's notes"))
       (div (@ (class "footer__links"))
            (a (@ (href "https://wohanley.com"))
               "home")
            (a (@ (href "https://wohanley.com/posts"))
               "posts")
            (a (@ (href "https://gitlab.com/whohanley"))
               "gitlab")
            (a (@ (href "https://github.com/wohanley"))
               "github")
            (a (@ (href "https://twitter.com/wohanley"))
               "twitter")
            (a (@ (href "https://are.na/will-o-hanley"))
               "are.na")
            (a (@ (href "https://www.goodreads.com/review/list/37883668?sort=date_read"))
               "goodreads")))))))

;; cf https://writepermission.com/org-blogging-clickable-headlines.html
(defun who/willopedia-headline (headline contents info)
  (let* ((text (org-export-data (org-element-property :title headline) info))
         (level (org-export-get-relative-level headline info))
         (level (min 7 (when level (1+ level))))
         (anchor-name (who/slugify-title text))
         (attributes (org-element-property :ATTR_HTML headline))
         (container (org-element-property :HTML_CONTAINER headline))
         (container-class (and container (org-element-property :HTML_CONTAINER_CLASS headline))))
    (when attributes
      (setq attributes
            (format " %s" (org-html--make-attribute-string
                           (org-export-read-attribute 'attr_html
                                                      `(nil
                                                        (attr_html ,(split-string attributes))))))))
    (concat
     (when (and container (not (string= "" container)))
       (format "<%s%s>" container (if container-class (format " class=\"%s\"" container-class) "")))
     (if (not (org-export-low-level-p headline info))
         (format "<h%d%s id=\"%s\"><a class=\"headline-anchor\" href=\"#%s\"></a>%s</h%d>%s"
                 level
                 (or attributes "")
                 anchor-name
                 anchor-name
                 text
                 level
                 (or contents ""))
       (concat
        (when (org-export-first-sibling-p headline info) "<ul>")
        (format "<li>%s%s</li>" text (or contents ""))
        (when (org-export-last-sibling-p headline info) "</ul>")))
     (when (and container (not (string= "" container)))
       (format "</%s>" (cl-subseq container 0 (cl-search " " container)))))))

(defun who/slugify-title (title)
  (->> title
       (downcase)
       (replace-regexp-in-string " " "-")
       (replace-regexp-in-string "</?code>" "")
       (replace-regexp-in-string "[^[:alnum:]_-]" "")))

(defun who/willopedia-link (link contents info)
  "Format links appropriately for the linkee.
Handles zettel and download paths, inlines images, and is agnostic wrt other stuff."
  (let ((link-type (org-element-property :type link))
        (link-path (org-element-property :path link)))
    (cond
     ;; org-id links
     ((string= 'id link-type)
      (let* ((id (s-replace "id:" "" link-path))
             (node-titles (org-roam-db-query [:select title
                                              :from nodes
                                              :where (= id $s1)]
                                             id))
             (file-path (expand-file-name (gethash id org-id-locations)))
             (file-titles (org-roam-db-query [:select title
                                              :from files
                                              :where (= file $s1)]
                                             file-path)))
        (when (> (length file-titles) 1) (error "Multiple titles found for file \"%s\"" file-path))
        (when (> (length node-titles) 1) (error "Multiple titles found for node \"%s\"" id))
        (when (< (length file-titles) 1) (error "No title found for file \"%s\"" file-path))
        (when (< (length node-titles) 1) (error "No title found for node \"%s\"" id))
        (let ((node-title (car (car node-titles)))
              (file-title (car (car file-titles))))
          (if (string= node-title file-title)
              (format "<a href=\"%s\">%s</a>"
                      (who/path-to-href file-path)
                      contents)
            (format "<a href=\"%s\">%s</a>"
                    ;; TODO technically we don't need the file-path if we're
                    ;; already on that page, and the fragment-only href would be
                    ;; easier on browsers
                    (concat (who/path-to-href file-path) "#" (who/slugify-title node-title))
                    contents)))))

     ;; inline images from org-download directory
     ((and (string= 'file link-type)
           (s-starts-with? (expand-file-name "~/org/download")
                           (expand-file-name link-path))
           (file-name-extension link-path)
           (string-match "png\\|jpg\\|svg\\|webp\\|gif"
                         (file-name-extension link-path)))
      (let* ((file-absolute (expand-file-name link-path))
             (out-dir (concat
                       (expand-file-name "~/wohanley.com/willopedia/media/")
                       (-> file-absolute f-dirname f-split last car)
                       "/")))
        (progn
          (f-mkdir-full-path out-dir)
          (copy-file file-absolute out-dir t)
          (format "<img src=\"%s\">"
                  (who/path-to-href link-path)))))

     ;; other files aren't included (not sure what they'd be)
     ((and (string= 'file link-type))
      (format "<a href=\"/403\">%s</a>"
              (or contents link-path)))

     ;; other kinds of links (probably http)
     (t
      (format "<a href=\"%s\">%s</a>"
              (org-element-property :raw-link link)
              (or contents (org-element-property :raw-link link)))))))

(defun who/willopedia-nodes-recently-modified ()
  (who/willopedia-update-publish-files)
  (->> (who/latest-files org-roam-directory)
       ;; (format "git -C %s log --no-pager --pretty='' --name-only" org-roam-directory)
       ;; (shell-command-to-string)
       ;; (s-lines)
       (-remove (lambda (line) (s-equals? "20221225145729-about.org" line)))
       (-take 50) ;; 50 is arbitrary, just want ~10 plus some wiggle room to exclude
       (-map (lambda (name) (f-join org-roam-directory name)))
       (-intersection who/willopedia-include-files)
       (-map #'who/org-roam-node-from-file)))

(defun who/willopedia-insert-links-recently-modified ()
  (interactive)
  (dolist (node (who/willopedia-nodes-recently-modified))
    (pcase-let ((`(,id ,title) node))
      (insert "- " (org-link-make-string (concat "id:" id) title))
      (run-hook-with-args #'org-roam-post-node-insert-hook id title)
      (newline))))

(defun who/org-roam-node-from-file (fname)
  (nth 0
       (org-roam-db-query
        [:select [id title]
         :from nodes
         :where (and (= nodes:level 0)
                     (= file $s1))]
        fname)))

(defun who/org-roam-public-backlinks (file)
  (if (not (org-roam-file-p file))
      '()
    (->>
     (org-roam-db-query
      [:select :distinct [srcfiles:title srcfiles:file]
       :from nodes :as destnode
             :join links :on (= links:dest destnode:id)
             :join nodes :as srcnodes :on (= srcnodes:id links:source)
             :join files :as srcfiles :on (= srcnodes:file srcfiles:file)
       :where (and (= destnode:file $s1)
                   (in srcfiles:file $v2))]
      file
      (vconcat who/willopedia-include-files))
     ;; some notes link to themselves internally, exclude those
     (-remove (lambda (backlink)
                (string= file (nth 1 backlink)))))))

(defun who/org-roam-public-outlinks (file)
  (if (not (org-roam-file-p file))
      '()
    (->>
     (org-roam-db-query
      [:select :distinct [destfiles:title destfiles:file]
       :from nodes :as srcnode
             :join links :on (= links:source srcnode:id)
             :join nodes :as destnodes :on (= destnodes:id links:dest)
             :join files :as destfiles :on (= destnodes:file destfiles:file)
       :where (and (= srcnode:file $s1)
                   (in destfiles:file $v2))]

      file
      (vconcat who/willopedia-include-files))
     ;; some notes link to themselves internally, exclude those
     (-remove (lambda (backlink)
                (string= file (nth 1 backlink)))))))

(defun who/willopedia-backlinks-sxml (backlinks)
  (if (= 0 (length backlinks))
      '()
    ;; this outer div doesn't seem to get rendered directly, but without it the
    ;; inner div gets messed up. I guess some intricacy of esxml
    `(div
      (div (@ (class "backlinks__container"))
           (h3 "Notes that link to this note")
           (ul (@ (class "backlinks__list"))
               ,@(->> backlinks
                      (-map (lambda (backlink)
                              (-let [(title source-file) backlink]
                                `(li
                                  (a (@ (class "backlink")
                                        (href ,(who/path-to-href source-file)))
                                     ,title)))))))))))

(defvar who/willopedia-include-files '()
  "Files visible in zettelkasten export (including as backlinks).
Refreshed when running `who/willopedia-publish'.")

(defvar who/willopedia-publish-files '()
  "Files to export from zettelkasten.
Refreshed when running `who/willopedia-publish'.")

(defvar who/willopedia-publish-all nil
  "non-nil to publish all zettels tagged public, otherwise only public zettels
updated since last publish.")
;; (setq who/willopedia-publish-all t)

(defun who/latest-zettel-commit ()
  (->> (format "git -C %s rev-parse HEAD" org-roam-directory)
       (shell-command-to-string)
       (s-trim)))

(defun who/willopedia-set-published-commit-latest ()
  (setq who/willopedia-published-commit (who/latest-zettel-commit)))

(defun who/zettels-changed-since-commit (commit-hash)
  (->> (format "git --no-pager -C %s diff --name-only %s.." org-roam-directory commit-hash)
       (shell-command-to-string)
       (s-lines)
       (-filter (lambda (line) (not (s-blank? line))))
       (-map (lambda (name) (f-join org-roam-directory name)))))

(defun who/willopedia-update-publish-files ()
  (setq who/willopedia-include-files
        (who/rg-files-with-matches "^\\#\\+filetags:.*[ :]public[ :].*$" org-roam-directory))
  (setq who/willopedia-publish-files
        (if who/willopedia-publish-all
            who/willopedia-include-files
          (let* ((changes (who/zettels-changed-since-commit who/willopedia-published-commit))
                 (dirty (-reduce-from (lambda (acc change)
                                        (-concat acc
                                                 (list change)
                                                 (-map (lambda (link) (nth 1 link)) (who/org-roam-public-outlinks change))
                                                 (-map (lambda (link) (nth 1 link)) (who/org-roam-public-backlinks change))))
                                      nil
                                      changes)))
            (-intersection who/willopedia-include-files dirty)))))

(defun who/willopedia-publish ()
  (interactive)
  (org-roam-update-org-id-locations)
  (who/willopedia-update-publish-files)
  (setq org-publish-project-alist
        `(("notes"
           :base-extension "org"
           :base-directory ,org-roam-directory
           :exclude ".*\\.org"
           :include ,who/willopedia-publish-files
           :with-tasks nil
           :publishing-function who/willopedia-publish-zettel
           :publishing-directory "~/wohanley.com/willopedia"
           :completion-function who/willopedia-set-published-commit-latest)))
  (org-publish-project "notes" nil t))

;;;
;; org-agenda
;;;

(defun who/org-clock-out-on-state ()
  (when (equal "WAIT" (org-get-todo-state))
    (org-clock-out nil t)))

(defun who/org-logbook-get (marker)
  (with-current-buffer (marker-buffer marker)
    (goto-char marker)
    (unless (org-at-heading-p)
      (outline-previous-heading))
    (when (re-search-forward ":LOGBOOK:" (save-excursion
                                           (outline-next-heading)
                                           (point))
                             t)
      (let* ((elt (org-element-property-drawer-parser nil))
             (beg (org-element-property :contents-begin elt))
             (end (org-element-property :contents-end elt)))
        (buffer-substring-no-properties beg end)))))

(defun who/org-logbook-latest-entry-time-string (logbook)
  "Get the timestamp (as a string) of the latest entry in `logbook'."
  (when logbook
    (string-match ".*State \\\"WAIT\\\"\\W+from.*\\[\\(.*\\)\\]" logbook)
    (match-string 1 logbook)))

(defun who/org-cmp-blocked (a b)
  (let* ((a-marker (get-text-property 0 'org-marker a))
         (b-marker (get-text-property 0 'org-marker b))
         (a-keyword (org-entry-get a-marker "TODO"))
         (b-keyword (org-entry-get b-marker "TODO")))
    ;; compare TODO keywords first
    (let ((cmp-kw (compare-strings a-keyword nil nil b-keyword nil nil)))
      (if (not (eq t cmp-kw))
          (cl-signum cmp-kw)
        ;; if TODO keywords are the same, move on and compare LOGBOOK timestamps
        (let ((a-time (who/org-logbook-latest-entry-time-string (who/org-logbook-get a-marker)))
              (b-time (who/org-logbook-latest-entry-time-string (who/org-logbook-get b-marker))))
          (cond
           ((and a-time b-time)
            (let ((cmp-ts (compare-strings a-time nil nil b-time nil nil)))
              (if (eq t cmp-ts)
                  nil
                (cl-signum cmp-ts))))

           (a-time
            1)

           (b-time
            -1)

           (t
            nil)))))))

(defun who/find-org-files (directory)
  (->> (format "fd '\.org$' %s" directory)
       (shell-command-to-string)
       (s-lines)
       (-filter (lambda (line) (not (s-blank? line))))))

(defun who/org-agenda-files-update (&rest _)
  "Update `org-agenda-files' from org files.
Includes files with open TODOs. Would be nice to also include files with active
timestamps."
  (let ((todo-zettels (->> (s-concat "rg --files-with-matches '(^\\*+ TODO)|(^\\*+ NEXT)|(^\\*+ HOLD)|(^\\*+ WAIT)' " org-roam-directory)
                           (shell-command-to-string)
                           (s-lines)
                           (-filter (lambda (line) (not (or (s-blank? line)
                                                            (s-starts-with? ".#" (file-name-nondirectory line)))))))))
    (setq org-agenda-files (seq-uniq (append (who/find-org-files who/org-agenda-directory)
                                             todo-zettels
                                             who/org-agenda-files-extra)))))

(defun who/switch-to-agenda ()
  (interactive)
  (org-agenda nil " "))

(defun who/org-agenda-set-effort (effort)
  "Set the effort property for the current headline."
  (interactive
   (list (read-string (format "Effort [%s]: " who/org-current-effort) nil nil who/org-current-effort)))
  (setq who/org-current-effort effort)
  (org-agenda-check-no-diary)
  (let* ((hdmarker (or (org-get-at-bol 'org-hd-marker)
                       (org-agenda-error)))
         (buffer (marker-buffer hdmarker))
         (pos (marker-position hdmarker))
         (inhibit-read-only t)
         newhead)
    (org-with-remote-undo buffer
      (with-current-buffer buffer
        (widen)
        (goto-char pos)
        (org-show-context 'agenda)
        (funcall-interactively 'org-set-effort nil who/org-current-effort)
        (end-of-line 1)
        (setq newhead (org-get-heading)))
      (org-agenda-change-all-lines newhead hdmarker))))

(defun who/org-agenda-process-inbox-item ()
  "Process a single item in the org-agenda."
  (interactive)
  (org-with-wide-buffer
   (org-agenda-set-tags)
   (call-interactively 'who/org-agenda-set-effort)
   (org-agenda-refile nil nil t)))

(defun who/bulk-process-entries ()
  (if (not (null org-agenda-bulk-marked-entries))
      (let ((entries (reverse org-agenda-bulk-marked-entries))
            (processed 0)
            (skipped 0))
        (dolist (e entries)
          (let ((pos (text-property-any (point-min) (point-max) 'org-hd-marker e)))
            (if (not pos)
                (progn (message "Skipping removed entry at %s" e)
                       (cl-incf skipped))
              (goto-char pos)
              (let (org-loop-over-headlines-in-active-region) (funcall 'who/org-agenda-process-inbox-item))
              ;; `post-command-hook' is not run yet.  We make sure any
              ;; pending log note is processed.
              (when (or (memq 'org-add-log-note (default-value 'post-command-hook))
                        (memq 'org-add-log-note post-command-hook))
                (org-add-log-note))
              (cl-incf processed))))
        (org-agenda-redo)
        (unless org-agenda-persistent-marks (org-agenda-bulk-unmark-all))
        (message "Acted on %d entries%s%s"
                 processed
                 (if (= skipped 0)
                     ""
                   (format ", skipped %d (disappeared before their turn)"
                           skipped))
                 (if (not org-agenda-persistent-marks) "" " (kept marked)")))))

(defun who/org-process-inbox ()
  "Called in org-agenda-mode, processes all inbox items."
  (interactive)
  (org-agenda-bulk-mark-regexp "inbox:")
  (who/bulk-process-entries))

;;;
;; work/personal context switching
;;;

(defvar who/org-agenda-files-extra nil)

(defun who/org-agenda-context-personal ()
  (setq who/org-agenda-keys "p")
  (setq who/org-agenda-directory (expand-file-name "~/org/gtd/"))
  (setq org-refile-targets '(("fun.org" :level . 0)
                             ("next.org" :level . 0)
                             ("someday.org" :level . 0)
                             ("read.org" :level . 0)
                             (who-org/find-projects :level . 0)
                             (who-org/find-areas-of-responsibility :level . 0)))
  (setq org-reverse-note-order '(("fun.org" . t)
                                 ("someday.org" . t)
                                 ("read.org" . t)))

  (setq org-capture-templates
        `(("i" "inbox" entry (file ,(f-join who/org-agenda-directory "inbox.org"))
           "* TODO [#B] %?\n")
          ("b" "backlog" entry (file ,(f-join who/org-agenda-directory "next.org"))
           "* TODO [#B] %?\n")
          ("a" "appointment" entry (file ,(f-join who/org-agenda-directory "calendars/personal.org"))
           "* %?\n")
          ("r" "read" entry (file ,(f-join who/org-agenda-directory "read.org"))
           "* TODO [#C] %(who/org-roam-capture-link-at-point)%? :read:"
           :prepend t)
          ("e" "email" entry (file ,(f-join who/org-agenda-directory "inbox.org"))
           "* TODO [#B] %a" :immediate-finish t)
          ("l" "link" entry (file ,(f-join who/org-agenda-directory "inbox.org"))
           "* TODO [#C] %(org-cliplink-capture)" :immediate-finish t)
          ("c" "org-protocol-capture" entry (file ,(f-join who/org-agenda-directory "inbox.org"))
           "* TODO [#C] [[%:link][%:description]]\n\n %i" :immediate-finish t)))

  (setq org-agenda-custom-commands
        `((" " "Agenda"
           ((agenda ""
                    ((org-agenda-span 3)
                     (org-deadline-warning-days 60)
                     (org-agenda-sorting-strategy '(habit-down time-up deadline-up todo-state-down priority-down urgency-down category-keep))))
            (todo "NEXT"
                  ((org-agenda-overriding-header "In Progress")
                   (org-agenda-files (seq-remove
                                      (lambda (filename)
                                        (member filename
                                                (mapcar (lambda (f) (expand-file-name (concat who/org-agenda-directory f)))
                                                        '("habits.org"
                                                          "habits_shared.org"
                                                          "read.org"))))
                                      org-agenda-files))
                   ;; (org-agenda-files (append '(,(concat who/org-agenda-directory "someday.org")
                   ;;                             ,(concat who/org-agenda-directory "next.org"))
                   ;;                           (who-org/find-projects)
                   ;;                           (who-org/find-areas-of-responsibility)))
                   ))
            (todo "TODO"
                  ((org-agenda-overriding-header "To Refile")
                   (org-agenda-files '(,(f-join who/org-agenda-directory "inbox.org")))))
            (todo "TODO"
                  ((org-agenda-overriding-header "Backlog")
                   (org-agenda-files (seq-remove
                                      (lambda (filename)
                                        (member filename
                                                (mapcar (lambda (f) (f-join who/org-agenda-directory f))
                                                        '("inbox.org"
                                                          "habits.org"
                                                          "habits_shared.org"
                                                          "read.org"
                                                          "someday.org"
                                                          "fun.org"))))
                                      org-agenda-files))
                   ;; (org-agenda-files (append '(,(concat who/org-agenda-directory "next.org"))
                   ;;                           (who-org/find-projects)
                   ;;                           (who-org/find-areas-of-responsibility)))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'timestamp))))
            (todo "HOLD|WAIT"
                  ((org-agenda-overriding-header "Blocked")))
            (todo "TODO"
                  ((org-agenda-overriding-header "Someday")
                   (org-agenda-files '(,(f-join who/org-agenda-directory "someday.org")))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'timestamp))))
            (todo "TODO"
                  ((org-agenda-overriding-header "Fun")
                   (org-agenda-files '(,(f-join who/org-agenda-directory "fun.org")))))
            (todo "NEXT" ;; skip TODOs from read.org, there are way too many
                  ((org-agenda-overriding-header "Reading")
                   (org-agenda-files '(,(f-join who/org-agenda-directory "read.org")))
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'timestamp))
                   (org-agenda-sorting-strategy '(todo-state-down priority-down))))))
          ("r" "Reading"
           ((todo ""
                  ((org-agenda-files '(,(f-join who/org-agenda-directory "read.org")))
                   (org-agenda-sorting-strategy '(todo-state-down priority-down))))))))

  (setq who/org-agenda-files-extra nil))

(defun who/org-agenda-context-work ()
  (setq who/org-agenda-keys "w")
  (setq who/org-agenda-directory (expand-file-name "~/lawyer/org/gtd/"))
  (setq org-refile-targets '(("fun.org" :level . 0)
                             ("next.org" :level . 0)
                             ("someday.org" :level . 0)
                             (who-org/find-projects :level . 0)
                             (who-org/find-areas-of-responsibility :level . 0)))
  (setq org-refile-targets `(("next.org" :level . 0)
                             (,(expand-file-name "~/org/gtd/read.org") :level . 0)
                             (who-org/find-projects :level . 0)
                             (who-org/find-areas-of-responsibility :level . 0)))
  (setq org-reverse-note-order nil)

  (setq org-capture-templates
        `(("i" "inbox" entry (file ,(expand-file-name "~/org/gtd/inbox.org")) ;; shared inbox
           "* TODO [#B] %?\n")
          ("b" "backlog" entry (file ,(f-join who/org-agenda-directory "next.org"))
           "* TODO [#B] %?\n")
          ("a" "appointment" entry (file ,(expand-file-name "~/org/gtd/calendars/personal.org"))
           "* %?\n")
          ("e" "email" entry (file ,(f-join who/org-agenda-directory "inbox.org"))
           "* TODO [#B] %a" :immediate-finish t)
          ("l" "link" entry (file ,(f-join who/org-agenda-directory "inbox.org"))
           "* TODO [#C] %(org-cliplink-capture)" :immediate-finish t)
          ("c" "org-protocol-capture" entry (file ,(expand-file-name "~/org/gtd/inbox.org"))
           "* TODO [#C] [[%:link][%:description]]\n\n %i" :immediate-finish t)))

  (setq org-agenda-custom-commands
        `((" " "Agenda"
           ((agenda ""
                    ((org-agenda-start-on-weekday nil)
                     (org-agenda-span 5)
                     (org-deadline-warning-days 120)
                     (org-agenda-sorting-strategy '(habit-down time-up deadline-up todo-state-down priority-down urgency-down category-keep))))
            (todo "TODO"
                  ((org-agenda-overriding-header "To Refile")
                   (org-agenda-files '(,(expand-file-name "~/org/gtd/inbox.org")
                                       ,(f-join who/org-agenda-directory "inbox.org")))))
            (todo "TODO|NEXT"
                  ((org-agenda-overriding-header "Backlog")
                   (org-agenda-files (seq-remove
                                      (lambda (filename)
                                        (member filename
                                                (list (expand-file-name "~/org/gtd/inbox.org")
                                                      (f-join who/org-agenda-directory "inbox.org")
                                                      (expand-file-name "~/org/gtd/habits_shared.org"))))
                                      org-agenda-files))
                   ;; skip scheduled items
                   (org-agenda-skip-function '(org-agenda-skip-entry-if 'timestamp))
                   (org-agenda-sorting-strategy '(todo-state-down deadline-up priority-down))))
            (todo "HOLD|WAIT"
                  ((org-agenda-overriding-header "Blocked")
                   (org-agenda-cmp-user-defined #'who/org-cmp-blocked)
                   (org-agenda-sorting-strategy '(user-defined-down))))))))

  (setq who/org-agenda-files-extra (list (expand-file-name "~/org/gtd/calendars")
                                         (expand-file-name "~/org/gtd/habits_shared.org"))))

(defun who/org-roam-context-personal ()
  (setq org-roam-directory (expand-file-name "~/org/zettelkasten"))
  (setq org-roam-db-location (expand-file-name "~/.emacs.d/org-roam.db"))
  (setq org-roam-dailies-directory "daily/")
  (setq org-roam-capture-templates (list who/org-roam-template-public
                                         who/org-roam-template-private)))

(defun who/org-roam-context-work ()
  (setq org-roam-directory (expand-file-name "~/lawyer/org/zettelkasten"))
  (setq org-roam-db-location (expand-file-name "~/lawyer/org/org-roam.db"))
  (setq org-roam-dailies-directory "journals/")
  (setq org-roam-capture-templates
        '(("d" "default" plain "%?"
           :target (file+head "pages/%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
           :unnarrowed t))))

(defun who/org-context-personal ()
  (interactive)
  (setq who/org-download-directory (expand-file-name "~/org/download"))
  (who/org-agenda-context-personal)
  (who/org-roam-context-personal)
  (setq who/org-context 'personal)
  (who/load-theme))

(defun who/org-context-work ()
  (interactive)
  (setq who/org-download-directory (expand-file-name "~/lawyer/org/download"))
  (who/org-agenda-context-work)
  (who/org-roam-context-work)
  (setq who/org-context 'work)
  (who/load-theme))

;;;
;; emms
;;;

(defun who/emms-seek-to (time)
  "Seek to TIME, expressed as a string in org-emms style."
  (emms-seek-to (org-emms-time-string-to-seconds time)))
