;;; packages.el --- who-org layer packages file for Spacemacs.
;;
;; org-mode stuff, much from https://github.com/jethrokuan/.emacs.d
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author:  <wohanley@pigeon>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `who-org-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `who-org/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `who-org/pre-init-PACKAGE' and/or
;;   `who-org/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst who-org-packages
  '(calfw
    calfw-org
    deft
    esxml
    org
    org-agenda
    org-clock-convenience
    org-download
    org-emms
    ;; (org-fc :location (recipe :fetcher github
    ;;                           :repo "l3kn/org-fc"
    ;;                           :files (:defaults "awk" "demo.org")))
    org-gcal
    org-noter
    org-pdftools
    (org-player :location local)
    org-roam
    org-roam-ui
    org-roam-bibtex
    org-superstar
    org-wild-notifier
    persist))

(defun who-org/init-calfw ()
  "Initialize calfw and add key-bindings"
  (use-package calfw
    :commands (cfw:open-calendar-buffer)
    :init
    (spacemacs/set-leader-keys "aCd" 'cfw:open-calendar-buffer)
    :config
    (setq calendar-week-start-day 1) ;; start week on Monday
    (define-key cfw:calendar-mode-map (kbd "TAB") 'cfw:show-details-command)
    (define-key cfw:calendar-mode-map (kbd "C-j") 'cfw:navi-next-item-command)
    (define-key cfw:calendar-mode-map (kbd "C-k") 'cfw:navi-prev-item-command)

    ;; hotfix: incorrect time range display https://github.com/kiwanami/emacs-calfw/issues/111
    ;; source: https://github.com/zemaye/emacs-calfw/commit/3d17649c545423d919fd3bb9de2efe6dfff210fe
    ;; this function keeps prompting me for dates, which is really annoying. not
    ;; sure what's going on, don't care enough to fix it
;;     (defun who/cfw:org-get-timerange (text)
;;       "Return a range object (begin end text).
;; If TEXT does not have a range, return nil."
;;       (let* ((dotime (cfw:org-tp text 'dotime)))
;;         (and (stringp dotime) (string-match org-ts-regexp dotime)
;; 	           (let* ((matches  (s-match-strings-all org-ts-regexp dotime))
;;                     (start-date (nth 1 (car matches)))
;;                     (end-date (nth 1 (nth 1 matches)))
;; 	                  (extra (cfw:org-tp text 'extra)))
;; 	             (if (string-match "(\\([0-9]+\\)/\\([0-9]+\\)): " extra)
;;                    (list (calendar-gregorian-from-absolute
;;                           (time-to-days
;;                            (org-read-date nil t start-date)))
;;                          (calendar-gregorian-from-absolute
;;                           (time-to-days
;;                            (org-read-date nil t end-date)))
;;                          text))))))
;;     (advice-add 'cfw:org-get-timerange :override #'who/cfw:org-get-timerange)
    ))

(defun who-org/init-calfw-org ()
  "Initialize calfw-org and add key-bindings"
  (use-package calfw-org
    :commands (cfw:open-org-calendar)
    :init
    (spacemacs/set-leader-keys "aoCd" 'cfw:open-org-calendar)
    :config
    (define-key cfw:org-schedule-map (kbd "TAB") 'cfw:org-open-agenda-day)
    (define-key cfw:org-custom-map (kbd "SPC") 'spacemacs-cmds)
    (define-key cfw:org-custom-map (kbd "TAB") 'cfw:org-open-agenda-day)))

(defun who-org/post-init-deft ()
  (setq deft-recursive 1)
  (setq deft-use-filename-as-title t)
  (setq deft-default-extension "org")
  (setq deft-directory "~/org/zettelkasten"))

(defun who-org/init-esxml ()
  (use-package esxml))

(defun who-org/post-init-org ()
  (defvar who/org-context 'personal)

  (setq org-export-async-init-file "~/.emacs.d/org-export-async-init.el")

  (setq org-blank-before-new-entry '((heading . nil)
                                     (plain-list-item . nil)))
  (setq org-cliplink-max-length 120)
  (setq org-clock-display-default-range 'untilnow)

  ;; (setq org-attach-id-dir "attach/")
  ;; (setq org-attach-id-to-path-function-list '(who/org-attach-uuid-to-path
  ;;                                             org-attach-id-uuid-folder-format
  ;;                                             org-attach-id-ts-folder-format
  ;;                                             org-attach-id-fallback-folder-format))

  (setq org-cite-global-bibliography '("~/org/master.bib"))
  ;; open helm-bibtex when clicking an org cite: link (instead of opening bibliography)
  (setq org-cite-follow-processor 'helm-bibtex-org-cite-follow)
  ;; citations are not links, but I'd like to be able to act like they are (e.g. [[cite:@whatever][Whatever]])
  ;; grotesque abuse of org link handling... whatever
  (org-link-set-parameters "cite" :follow (lambda (query _) (helm-bibtex-follow (list nil (list :key (substring query 1))))))
  (setq org-cite-csl-styles-dir (expand-file-name "~/Zotero/styles"))
  (setq org-cite-export-processors '((t . (csl "chicago-fullnote-bibliography.csl"))))
  ;; Overwrite `org-ref-insert-link' binding. Not sure how or when org-ref-core
  ;; actually gets loaded (this doesn't work without `with-eval-after-load')
  (with-eval-after-load 'org-ref-core
    (spacemacs/set-leader-keys-for-major-mode 'org-mode "ic" #'org-cite-insert))

  ;; link to notmuch search
  (org-link-set-parameters "notmuch-tree" :follow (lambda (query _) (notmuch-tree query)))

  (require 'org-habit)
  (require 'org-protocol)
  (add-to-list 'org-modules 'org-habit)
  (add-to-list 'org-modules 'org-protocol)

  ;; (require 'org-contacts)
  ;; (add-to-list 'org-modules 'org-contacts)
  ;; (setq org-contacts-files (notdeft-list-files-by-query "!all tag:person"))

  (setq org-special-ctrl-a/e t)
  (setq org-startup-folded 'showeverything) ;; sometimes interesting stuff in the property drawers
  (setq org-adapt-indentation nil)
  (setq org-indent-indentation-per-level 0)
  (setq org-list-indent-offset 2)
  (setq org-return-follows-link t)
  (setq org-link-frame-setup '((file . find-file))) ;; follow links in same window
  (setq org-agenda-diary-file "~/org/diary.org")
  (setq org-catch-invisible-edits 'show)
  (setq org-habit-preceding-days 21)
  (setq org-habit-following-days 7)

  (define-key org-mode-map (kbd "*") #'who/org-asterisk)
  (define-key org-mode-map (kbd "RET") #'who/org-return)

  (add-hook 'org-mode-hook #'auto-save-visited-mode)
  (add-hook 'org-after-todo-state-change-hook #'who/org-clock-out-on-state)
  (add-hook 'org-mode-hook #'who/style-org)
  (add-hook 'org-mode-hook #'who/ef-themes-custom-faces-org)
  (add-hook 'ef-themes-post-load-hook #'who/ef-themes-custom-faces-org)
  (setq org-agenda-before-sorting-filter-function #'who/org-agenda-before-sorting-filter)

  (defface who/org-agenda-category
    '((t :weight light))
    "Face for category prefix in agenda.")

  (defface who/org-priority-low
    '((t :weight light))
    "Face for #C priority in agenda.")

  ;; (defun who/org-font-lock-set-keywords ()
  ;;   (setq org-font-lock-extra-keywords))
  ;; (remove-hook 'org-font-lock-set-keywords-hook #'who/org-font-lock-set-keywords)

  ;; (font-lock-add-keywords 'org-mode
  ;;   '(("\\#\\+property: \\(roam_topics\\) .+$" 0 org-default-face t))
  ;;   'append)

;;   (defface who/roam-topics-keyword
;;     '((((class color) (min-colors 88) (background light))
;;        :foreground "#0030b4")
;;       (((class color) (min-colors 88) (background dark))
;;        :foreground "#34cfff")
;;       (t
;;        :foreground "darkgray"))
;;     "Face for #+roam_topics keyword.")

;;   (defun who/org-font-lock-set-keywords ()
;;     "Add font-lock function to Org's hook.
;; The hook is `org-font-lock-set-keywords-hook'."
;;     (add-to-list 'org-font-lock-extra-keywords
;;                  '(who/org-fontify-meta-lines-and-blocks)
;;                  'append))

;;   (defun who/org-fontify-meta-lines-and-blocks (limit)
;;     "Override Org's font-lock for #+transclude keyword.
;; This function does the following:

;; 1. Apply face `who/roam-topics-keyword' to #+roam_topics
;; 2. Re-applies Org's font-lock for links to the topic link
;; 3. Apply Org's face `org-meta-line' to properties

;; Argument LIMIT is to limit scope of `re-search-forward'; it's the
;; same with `org-fontify-meta-lines-and-blocks'."
;;     (let ((case-fold-search t)
;;           (regexp "\\(^[  ]*#\\+roam_topics:\\)\\(.*]]\\)?\\(.*$\\)")
;;           (beg)(end)(keyword-end)(prop-beg)(prop-end))
;;       (when (re-search-forward regexp limit t)
;;         (setq beg (match-beginning 0))
;;         (setq end (match-end 0))
;;         (setq keyword-end (match-end 1))
;;         (setq prop-beg (match-beginning 3))
;;         (setq prop-end (match-end 3))
;;         (remove-text-properties beg end
;;                                 '(font-lock-fontified t face org-meta-line))
;;         (add-text-properties beg keyword-end
;;                              '(font-lock-fontified t
;;                                                    face who/roam-topics-keyword))
;;         (add-text-properties prop-beg prop-end
;;                              '(font-lock-fontified t
;;                                                    face org-meta-line))
;;         (save-excursion
;;           (goto-char beg)
;;           (org-activate-links end)))))

;;   (add-hook 'org-font-lock-set-keywords-hook #'who/org-font-lock-set-keywords)

  (spacemacs/set-leader-keys-for-major-mode 'org-mode "n" #'org-noter)
  (spacemacs/set-leader-keys-for-major-mode 'org-mode "ir" #'who/org-insert-link-to-latest)

  (add-hook 'auto-save-hook (lambda () (quiet! (org-save-all-org-buffers)))))

(defun who-org/find-projects ()
  (notdeft-list-files-by-query "!all tag:\"project\" AND NOT tag:\"archive\""))

(defun who-org/find-areas-of-responsibility ()
  (notdeft-list-files-by-query "!all tag:\"aor\" AND NOT tag:\"archive\""))

(defun who-org/post-init-org-agenda ()
  (who/org-agenda-context-personal)

  (advice-add 'org-agenda :before #'who/org-agenda-files-update)

  (setq org-agenda-log-mode-items '(closed clock state)) ;; adding 'state includes habits in log
  (setq org-agenda-todo-list-sublevels nil) ;; don't show TODOs that are the child of another TODO
  (setq org-agenda-dim-blocked-tasks nil) ;; this is slow and I don't need it (https://orgmode.org/worg/agenda-optimization.html)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)
  (setq org-log-state-notes-insert-after-drawers nil)

  (setq org-todo-keywords
        '((sequence "TODO(t@)" "HOLD(h@)" "WAIT(w@)" "NEXT(n)" "|" "DONE(d)" "NOPE(f)")))

  (push '("org-roam-any" :protocol "roam-any" :function who/org-roam-protocol-open-any)
        org-protocol-protocol-alist)

  ;;;
  ;; org-agenda
  ;;;

  (setq org-agenda-block-separator nil)
  (setq org-agenda-start-with-log-mode t)
  (setq org-columns-default-format "%40ITEM(Task) %Effort(EE){:} %CLOCKSUM(Time Spent) %SCHEDULED(Scheduled) %DEADLINE(Deadline)")
  (setq org-agenda-prefix-format '((agenda . " %i %-10 c%?-12t% s")
                                   (todo . " %i %-10 c")
                                   (tags . " %i %-10 c")
                                   (search . " %i %-10 c")))
  (setq org-agenda-window-setup 'current-window)
  (setq org-agenda-restore-windows-after-quit nil)

  ;; https://github.com/syl20bnr/spacemacs/issues/3094
  (setq org-refile-use-outline-path 'file
        org-outline-path-complete-in-steps nil)
  (setq org-refile-allow-creating-parent-nodes 'confirm)

  (defvar who/org-current-effort "1:00" "Current effort for agenda items.")

  ;; Handle tasks with both scheduled and deadline timestamps in the agenda:
  ;;  - Don't show the deadline warning before the scheduled date
  ;;  - Don't show a duplicate line for the scheduled timestamp if the task is already shown because
  ;;    of its deadline
  ;;  - Don't show DONEs at all
  (setq org-agenda-skip-deadline-prewarning-if-scheduled 'pre-scheduled)
  (setq org-agenda-skip-scheduled-if-deadline-is-shown t)
  (setq org-agenda-skip-deadline-if-done t)

  ;; XXX is this the right place for this? forget what it's supposed to do
  (set-face-attribute 'org-agenda-done nil :foreground 'unspecified)

  ;;;
  ;; org-journal
  ;;;

  (setq org-journal-dir "~/org/zettelkasten")
  (setq org-journal-date-prefix "#+TITLE: ")
  (setq org-journal-file-format "journal-%Y-%m-%d.org")
  (setq org-journal-date-format "%Y-%m-%d")

  ;;;
  ;; bindings
  ;;;

  (add-hook 'org-agenda-mode-hook
            (lambda ()
              (define-key org-agenda-mode-map "i" 'org-agenda-clock-in)
              (define-key org-agenda-mode-map "o" 'org-agenda-clock-out)
              (define-key org-agenda-mode-map "s" 'who/org-agenda-process-inbox-item)
              (define-key org-agenda-mode-map "r" 'who/org-process-inbox)
              (define-key org-agenda-mode-map "R" 'org-agenda-refile)
              (define-key org-agenda-mode-map "y" 'org-agenda-todo-yesterday) ;; overriding org-agenda-year-view
            ))

  (bind-key "<f1>" 'who/switch-to-agenda)

  (defvar who/org-agenda-bulk-process-key ?f
    "Default key for bulk processing inbox items.")
  (setq org-agenda-bulk-custom-functions `((,who/org-agenda-bulk-process-key who/org-agenda-process-inbox-item))))

(defun who-org/init-org-clock-convenience ()
  (use-package org-clock-convenience
    :defer t
    :bind (:map org-agenda-mode-map
               ("<S-up>" . org-clock-convenience-timestamp-up)
               ("<S-down>" . org-clock-convenience-timestamp-down)
               ("o" . org-clock-convenience-fill-gap)
               ("e" . org-clock-convenience-fill-gap-both))))

(defun who-org/post-init-org-download ()
  (use-package org-download
    :defer t
    :after org
    :init
    (spacemacs/set-leader-keys-for-major-mode 'org-mode
      "s-Y" 'org-download-screenshot
      "s-y" 'org-download-yank)
    :config
    (if (memq window-system '(mac ns))
        (setq org-download-screenshot-method "screencapture -i %s")
      (setq org-download-screenshot-method "import %s"))
    (setq who/org-download-directory (expand-file-name "~/org/download"))
    (setq org-download-method #'who/org-download)))

(defun who-org/init-org-emms ()
  (add-to-list 'load-path "~/.emacs.d/private/who-org/extra/emms-5.3/lisp")

  (require 'emms-setup)
  (emms-all)
  ;; not sure why this doesn't work, but seeking is wildly inconsistent with the default players - vlc seems to work
  ;; (emms-default-players)
  (setq emms-player-list '(emms-player-vlc))

  (use-package org-emms
    :after org
    :config
    (setq org-emms-default-directory "~/org/library")
    (setq org-emms-delay 1)))

(defun who-org/init-org-fc ()
  (use-package org-fc-hydra
    :after (hydra)
    :config
    (setq org-fc-directories '("~/org/zettelkasten"))))

(defun who/organised-exchange-fetch ()
  "Sync Outlook calendar with Org agenda using https://github.com/ettomatic/organised-exchange"
  (interactive)
  (if (get-buffer "stanford.org")
      (kill-buffer "stanford.org"))
  (shell-command "~/scripts/outlook2org/organised-exchange/bin/eto")
  (message "Outlook import finished"))

(defun who/org-gcal-add-at-point ()
  "Post the org item at point to gcal."
  (interactive)
  (org-entry-put (point) "calendar-id" "willy.ohanley@gmail.com")
  (org-gcal-post-at-point))

(defun who/org-schedule-incl-gcal-at-point ()
  "Schedule the org item at point and post it to gcal."
  (interactive)
  (org-schedule nil)
  (who/org-gcal-add-at-point))

;; see https://github.com/kidd/org-gcal.el/issues/164
(defun who/org-gcal--convert-time-to-local-timezone (date-time local-timezone)
  (format-time-string "%Y-%m-%dT%H:%M:%S%z" (parse-iso8601-time-string date-time) local-timezone))

(defun who-org/init-org-gcal ()
  (use-package org-gcal
    :init
    ;; (require 'plstore)
    ;; (add-to-list 'plstore-encrypt-to '("9C5400B1EB789B3B8D34508807FE57C008A31E60"))
    (setq org-gcal-token-file "~/.config/org-gcal/.org-gcal-token"
          org-gcal-client-id (who/read-file-contents "~/.config/org-gcal/.org-gcal-client-id")
          org-gcal-client-secret (who/read-file-contents "~/.config/org-gcal/.org-gcal-client-secret")
          org-gcal-fetch-file-alist '(("willy.ohanley@gmail.com" . "~/org/gtd/calendars/personal.org")
                                      ("wohanley@everyonelegal.ca" . "~/org/gtd/calendars/elc.org")
                                      ("c_8ui5all7mct0ts559nu72l6t88@group.calendar.google.com" . "~/org/gtd/calendars/elc-shared.org"))
          ;; '(-25200 "PDT") is the format that comes out of
          ;; #'current-time-zone, and #'format-time-string seems to only work
          ;; with this format. supposedly you can pass a string like
          ;; "Canada/Pacific" to #'format-time-string, but that always returns a
          ;; time in UTC in my experience. I dunno wtf is going on, but this
          ;; seems to do what I want. see https://github.com/kidd/org-gcal.el/issues/164
          org-gcal-local-timezone nil)

    :after org

    :config
    (advice-add #'org-gcal--convert-time-to-local-timezone :override #'who/org-gcal--convert-time-to-local-timezone)

    ;; (add-hook 'org-agenda-mode-hook (lambda () (org-gcal-sync nil t)))
    ;; (add-hook 'org-capture-after-finalize-hook (lambda () (org-gcal-sync nil t)))
    :bind (:map org-mode-map
                ("M-m m d S" . who/org-schedule-incl-gcal-at-point)
                ("M-m m d G" . who/org-gcal-add-at-point))))

(defun who/fetch-calendars ()
  (interactive)
  (org-gcal-fetch)
  (who-org/organised-exchange-fetch))

(defun who/org-noter-insert-highlighted-note ()
  "Highlight the active region and add a precise note at its position."
  (interactive)
  ;; Adding an annotation will deactivate the region, so we reset it afterward
  (let ((region (pdf-view-active-region)))
    (call-interactively 'pdf-annot-add-highlight-markup-annotation)
    (setq pdf-view-active-region region))
  (call-interactively 'org-noter-insert-precise-note))

(defun who/org-noter-extend-highlighted-note ()
  "Highlight the active region and add its text to the previous note.

This is useful when a note extends over a page boundary."
  (interactive)
  (let ((region (pdf-view-active-region))
        (selected (mapconcat 'identity (pdf-view-active-region-text) ? )))
    (call-interactively 'pdf-annot-add-highlight-markup-annotation)
    (select-window (org-noter--get-notes-window 'force))
    (save-excursion
      (call-interactively 'org-previous-visible-heading)
      (move-end-of-line nil)
      (insert " " (replace-regexp-in-string "\n" " " selected)))
    (select-window (org-noter--get-doc-window))))

(defun who-org/init-org-noter ()
  (use-package org-noter
    :after org

    :commands org-noter

    :config
    (setq org-noter-insert-note-no-questions t)
    (setq org-noter-hide-other nil)
    ;; arbitrary big number to treat all notes as short
    (setq org-noter-max-short-selected-text-length 999999)

    :bind
    (:map org-noter-doc-mode-map
          ("i" .         'org-noter-insert-precise-note)
          ("<mouse-9>" . 'org-noter-insert-precise-note)
          ("M-i" .       'org-noter-insert-note)
          ("e" .         'who/org-noter-extend-highlighted-note)
          ("d" .         'who/org-noter-insert-highlighted-note)
          ("<mouse-8>" . 'who/org-noter-insert-highlighted-note)
          ("e" .         'who/org-noter-extend-highlighted-note)
          ("h" .         'pdf-annot-add-highlight-markup-annotation))))

(defun who-org/init-org-pdftools ()
  (use-package org-pdftools
    :hook (org-load . org-pdftools-setup-link)
    :config
    (setq org-pdftools-root-dir "~/org/library")
    (setq org-pdftools-markup-pointer-color "#FFFF00")))

;; org-pdftools and org-noter integration doesn't seem to work very well right now. maybe check back later, development seems active
;; (defun who-org/init-org-noter-pdftools ()
;;   (use-package org-noter-pdftools
;;     :config
;;     (with-eval-after-load 'pdf-annot
;;       (add-hook 'pdf-annot-activate-handler-functions 'org-noter-jump-to-note))))

(defun who-org/init-org-player ()
  (use-package org-player
    :commands who/org-player-insert-link-to-position
    :init
    (spacemacs/set-leader-keys-for-major-mode 'org-mode "i w" 'who/org-player-insert-link-to-position)))

(defun who-org/post-init-org-roam ()
  (require 'org-roam-protocol)

  (setq org-roam-v2-ack t)

  ;; Default 'sqlite-builtin isn't working at the moment. See https://github.com/org-roam/org-roam/issues/2393
  (setq org-roam-database-connector 'sqlite)

  (add-hook 'org-roam-preview-postprocess-functions
            (lambda (s)
              (s-join "\n"
                      (-take 3 (s-lines s)))))

  ;; Capture templates

  (setq who/org-roam-template-private
        '("d" "default" plain "#+title: ${title}\n\n%?"
          :target (file "%<%Y%m%d%H%M%S>-${slug}.org")
          :unnarrowed t))

  (setq who/org-roam-template-public
        '("p" "public" plain "#+title: ${title}\n#+filetags: :public:\n\n%?"
          :target (file "%<%Y%m%d%H%M%S>-${slug}.org")
          :unnarrowed t))

  (setq org-roam-dailies-capture-templates
        `(("d" "default" entry "* %?"
           :target (file+head "%<%Y-%m-%d>.org"
                              "#+title: %<%Y-%m-%d>\n#+filetags: daily"))))

  (who/org-roam-context-personal)

  (org-roam-db-autosync-enable)

  ;; Don't try to find an org-roam node while in agenda, it won't work (e.g.
  ;; calling helm-bibtex from the agenda will blow up without this advice)
  (advice-add #'org-roam-node-at-point :around
              (lambda (fn &rest args)
                (if (eq major-mode 'org-agenda-mode)
                    nil
                  (apply fn args))))

  (setq org-roam-node-display-template
        (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))

  ;; wrap lines in backlinks buffer
  (add-hook 'org-roam-mode-hook #'visual-line-mode)

  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-direction)
                 (direction . right)
                 (window-width . 0.33)
                 (window-height . fit-window-to-buffer)))
  (add-to-list 'display-buffer-alist
               '("\\*org-roam\\*"
                 (display-buffer-in-direction)
                 (direction . bottom)
                 (window-height . 0.4)
                 (window-width . fit-window-to-buffer)))

  ;; visit nodes from backlinks buffer on click
  (defun who/org-roam-buffer-open-in-other-window ()
    (interactive)
    (let ((current-prefix-arg '(4))) ;; why 4?
      (call-interactively 'org-roam-preview-visit)))

  (define-key org-roam-mode-map [mouse-1] #'who/org-roam-buffer-open-in-other-window)

  ;; show backlinks on opening zettel. not doing this right now because it has a
  ;; lot of weird knock-on effects. maybe there's a more stable way to do it
  ;; (add-hook 'find-file-hook #'who-org/show-backlinks)
  ;;
  ;; (defun who-org/show-backlinks ()
  ;;   (when (and (not (get-buffer-window org-roam-buffer))
  ;;              (f-descendant-of? buffer-file-name (f-join (getenv "HOME") "org/zettelkasten")))
  ;;     (org-roam)))

  ;; Org page-break-lines-mode breaks org-roam-buffer. see
  ;; https://github.com/org-roam/org-roam/issues/1732,
  ;; https://github.com/org-roam/org-roam/issues/1772
  (global-page-break-lines-mode -1)
  (defun display-line-numbers-customize () (setq display-line-numbers t))
  (add-hook 'org-mode-hook 'display-line-numbers-customize)
  (advice-add 'org-roam-buffer-persistent-redisplay :before
			        (lambda () (remove-hook 'org-mode-hook 'display-line-numbers-customize)))
  (advice-add 'org-roam-buffer-persistent-redisplay :after
			        (lambda () (add-hook 'org-mode-hook 'display-line-numbers-customize)))
  ;; various other proposed solutions that didn't work:
  ;; (global-page-break-lines-mode +1)
  ;; (setq page-break-lines-modes
  ;;       '(emacs-lisp-mode lisp-mode scheme-mode compilation-mode help-mode))
  ;; (global-page-break-lines-mode -1)
  ;; (setq page-break-lines-modes nil)

  ;; Web publishing

  (org-export-define-derived-backend 'willopedia-html 'html
    :translate-alist '((template . who/willopedia-zettel-template)
                       (link . who/willopedia-link)
                       (headline . who/willopedia-headline)))

  (setq org-export-exclude-tags '("private" "noexport"))
  (setq org-export-with-properties '("NOTER_PAGE"))
  (setq org-export-preserve-breaks t)
  (setq org-export-with-entities t)
  (setq org-export-headline-levels 6)
  (setq org-export-with-toc nil)
  (setq org-export-with-section-numbers nil)
  (setq org-export-with-sub-superscripts nil)
  (setq org-export-with-tags nil)
  (setq org-export-with-broken-links 'mark)
  ;; (setq org-export-with-smart-quotes t) ;; needs some work on escaping

  (setq org-html-doctype "html5")
  (setq org-html-metadata-timestamp-format "%Y-%m-%d")
  (setq org-html-checkbox-type 'unicode)
  (setq org-html-html5-fancy t)
  (setq org-html-htmlize-output-type 'css)
  ;; (setq org-html-inline-images t)
  ;; (setq org-html-self-link-headlines t)
  ;; (setq org-html-doctype "html5")
  ;; (setq org-html-validation-link nil)

  ;; need to rebuild all to get the backlinks right anyway
  (setq org-publish-use-timestamps-flag nil)

  ;; Keybindings

  (spacemacs/declare-prefix "ar" "org-roam")
  (spacemacs/declare-prefix "ard" "org-roam-dailies")
  (spacemacs/declare-prefix "art" "org-roam-tags")
  (spacemacs/set-leader-keys
    "ardy" 'org-roam-dailies-goto-yesterday
    "ardt" 'org-roam-dailies-goto-today
    "ardT" 'org-roam-dailies-goto-tomorrow
    "ardd" 'org-roam-dailies-goto-date
    "arb"  'helm-bibtex
    "arf"  'org-roam-node-find
    "arm"  'who/org-roam-matter-create
    "arc"  'org-roam-node-capture
    "arg"  'org-roam-graph
    "ari"  'org-roam-node-insert
    "arl"  'org-roam-buffer-toggle
    "arta" 'org-roam-tag-add
    "arp"  'who/org-roam-mark-public
    "artd" 'org-roam-tag-delete
    "ara"  'org-roam-alias-add)

  (spacemacs/declare-prefix-for-mode 'org-mode "mr" "org-roam")
  (spacemacs/declare-prefix-for-mode 'org-mode "mrd" "org-roam-dailies")
  (spacemacs/declare-prefix-for-mode 'org-mode "mrt" "org-roam-tags")
  (spacemacs/set-leader-keys-for-major-mode 'org-mode
    "rb"  'org-roam-switch-to-buffer
    "rdy" 'org-roam-dailies-goto-yesterday
    "rdt" 'org-roam-dailies-goto-today
    "rdT" 'org-roam-dailies-goto-tomorrow
    "rdd" 'org-roam-dailies-goto-date
    "rf"  'org-roam-node-find
    "rm"  'who/org-roam-matter-create
    "rg"  'org-roam-graph
    "ri"  'org-roam-node-insert
    "rl"  'org-roam-buffer-toggle
    "rta" 'org-roam-tag-add
    "rp"  'who/org-roam-mark-public
    "rtd" 'org-roam-tag-delete
    "ra"  'org-roam-alias-add))

(defun who-org/init-org-roam-bibtex ()
  (use-package org-roam-bibtex
    :after org-roam))

(defun who-org/post-init-org-roam-bibtex ()
  (setq bibtex-completion-bibliography '("~/org/master.bib"))
  (setq bibtex-completion-pdf-field "file")
  (setq bibtex-completion-notes-path org-roam-directory)

  (setq orb-roam-ref-format 'org-cite)

  (setq who/org-roam-template-bib
        '("n" "bibliography notes" plain "%?"
          :target (file+head "${citekey}.org" "#+title: ${title}
#+filetags: :bib:public:

* Notes
:PROPERTIES:
:NOTER_DOCUMENT: ${file}
:END:
")
          :unnarrowed t))

  (advice-add #'orb-edit-note :around #'who/advise-orb-templates)

  (org-roam-bibtex-mode 1))

(defun who-org/post-init-org-roam-ui ()
  ;; Don't try to sync general Emacs theme with ORUI (I have custom ORUI themes)
  (setq org-roam-ui-sync-theme nil)

  ;; Sync theme after activating ORUI
  (advice-add 'org-roam-ui--ws-on-open
              :after
              (lambda (_ws)
                ;; I have no idea why but '(org-roam-ui-mode t) gets pushed to
                ;; my custom theme settings on (org-roam-ui-mode 1), so undo
                ;; that here. ??????????
                (custom-push-theme 'theme-value 'org-roam-ui-mode 'user 'reset)
                (when org-roam-ui-custom-theme
                  (org-roam-ui-sync-theme))))

  ;; Stop ORUI when closing browser tab
  (advice-add 'org-roam-ui--ws-on-close
              :after
              (lambda (_ws)
                (org-roam-ui-mode -1))))

(defun who-org/post-init-org-superstar ()
  (setq org-superstar-leading-bullet " ‧")
  (setq org-superstar-headline-bullets-list (list ?🟎))
  ;; Using variable-pitch fonts in org-mode (with ef-themes-mixed-fonts)
  ;; includes leading asterisks (or whatever org-superstar puts in there). Want
  ;; to keep headings indented

  ;; I don't think this is really the right way to override a face attribute but
  ;; whatever
  (advice-add #'org-superstar-mode
              :after
              (lambda ()
                (set-face-attribute 'org-superstar-leading nil :inherit 'fixed-pitch))))

(defun who-org/post-init-org-wild-notifier ()
  (setq org-wild-notifier-alert-time '(60 10 2))
  (setq org-wild-notifier-keyword-whitelist nil)
  (setq org-wild-notifier-keyword-blacklist '("DONE" "NOPE"))
  (setq org-wild-notifier-predicate-whitelist nil)
  (setq org-wild-notifier-predicate-blacklist
        (list
         ;; don't notify about habits
         (lambda (marker)
           (-contains? (org-entry-properties marker 'all)
                       '("STYLE" . "habit")))
         ;; don't notify about events with no specific time set
         (lambda (marker)
           (let ((timestamps (->> (list (org-entry-get marker "TIMESTAMP")
                                        (org-entry-get marker "SCHEDULED")
                                        (org-entry-get marker "DEADLINE"))
                                  (-remove #'null))))
             (if (null timestamps)
                 nil
               (-none? (lambda (ts)
                         (org-timestamp-has-time-p (org-timestamp-from-string ts)))
                       timestamps))))))

  (alert-define-style
    'who/alert-style-reminder
    :title "Agenda reminder"
    :notifier
    (lambda (info)
      (let* ((message (plist-get info :message))
             (info (plist-put info :persistent t))
             (info (if (or (s-contains? "in 2 minutes" message)
                           (s-contains? "right now" message))
                       (plist-put info :severity 'high)
                     info)))
        (alert-libnotify-notify info))))

  (add-to-list 'alert-user-configuration
               '(((:title . "Agenda")) who/alert-style-reminder))

  ;; I was having some trouble with Spacemacs org layer not enabling
  ;; org-wild-notifier-mode. Probably fine now but I keep this out of paranoia
  (org-wild-notifier-mode 1))

(defun who-org/init-persist ()
  (use-package persist
    :config
    (persist-defvar who/willopedia-published-commit ""
                    "Hash of last published commit of zettels.")))
