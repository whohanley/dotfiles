(defconst who-elfeed-packages
  '(elfeed))

(defun who-elfeed/init-elfeed ()
  (use-package elfeed
    :commands elfeed
    :init (spacemacs/set-leader-keys "are" #'elfeed)
    :config

    (setq elfeed-enclosure-default-dir (expand-file-name "~/Sync/feeds"))

    (add-hook 'elfeed-new-entry-hook #'who/elfeed-on-new-entry)

    ;; search customizations

    (setq elfeed-search-filter "@2-months-ago +unread")
    (setq elfeed-search-date-format '("%m-%d" 5 :left))
    (setq elfeed-search-title-max-width 120)
    (setq elfeed-show-entry-switch #'switch-to-buffer)
    (setq elfeed-search-header-function #'who/elfeed-search-header)
    (setq elfeed-search-print-entry-function #'who/elfeed-search-print-entry)

    ;; Will load elfeed after ef-themes, but might also load a new ef-theme
    ;; after loading elfeed. Account for both by calling
    ;; `who/ef-themes-custom-faces-elfeed' immediately and adding to the hook
    (add-hook 'ef-themes-post-load-hook #'who/ef-themes-custom-faces-elfeed)
    (who/ef-themes-custom-faces-elfeed)

    ;; feed definitions
    (load (expand-file-name "~/.config/elfeed/elfeed-feeds.el"))

    ;; https://github.com/skeeto/elfeed/issues/466#issuecomment-1275327427
    (define-advice elfeed-search--header (:around (oldfun &rest args))
      (if elfeed-db
          (apply oldfun args)
        "No database loaded yet")))

  ;; bindings

  (with-eval-after-load 'elfeed-search
    (keymap-set elfeed-search-mode-map "RET" #'who/elfeed-search-peek-entry)
    (keymap-set elfeed-search-mode-map "d"   #'elfeed-search-untag-all-unread)
    (keymap-set elfeed-search-mode-map "e"   #'who/elfeed-search-save-entry-audio)
    (keymap-set elfeed-search-mode-map "v"   #'elfeed-search-browse-url))

  (with-eval-after-load 'elfeed-show
    (keymap-set elfeed-show-mode-map "d" #'who/elfeed-show-untag-unread)
    (keymap-set elfeed-show-mode-map "e" #'who/elfeed-show-save-entry-audio)
    (keymap-set elfeed-show-mode-map "v" #'elfeed-show-visit)))
