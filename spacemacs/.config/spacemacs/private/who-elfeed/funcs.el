(defun who/elfeed-on-new-entry (entry)
  (let (title (elfeed-entry-title entry))
    (when (or (s-contains? "[Teaser]" title)
              (s-starts-with? "Premium - " title))
      (elfeed-untag entry "unread")
      (message "Ignoring entry %s" title))))

(defun who/elfeed-search-save-entry-audio ()
  "Download audio enclosures for selected entries."
  (interactive)
  (cl-loop for entry in (elfeed-search-selected)
           do (who/elfeed-save-entry-audio entry)))

(defun who/elfeed-show-save-entry-audio ()
  "Download audio enclosures for shown entry."
  (interactive)
  (who/elfeed-save-entry-audio elfeed-show-entry))

(defun who/elfeed-save-entry-audio (entry)
  "Download all audio enclosures for `entry'.

cf `elfeed-show-save-enclosure-multi'."
  (dolist (encl (elfeed-entry-enclosures entry))
    (pcase-let*
        ;; `elfeed-show-save-enclosure-multi' accesses enclosure URLs via
        ;; `aref'. I don't understand how that can work. I like `pcase'
        ;; better anyway
        ((`(,encl-url ,encl-mime) encl)
         (fname
          (who/sanitize-file-name
           ;; lots of files are named e.g. "media.mp3". uniquify with title
           (format "%s_%s"
                   (elfeed-entry-title entry)
                   (funcall elfeed-show-enclosure-filename-function entry encl-url))))
         (fpath (f-join elfeed-enclosure-default-dir fname)))
      (when (s-starts-with? "audio" encl-mime)
        (if (file-exists-p fpath)
            (message "Skipping download of existing enclosure %s" fname)
          (message "Starting download of enclosure %s to %s" encl-url fpath)
          (async-start
           `(lambda ()
              (url-copy-file ,encl-url ,fpath))
           `(lambda (_)
              (message "Finished downloading enclosure %s to %s" ,encl-url ,fpath))))))))

(defun who/elfeed-search-peek-entry (entry)
  "Display the currently selected item in a buffer, but do not remove the unread tag."
  (interactive (list (elfeed-search-selected :ignore-region)))
  (require 'elfeed-show)
  (when (elfeed-entry-p entry)
    (elfeed-search-update-entry entry)
    (unless elfeed-search-remain-on-entry (forward-line))
    (elfeed-show-entry entry)))

(defun who/elfeed-show-untag-unread ()
  (interactive)
  (elfeed-show-untag 'unread)
  (elfeed-kill-buffer))

(defun who/elfeed-search-header ()
  "Computes the string to be used as the Elfeed header."
  (cond
   ((zerop (elfeed-db-last-update))
    (elfeed-search--intro-header))
   ((> (elfeed-queue-count-total) 0)
    (let ((total (elfeed-queue-count-total))
          (in-process (elfeed-queue-count-active)))
      (format "%d jobs pending, %d active..."
              (- total in-process) in-process)))
   ((let* ((db-time (seconds-to-time (elfeed-db-last-update)))
           (update (format-time-string "%m-%d %H:%M" db-time)))
      (format " %s | %s"
              (propertize update 'face 'elfeed-search-last-update-face)
              (cond
               (elfeed-search-filter-active "")
               ((string-match-p "[^ ]" elfeed-search-filter)
                (propertize elfeed-search-filter
                            'face 'elfeed-search-filter-face))
               ("")))))))

(defun who/elfeed-search-print-entry (entry)
  "Print ENTRY to the buffer. Derived from `elfeed-search-print-entry--default'."
  (let* ((date (elfeed-search-format-date (elfeed-entry-date entry)))
         (title (or (elfeed-meta entry :title) (elfeed-entry-title entry) ""))
         (title-faces (elfeed-search--faces (elfeed-entry-tags entry)))
         (feed (elfeed-entry-feed entry))
         (feed-title
          (when feed
            (or (elfeed-meta feed :title) (elfeed-feed-title feed))))
         (tags (elfeed-entry-tags entry))
         ;; filter unread from printed tags
         (tags (-remove (lambda (tag) (eq 'unread tag)) tags))
         (tags (mapcar #'symbol-name tags))
         (tags-str (mapconcat
                    (lambda (s) (propertize s 'face 'elfeed-search-tag-face))
                    tags " "))
         (window-third-width (floor (/ (- (window-width) (length date) ) 3)))
         (elfeed-search-trailing-width window-third-width)
         (title-width (* 2 window-third-width))
         (title-column (elfeed-format-column
                        title (elfeed-clamp elfeed-search-title-min-width
                                            title-width
                                            elfeed-search-title-max-width)
                        :left)))
    (insert (propertize date 'face 'elfeed-search-date-face) "  ")
    (insert (propertize title-column 'face title-faces 'kbd-help title) " ")
    (when feed-title
      (insert (propertize feed-title 'face 'elfeed-search-feed-face) " "))
    (when tags
      (insert tags-str))))

(defun who/ef-themes-custom-faces-elfeed ()
  "ef-themes overrides for Elfeed."
  (ef-themes-with-colors
    (custom-set-faces
     `(elfeed-search-feed-face ((,c :foreground ,fg-main)))
     `(elfeed-search-filter-face ((,c :foreground ,accent-0)))
     `(elfeed-search-title-face ((,c :foreground ,fg-main))))))
