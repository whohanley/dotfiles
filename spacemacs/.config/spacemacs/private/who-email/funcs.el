(defun who/mailto (to)
  "Handler for mailto protocol (normally invoked via desktop file Exec)."
  (notmuch-mua-mail (s-replace "mailto:" "" (url-unhex-string to))))

(defun who/open-email-inbox ()
  "Open and refresh the notmuch inbox buffer, or create a new one if necessary."
  (interactive)
  (let ((inbox-buffer (get-buffer "*notmuch-saved-tree-inbox*")))
    (if inbox-buffer
        (progn
          (switch-to-buffer inbox-buffer)
          (notmuch-refresh-this-buffer))
      (notmuch-tree "tag:inbox"))))

(defun who/matches-elc-email (str)
  "True iff STR contains my ELC email address."
  (and (not (s-blank? str))
       (s-contains? "wohanley@everyonelegal.ca" str t)))

(defun who/check-email-zero ()
  (when (equal 0 (notmuch-call-notmuch-sexp "count" "tag:inbox"))
    (let ((marker (org-id-find "0ea2e1d5-3a8a-466c-8528-c2ce278f5057" 'marker)))
      (with-current-buffer (marker-buffer marker)
        (goto-char marker)
        (unless
            ;; skip transition if already zeroed today
            (org-time>= (org-entry-get marker "LAST_REPEAT")
                        ;; see https://emacs.stackexchange.com/questions/78438/converting-day-value-to-a-timestamp
                        (time-to-seconds (days-to-time (- (org-today)
                                                          (date-to-day "1970-01-01")))))
          (org-todo 'done))
        ;; this `move-marker' is apparently a good cleanup thing to do? idk but
        ;; `org-id-goto' does it
        (move-marker marker nil)))))

;;;
;; notmuch-search
;;;

(defun who/notmuch-search-uninbox ()
  (interactive)
  (notmuch-search-tag (list "-inbox" "-unread"))
  (notmuch-search-next-thread)
  (who/check-email-zero))

(defun who/notmuch-search-toggle-unread ()
  (interactive)
  (if (member "unread" (notmuch-search-get-tags))
      (notmuch-search-tag (list "-unread"))
    (notmuch-search-tag (list "+unread"))))

(defun who/notmuch-search-ham ()
  (interactive)
  (ham-start-process (notmuch-search-find-thread-id))
  (notmuch-search-tag (list "-spam" "-maybe-spam")))

(defun who/notmuch-search-spam ()
  (interactive)
  (spam-start-process (notmuch-search-find-thread-id))
  (notmuch-search-tag (list "+spam" "-maybe-spam" "-inbox"))
  (notmuch-search-next-thread))

;;;
;; notmuch-tree
;;;

(defun who/notmuch-tree-uninbox ()
  (interactive)
  (notmuch-tree-tag-thread (list "-inbox" "-unread"))
  (notmuch-tree-next-thread-in-tree)
  (notmuch-tree-show-message-in)
  (who/check-email-zero))

(defun who/notmuch-tree-toggle-unread ()
  (interactive)
  (if (member "unread" (notmuch-search-get-tags))
      (notmuch-tree-tag (list "-unread"))
    (notmuch-tree-tag (list "+unread"))))

(defun who/notmuch-tree-ham ()
  (interactive)
  (ham-start-process (notmuch-tree-get-messages-ids-thread-search))
  (notmuch-tree-tag-thread (list "-spam" "-maybe-spam")))

(defun who/notmuch-tree-spam ()
  (interactive)
  (spam-start-process (notmuch-tree-get-messages-ids-thread-search))
  (notmuch-tree-tag-thread (list "+spam" "-maybe-spam" "-inbox"))
  (notmuch-tree-next-matching-message))

;;;
;; notmuch-show
;;;

;; want to split pane horizontally instead of vertically if there's space a
;; la org-roam, but vertical split is hardcoded into notmuch. maybe fix
;; later
;;
;; (add-to-list 'display-buffer-alist
;;              '("\\*notmuch-id*"
;;                (display-buffer-in-direction)
;;                (direction . right)
;;                (window-width . 0.5)
;;                (window-height . fit-window-to-buffer)))
;; (add-to-list 'display-buffer-alist
;;              '("\\*notmuch-id*"
;;                (display-buffer-in-direction)
;;                (direction . bottom)
;;                (window-height . 0.6)
;;                (window-width . fit-window-to-buffer)))

(defun who/notmuch-show-pop-uninbox ()
  (interactive)
  (notmuch-bury-or-kill-this-buffer)
  (who/notmuch-search-uninbox))

(defun who/notmuch-show-jump-to-latest ()
  "Jump to the message in the current thread with the latest
timestamp."
  (interactive)
  (let ((timestamp 0)
        latest)
    (notmuch-show-mapc
     (lambda () (let ((ts (notmuch-show-get-prop :timestamp)))
                  (when (> ts timestamp)
                    (setq timestamp ts
                          latest (point))))))
    (if latest
        (progn
          (goto-char latest)
          (recenter-top-bottom 10)) ;; scroll down to message
      (error "Cannot find latest message."))))

(defun who/org-capture-email ()
  (interactive)
  (org-capture nil "e"))

;; inline seems less weird. still not really sure how attachment vs inline forwarding works
(defun who/notmuch-show-forward-inline (prefix)
  (interactive "P")
  (let ((message-forward-as-mime nil)
        (message-forward-ignored-headers ".*"))
    (notmuch-show-forward-message prefix)))

;;;
;; util
;;;

(defun spam-start-process (query)
  "Start a process to mark selected messages as spam with bogofilter."
  (start-process-shell-command "bogofilter-spam" "*bogofilter*"
                               (concat notmuch-command " search --output=files "
                                       ;; tag:spam finagling is important - without it we might
                                       ;; have a race with a corresponding 'notmuch tag'
                                       "'(tag:spam or not tag:spam) and (" query ")'"
                                       " | bogofilter -s -b -v")))

(defun ham-start-process (query)
  "Start a process to mark selected messages as ham with bogofilter."
  (start-process-shell-command "bogofilter-ham" "*bogofilter*"
                               (concat notmuch-command " search --output=files "
                                       ;; tag:spam finagling is important - without it we might
                                       ;; have a race with a corresponding 'notmuch tag'
                                       "'(tag:spam or not tag:spam) and (" query ")'"
                                       " | bogofilter -n -b -v")))


;;;
;; gmail replies
;;;

(defun who/search-gmail-message-id (message-id)
  "Search for the current Gmail message in the web client."
  (browse-url
   (format "https://mail.google.com/mail/u/0/#search/rfc822msgid%%3A%s"
           (url-hexify-string (substring message-id 3)))))

(defun who/notmuch-reply-dispatch (view sender)
  (let* ((props (if (equal view 'show)
                    (notmuch-show-get-message-properties)
                  (notmuch-tree-get-message-properties)))
         (headers (plist-get props :headers))
         (msg-id (notmuch-show-get-message-id)))
    (if (or (who/matches-elc-email (plist-get headers :To))
            (who/matches-elc-email (plist-get headers :Cc))
            (who/matches-elc-email (plist-get headers :Bcc)))
        (who/search-gmail-message-id msg-id)
      (cond
       ((and (equal view 'show) (not sender))
        (call-interactively #'notmuch-show-reply))

       ((and (equal view 'show) sender)
        (call-interactively #'notmuch-show-reply-sender))

       ((and (equal view 'tree) (not sender))
        (call-interactively #'notmuch-tree-reply))

       ((and (equal view 'tree) sender)
        (call-interactively #'notmuch-tree-reply-sender))))))

(defun who/notmuch-show-reply ()
  (interactive)
  (who/notmuch-reply-dispatch 'show nil))

(defun who/notmuch-show-reply-sender ()
  (interactive)
  (who/notmuch-reply-dispatch 'show t))

(defun who/notmuch-tree-reply ()
  (interactive)
  (who/notmuch-reply-dispatch 'tree nil))

(defun who/notmuch-tree-reply-sender ()
  (interactive)
  (who/notmuch-reply-dispatch 'tree t))

;; Cosmetics

(defun who/ef-themes-custom-faces-notmuch ()
  (ef-themes-with-colors
    (custom-set-faces
     `(notmuch-search-date ((,c :foreground ,accent-0)))
     `(notmuch-search-matching-authors ((,c :foreground ,fg-main)))
     `(notmuch-tree-match-author-face ((,c :foreground ,fg-main))))))

(defun who/notmuch-insert-end-of-results (f &rest args)
  "Add shadow face to 'End of search results.'

I cannot get the simplest imaginable font-locking to work, so this advice thing
is actually easier.
No effect: (font-lock-add-keywords 'notmuch-tree-mode '((\"search\" . 'shadow)))"
  (if (equal args (list "End of search results."))
      (apply f (list (propertize "End of search results." 'face 'shadow)))
    (apply f args)))
