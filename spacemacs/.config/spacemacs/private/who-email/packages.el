;;; packages.el --- who-email layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2018 Sylvain Benner & Contributors
;;
;; Author:  <wohanley@pigeon>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `who-email-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `who-email/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `who-email/pre-init-PACKAGE' and/or
;;   `who-email/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst who-email-packages
  '(org
    (notmuch :url "https://git.notmuchmail.org/git/notmuch"
             :fetcher git
             :branch "release"
             :files ("emacs/*.el" "emacs/*.png"))
    (notmuch-calendar-x :location local)
    ol-notmuch)
  "The list of Lisp packages required by the who-email layer.

Each entry is either:

1. A symbol, which is interpreted as a package to be installed, or

2. A list of the form (PACKAGE KEYS...), where PACKAGE is the
    name of the package to be installed or loaded, and KEYS are
    any number of keyword-value-pairs.

    The following keys are accepted:

    - :excluded (t or nil): Prevent the package from being loaded
      if value is non-nil

    - :location: Specify a custom installation location.
      The following values are legal:

      - The symbol `elpa' (default) means PACKAGE will be
        installed using the Emacs package manager.

      - The symbol `local' directs Spacemacs to load the file at
        `./local/PACKAGE/PACKAGE.el'

      - A list beginning with the symbol `recipe' is a melpa
        recipe.  See: https://github.com/milkypostman/melpa#recipe-format")

(defun who-email/post-init-org ()
  (require 'ol-notmuch))

(defun who-email/init-notmuch ()
  (use-package notmuch
    :preface (setq-default notmuch-command (executable-find "notmuch"))
    :commands (notmuch notmuch-tree)
    :if (executable-find "notmuch")
    :bind (("<f2>" . who/open-email-inbox)
           :map notmuch-search-mode-map
           ("d" . who/notmuch-search-uninbox)
           ("y" . who/notmuch-search-toggle-unread)
           ("a" . notmuch-search-archive-thread)
           ("H" . who/notmuch-search-ham)
           ("h" . who/notmuch-search-spam)
           ("c" . who/org-capture-email)
           ("r" . notmuch-search-reply-to-thread)
           ("R" . notmuch-search-reply-to-thread-sender)
           :map notmuch-tree-mode-map
           ("d" . who/notmuch-tree-uninbox)
           ("H" . who/notmuch-tree-ham)
           ("h" . who/notmuch-tree-spam)
           ("y" . who/notmuch-tree-toggle-unread)
           ("c" . who/org-capture-email)
           ("r" . who/notmuch-tree-reply-sender)
           ("R" . who/notmuch-tree-reply)
           :map notmuch-show-mode-map
           ("d" . who/notmuch-show-pop-uninbox)
           ("l" . who/notmuch-show-jump-to-latest)
           ("<tab>" . org-next-link)
           ("<backtab>". org-previous-link)
           ("C-<return>" . browse-url-at-point)
           ("c" . who/org-capture-email)
           ("f" . who/notmuch-show-forward-inline)
           ("F" . notmuch-show-forward-message)
           ("r" . who/notmuch-show-reply-sender)
           ("R" . who/notmuch-show-reply))

    :config
    (setq notmuch-identities '("William O'Hanley <me@wohanley.com>"
                               "William O'Hanley <wohanley@law.stanford.edu>"
                               "William O'Hanley <wohanley@stanford.edu>"
                               "William O'Hanley <whohanley@uvic.ca>"
                               "William O'Hanley <willy.ohanley@gmail.com>"
                               "William O'Hanley <william.ohanley@gmail.com>"
                               "William O'Hanley <wohanley@everyonelegal.ca>"))

    ;; not sure why but completions don't work with company
    (setq notmuch-address-use-company nil)

    ;; check for text signalling that there should be an attachment
    (setq notmuch-mua-attachment-regexp "\\(attach\\|\\bhere's\\b\\)")
    (add-hook 'notmuch-mua-send-hook 'notmuch-mua-attachment-check)

    ;; Cosmetics

    (setq notmuch-tree-result-format '(("date" . "%12s  ")
                                       ("authors" . "%-20s")
                                       ((("tree" . "%s")
                                         ("subject" . "%s"))
                                        . " %-54s ")
                                       ("tags" . "%s")))
    (setq notmuch-search-result-format '(("date" . "%12s ")
                                         ("count" . "%-7s ")
                                         ("authors" . "%-20s ")
                                         ("subject" . "%s ")
                                         ("tags" . "%s")))


    ;; Use proportional fonts
    (add-hook 'notmuch-message-mode-hook #'who/buffer-face-mode-human)
    (add-hook 'notmuch-show-mode-hook (lambda () (buffer-face-set 'variable-pitch)))

    (add-hook 'ef-themes-post-load-hook #'who/ef-themes-custom-faces-notmuch)

    (advice-add #'notmuch-tree-process-sentinel
                :around
                (lambda (f &rest args)
                  (advice-add #'insert :around #'who/notmuch-insert-end-of-results)
                  (apply f args)
                  (advice-remove #'insert #'who/notmuch-insert-end-of-results)))

    ;; show absolute dates
    ;; (defun who/notmuch-tree-date-format (fstring thread)
    ;;   (format fstring (plist-get thread :date_relative)))
    ;; (setq notmuch-tree-result-format
    ;;       `(
    ;;         ;; ("date" . "%12s ")
    ;;         (who/notmuch-tree-date-format . "%12s ")
    ;;         ("authors" . "%-20s")
    ;;         ((("tree" . "%s")
    ;;           ("subject" . "%s"))
    ;;          . " %-54s ")
    ;;         ("tags" . "(%s)")))
    ;; (setq notmuch-show-relative-dates nil)

    :custom
    (message-auto-save-directory "~/.mail/drafts/")

    (message-send-mail-function 'message-send-mail-with-sendmail)
    (sendmail-program "~/scripts/mail/msmtpq")

    ;; pick SMTP server based on envelope from: per https://notmuchmail.org/emacstips/#index11h2
    (message-sendmail-envelope-from 'header)
    (mail-envelope-from 'header)
    (mail-specify-envelope-from t)

    ;; where to save sent mail
    (notmuch-fcc-dirs '((".*@wohanley.com" . "me/Sent")
                        ("willy.ohanley@gmail.com" . "gmail/Sent")
                        ("william.ohanley@gmail.com" . "gmail-william/Sent")
                        ("wohanley@everyonelegal.ca" . "elc/Sent")
                        ("whohanley@uvic.ca" . "uvic/Sent")))

    (message-sendmail-f-is-evil nil)
    (message-kill-buffer-on-exit t)
    (notmuch-always-prompt-for-sender t)
    (notmuch-crypto-process-mime t)
    ;; (notmuch-labeler-hide-known-labels t)
    (notmuch-search-oldest-first nil)
    (notmuch-archive-tags '("+archived" "-inbox" "-unread"))
    (notmuch-message-headers '("To" "Cc" "Subject" "Bcc"))
    (notmuch-saved-searches '((:name "inbox" :query "tag:inbox" :key "j" :search-type tree)
                              (:name "unread" :query "tag:inbox and tag:unread" :key "u")
                              (:name "spam" :query "tag:spam" :key "s")
                              (:name "drafts" :query "tag:draft" :key "d")))
    (notmuch-hello-sections '(notmuch-hello-insert-saved-searches))))

(defun who-email/init-notmuch-calendar-x ()
  (use-package notmuch-calendar-x
    :after notmuch
    :config
    (setq notmuch-calendar-capture-target '("~/org/gtd/calendars/personal.org"))
    (setq notmuch-calendar-default-tags '())))

(defun who-email/init-ol-notmuch ()
  (use-package ol-notmuch
    :after (notmuch org)))

;;; packages.el ends here
