#!/usr/bin/env bash

# Start XScreenSaver after idling, then suspend after more idling.
#
# XScreenSaver itself is set to activate after 12 hours (its maximum) so it
# stays out of the way as far as activation.
#
# n.b. avoid --not-when-fullscreen - XScreenSaver seems to count as fullscreen

xidlehook \
    --not-when-audio \
    --detect-sleep \
    --timer 840 'xscreensaver-command -activate' '' \
    --timer 20 'xscreensaver-command -lock' '' \
    --timer 900 'systemctl suspend' ''
