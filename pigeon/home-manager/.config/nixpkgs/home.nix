{ config, pkgs, ... }:

let
  home = "/home/wohanley";
  scripts = "${home}/scripts";
  nix-profile = "${home}/.nix-profile";
in {
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "19.09";

  # https://github.com/NixOS/nix/issues/599
  # https://github.com/rycee/home-manager/issues/354
  home.sessionVariables.LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";

  home.sessionVariables.FONTCONFIG_FILE = pkgs.makeFontsConf { fontDirectories = pkgs.texlive.tex-gyre.pkgs; };

  # Something is setting SSH_AUTH_SOCK incorrectly even before .profile is
  # sourced. I have no idea what it is and I am so fucking tired of searching.
  # This overrides the incorrect value. Does this kludge have negative side
  # effects that aren't immediately obvious? Probably! I love computers.
  home.sessionVariables.SSH_AUTH_SOCK = "$(${pkgs.gnupg}/bin/gpgconf --list-dirs agent-ssh-socket)";

  home.packages = [
    pkgs.emacs-all-the-icons-fonts
    pkgs.glibcLocales # for LOCALE_ARCHIVE
    pkgs.entr
    pkgs.fzf
    pkgs.lyx
    pkgs.parallel
    pkgs.pass
    pkgs.ripgrep
    (pkgs.texlive.combine { inherit (pkgs.texlive) scheme-medium
      # simple-resume-cv dependencies
      datetime2
      hyphenat; })
  ];

  services.syncthing = {
    enable = true;
    tray = false; # also install qsyncthingtray. note this pulls in a lot from Qt
  };

  services.gpg-agent = {
    enable = true;
    enableSshSupport = true;
    defaultCacheTtl = 86400;
    maxCacheTtl = 86400;
    defaultCacheTtlSsh = 86400;
    maxCacheTtlSsh = 86400;
    extraConfig = ''
      pinentry-program /usr/bin/pinentry
    '';
  };

  # services.sxhkd = {
  #   enable = true;
  #   keybindings = {
  #     "slash ; s ; h ; r ; u ; g" = "${scripts}/replace-typed-text.sh /shrug";
  #   };
  # };

  ###
  # Email
  ###

  accounts.email = {
    maildirBasePath = "/home/wohanley/.mail";
    accounts.me = {
      address = "me@wohanley.com";
      primary = true;
      userName = "me@wohanley.com";
      passwordCommand = "${pkgs.pass}/bin/pass email/me";
      realName = "William O'Hanley";
      imap.host = "mail.wohanley.com";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "Drafts" "Junk" "Sent" ];
        onNotify = "${scripts}/mail/on-new-mail.sh me";
      };
      smtp = {
        host = "mail.wohanley.com";
        port = 587;
        tls.useStartTls = true;
      };
      msmtp.enable = true;
      notmuch.enable = true;
    };
    accounts.stanford = {
      address = "wohanley@stanford.edu";
      userName = "wohanley@stanford.edu";
      passwordCommand = "${pkgs.pass}/bin/pass sunet";
      realName = "William O'Hanley";
      imap.host = "outlook.office365.com";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "Drafts" "Junk Email" "Sent Items" ];
        onNotify = "${scripts}/mail/on-new-mail.sh stanford";
      };
      # SMTP auth is all fucky because of Outlook :/ so I just don't bother, but maybe the XOAUTH2
      # stuff I did for the stanford-law GMail would work here too
      notmuch.enable = true;
    };
    accounts.stanford-law = {
      address = "wohanley@law.stanford.edu";
      userName = "wohanley@law.stanford.edu";
      passwordCommand = "${scripts}/mutt_oauth2.py ${home}/.mutt-oauth2/stanford-law-oauth-tokens";
      realName = "William O'Hanley";
      flavor = "gmail.com";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
        extraConfig.account.authMechs = "XOAUTH2";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "[Gmail]/All Mail" "[Gmail]/Sent Mail" "[Gmail]/Spam" ];
        onNotify = "${scripts}/mail/on-new-mail.sh stanford-law";
        extraConfig.xoauth2 = true;
      };
      msmtp = {
        enable = true;
        extraConfig.auth = "oauthbearer";
      };
      notmuch.enable = true;
    };
    accounts.uvic = {
      address = "whohanley@uvic.ca";
      userName = "whohanley";
      passwordCommand = "${pkgs.pass}/bin/pass email/uvic";
      realName = "William O'Hanley";
      imap.host = "imap.uvic.ca";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "Drafts" "Junk" "Sent" ];
        onNotify = "${scripts}/mail/on-new-mail.sh uvic";
      };
      smtp = {
        host = "smtp.uvic.ca";
        port = 587;
        tls.useStartTls = true;
      };
      msmtp = {
        enable = true;
      };
    };
    accounts.gmail-william = {
      address = "william.ohanley@gmail.com";
      userName = "william.ohanley@gmail.com";
      passwordCommand = "${scripts}/mutt_oauth2.py ${home}/.mutt-oauth2/google-william-ohanley-oauth-tokens";
      realName = "William O'Hanley";
      flavor = "gmail.com";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
        extraConfig.account.authMechs = "XOAUTH2";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "[Gmail]/All Mail" "[Gmail]/Sent Mail" "[Gmail]/Spam" ];
        onNotify = "${scripts}/mail/on-new-mail.sh gmail-william";
        extraConfig.xoauth2 = true;
      };
      msmtp = {
        enable = true;
        extraConfig.auth = "oauthbearer";
      };
    };
    accounts.gmail = {
      address = "willy.ohanley@gmail.com";
      userName = "willy.ohanley@gmail.com";
      passwordCommand = "${scripts}/mutt_oauth2.py ${home}/.mutt-oauth2/google-willy-ohanley-oauth-tokens";
      realName = "William O'Hanley";
      flavor = "gmail.com";
      mbsync = {
        enable = true;
        create = "both";
        remove = "both";
        expunge = "both";
        extraConfig.account.authMechs = "XOAUTH2";
      };
      imapnotify = {
        enable = true;
        boxes = [ "INBOX" "Junk" "Sent" "Willy.OHanley@dal.ca" "[Gmail]/All Mail" "[Gmail]/Sent Mail" "[Gmail]/Spam" ];
        onNotify = "${scripts}/mail/on-new-mail.sh gmail";
        extraConfig.xoauth2 = true;
      };
      msmtp = {
        enable = true;
        extraConfig.auth = "oauthbearer";
      };
    };
  };

  # I need XOAUTH2 to read my @law.stanford.edu email. mbsync needs to be built locally to pick up
  # the cyrus-sasl XOAUTH2 plugin (???) so it lives in ~/bin/mbsync and I update it manually when
  # necessary. I can't get home-manager to build my mbsync config file without also installing
  # mbsync so I rely on PATH precedence to get the ~/bin one picked up. This sucks but I am so tired
  # of trying to get my email working. See:
  #  - https://unix.stackexchange.com/questions/625637/configuring-mbsync-with-authmech-xoauth2
  #  - https://bbs.archlinux.org/viewtopic.php?id=238727
  #  - http://blog.onodera.asia/2020/06/how-to-use-google-g-suite-oauth2-with.html
  programs.mbsync.enable = true;

  services.imapnotify.enable = false;

  programs.msmtp.enable = true;

  programs.notmuch = {
    # unfortunately notmuch is also installed through pacman, because I can't
    # figure out how to make the shared lib available to python if it's
    # installed through nix
    enable = true;
    new.tags = [ "new" ];
    # this doesn't actually work, needs to be added to the DB but the config set
    # command fails because notmuchrc is unwritable. also not sure this is the
    # right format anyway
    extraConfig = {
      headers = {
        XSpamFlag = "X-Spam-Flag";
        XBogosity = "X-Bogosity";
      };
    };
    hooks.postNew = ''
      # Apply initial tags (sent, notify, etc)
      ${pkgs.notmuch}/bin/notmuch tag --input ${scripts}/mail/notmuch-post-new-tags
      # Train bogofilter on my own sent mail (ham)
      ${pkgs.notmuch}/bin/notmuch search --output=files tag:new and tag:sent | xargs bogofilter -nB
      # Remove new, we're done
      ${pkgs.notmuch}/bin/notmuch tag -new -- tag:new
    '';
    search.excludeTags = ["deleted" "spam"];
  };

  # notmuch-show-view-part uses .mailcap for file associations. not sure what
  # mechanism I'm using in general or why it doesn't apply here - investigate
  home.file.".mailcap".text = ''
    application/vnd.openxmlformats-officedocument.wordprocessingml.document; /usr/bin/xdg-open %s
  '';

  systemd.user.services.fetch-mail = {
    Unit = {
      Description = "Fetch mail";
    };
    Service = {
      Type = "oneshot";
      ExecStart = "${scripts}/mail/on-new-mail.sh";
      # failures likely, network is slow to come up
      Restart = "on-failure";
      RestartSec = 20;
      # stop retrying after a while, might just not have network
      StartLimitInterval = 300;
      StartLimitBurst = 6;
    };
    Install = {
      # run on login. imapnotify runs separately after that
      WantedBy = [ "default.target" ];
    };
  };

  systemd.user.services.picom = {
    Unit = {
      Description = "Picom display compositor";
      After = [ "graphical-session-pre.target" ];
      PartOf = [ "graphical-session.target" ];
    };
    Service = {
      ExecStart = "picom";
      Restart = "always";
      RestartSec = 3;
    };
    Install = {
      WantedBy = [ "graphical-session.target" ];
    };
  };

  # Check wohanley.com guestbook

  systemd.user.services.check-guestbook = {
    Unit = {
      Description = "Check wohanley.com guestbook for changes";
    };
    Service = {
      Type = "oneshot";
      WorkingDirectory = "${scripts}/guestbook";
      ExecStart = "${scripts}/guestbook/check-for-updates.sh";
      # failures likely, network is slow to come up
      Restart = "on-failure";
      RestartSec = 20;
      # stop retrying after a while, might just not have network
      StartLimitInterval = 300;
      StartLimitBurst = 6;
    };
  };

  systemd.user.timers.check-guestbook = {
    Unit = {
      Description = "Check wohanley.com guestbook for changes daily";
    };
    Timer = {
      OnBootSec = "15min";
      OnUnitActiveSec = "1d";
    };
    Install = {
      WantedBy = [ "timers.target" ];
    };
  };

  # Rotate desktop wallpaper

  systemd.user.services.rotate-wallpaper = {
    Unit = {
      Description = "Rotate desktop wallpaper";
    };
    Service = {
      Type = "oneshot";
      WorkingDirectory = "${scripts}/wallpaper";
      ExecStart = "${scripts}/wallpaper/set-wallpaper.sh";
    };
  };

  systemd.user.timers.rotate-wallpaper = {
    Unit = {
      Description = "Rotate wallpaper every now and then";
    };
    Timer = {
      OnCalendar="*:0/20"; # every 20 minutes
    };
    Install = {
      WantedBy = [ "timers.target" ];
    };
  };

  # Delete old Nix generations. They will take up the entire drive otherwise

  systemd.user.services.prune-nix = {
    Unit = {
      Description = "Remove old Nix generations";
    };
    Service = {
      Type = "oneshot";
      ExecStart = "${nix-profile}/bin/nix-collect-garbage --delete-older-than 30d";
    };
  };

  systemd.user.timers.prune-nix = {
    Unit = {
      Description = "Remove old Nix generations weekly";
    };
    Timer = {
      OnCalendar="weekly";
      Persistent=true;
    };
    Install = {
      WantedBy = [ "timers.target" ];
    };
  };
}
