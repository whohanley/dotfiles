#!/bin/bash

brightness=$(cat "/sys/class/leds/asus::kbd_backlight/brightness")

if [ "$1" == "up" ] && [ "$brightness" -lt 3 ]; then
    echo $((brightness + 1)) | sudo tee "/sys/class/leds/asus::kbd_backlight/brightness"
elif [ "$1" == "down" ] && [ "$brightness" -gt 0 ]; then
    echo $((brightness - 1)) | sudo tee "/sys/class/leds/asus::kbd_backlight/brightness"
fi
