#!/usr/bin/env zsh

$HOME/scripts/kbd-backlight.sh 0

sed -ie 's/"Adwaita-dark"/"Adwaita"/' "${HOME}/.config/xsettingsd/xsettingsd.conf"
killall -HUP xsettingsd

gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita'
gsettings set org.gnome.desktop.interface icon-theme 'breeze'
gsettings set org.gnome.desktop.interface color-scheme 'prefer-light'

$HOME/scripts/wallpaper/set-wallpaper.sh

emacsclient --eval "(who/load-theme-light)"

VSC_SETTINGS_PATH="${HOME}/.config/Code - OSS/User/settings.json"
cat "$VSC_SETTINGS_PATH" | jq '."workbench.colorTheme" |= "Default Light+"' > tmp.json
mv tmp.json "$VSC_SETTINGS_PATH"

KITTY_THEMES_NOCACHE=''
if [[ $(nmcli con show --active | grep -c 'vpn\|wireguard') -gt 0 ]]; then
    # Don't try to download an updated version of the theme over VPN, connection
    # is often blocked
    KITTY_THEMES_NOCACHE='--cache-age=-1'
fi

kitty +kitten themes ${=KITTY_THEMES_NOCACHE} --config-file-name 'themes.conf' --reload-in=all 'vimbones'
