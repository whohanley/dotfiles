#!/usr/bin/env zsh

$HOME/scripts/kbd-backlight.sh 3

sed -ie 's/"Adwaita"/"Adwaita-dark"/' "${HOME}/.config/xsettingsd/xsettingsd.conf"
killall -HUP xsettingsd

gsettings set org.gnome.desktop.interface gtk-theme 'Adwaita-dark'
gsettings set org.gnome.desktop.interface icon-theme 'breeze-dark'
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'

$HOME/scripts/wallpaper/set-wallpaper.sh

emacsclient --eval "(who/load-theme-dark)"

VSC_SETTINGS_PATH="${HOME}/.config/Code - OSS/User/settings.json"
cat "${VSC_SETTINGS_PATH}" | jq '."workbench.colorTheme" |= "Default Dark+"' > tmp.json
mv tmp.json "$VSC_SETTINGS_PATH"

KITTY_THEMES_NOCACHE=''
if [[ $(nmcli con show --active | grep -c 'vpn\|wireguard') -gt 0 ]]; then
    # Don't try to download an updated version of the theme over VPN, connection
    # is often blocked
    KITTY_THEMES_NOCACHE='--cache-age=-1'
fi

kitty +kitten themes ${=KITTY_THEMES_NOCACHE} --config-file-name 'themes.conf' --reload-in=all 'Everforest Dark Hard'
