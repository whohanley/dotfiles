#!/usr/bin/env sh

# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	      . "$HOME/.bashrc"
    fi
fi

# Arch problems, see https://www.reddit.com/r/archlinux/comments/u7d23c/visual_studio_code_live_share_broken_all_of/
export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1

export PATH="$PATH:~/.dotnet/tools"

export JAVA_HOME="/lib/jvm/default"
export JDK_HOME="/lib/jvm/default"
# Java doesn't like i3
export _JAVA_AWT_WM_NONREPARENTING=1
# Java doesn't anti-alias text by default, which produces god awful ugly text. These options sort of fix it
export _JAVA_OPTIONS="-Dawt.useSystemAAFontSettings=on -Dswing.aatext=true -Dsun.java2d.xrender=true"

export GOPATH="$HOME/code/go"
export PYTHONPATH="$PYTHONPATH:$HOME/lib/python"

# Nix stuff. A lot of this is stragglers from pigeon, doesn't seem necessary
# here, but makes for instructive examples
export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}
#export NIX_PATH=$HOME/.nix-defexpr/channels${NIX_PATH:+:}$NIX_PATH # nixpkgs unstable
#export NIX_PATH="nixpkgs=$HOME/code/nixpkgs" # local nixpkgs
export NIX_REMOTE=daemon
#. "$HOME/.nix-profile/etc/profile.d/nix.sh"
#. "$HOME/.nix-profile/etc/profile.d/nix-daemon.sh"
#. /nix/var/nix/profiles/default/etc/profile.d/nix.sh
#. /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh
# include home-manager variables
. "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"

# Note that something Nix-related (home-manager, I think) puts Nix bins at the
# front of PATH after X init. I don't know how and can't override it.
# cf ../home-manager/.config/environment.d/20-home-bin-precedence.conf
export PATH="$HOME/bin:$HOME/scripts:$HOME/Applications/bin:$HOME/.local/bin:$HOME/opt/node/node_modules/.bin:$GOPATH/bin:$HOME/opt/activator-1.3.2:$HOME/.local/share/gem/ruby/2.7.0/bin:$HOME/.rbenv/bin:/usr/local/heroku/bin:$HOME/games/glulx:$PATH"

# Karma (?) needs this to use headless Chromium as JS test runner
export CHROME_BIN=chromium

# custom LaTeX packages
export TEXINPUTS="$TEXINPUTS::$HOME/texmf"

# Make environment variables (particularly Nix paths) available to systemd user units
systemctl --user import-environment PATH MSMTP_LOG MSMTP_QUEUE NMBGIT NOTMUCH_CONFIG

# gpg-agent needs this. not sure why https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPG_002dAGENT.html
GPG_TTY=$(tty)
export GPG_TTY

export QT_QPA_PLATFORMTHEME='qt5ct'

export TERMINAL=kitty
export EDITOR=nano

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
