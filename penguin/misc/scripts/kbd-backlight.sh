#!/bin/bash

brightness=$(cat "/sys/devices/platform/dell-laptop/leds/dell::kbd_backlight/brightness")

if [ "$1" == "up" ] && [ "$brightness" -lt 3 ]; then
    echo $((brightness + 1)) | sudo tee "/sys/devices/platform/dell-laptop/leds/dell::kbd_backlight/brightness"
elif [ "$1" == "down" ] && [ "$brightness" -gt 0 ]; then
    echo $((brightness - 1)) | sudo tee "/sys/devices/platform/dell-laptop/leds/dell::kbd_backlight/brightness"
fi
