#!/usr/bin/env bash

curl --silent https://etherpad.wikimedia.org/p/MguL8M0I7lJ9i636WLFM/export/txt > current

if [ ! $? -eq 0 ]; then
    exit 1; # probably network, systemd will retry
fi

if [ ! -f last ]; then
  cp current last
fi

diff last current

if [ ! $? -eq 0 ]; then
    ACTION=$(dunstify --action="default,Show" "wohanley.com guestbook updated")
    if [ "$ACTION" == "default" ]; then
        xdg-open 'https://etherpad.wikimedia.org/p/MguL8M0I7lJ9i636WLFM'
    fi

    cp current last
fi

rm current
