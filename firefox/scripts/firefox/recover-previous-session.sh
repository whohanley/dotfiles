#!/bin/sh

# Sometimes Firefox doesn't properly persist its session before quitting. Not
# sure why, but it does seem to save things as it goes in previous.jsonlz4, so
# just use that.
# cf https://support.mozilla.org/bm/questions/1364802

cp \
    ~/.mozilla/firefox/juticzxm.default-release/sessionstore.jsonlz4 \
    ~/.mozilla/firefox/juticzxm.default-release/sessionstore.bak.jsonlz4

cp \
    ~/.mozilla/firefox/juticzxm.default-release/sessionstore-backups/previous.jsonlz4 \
    ~/.mozilla/firefox/juticzxm.default-release/sessionstore.jsonlz4
