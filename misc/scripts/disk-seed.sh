#!/usr/bin/env bash

# lock zettels and agenda files into memory
vmtouch -dl \
    -I '*.org' \
    "$HOME/org/zettelkasten" \
    "$HOME/org/gtd"
