#!/bin/bash

replace() {
    # Delete the text being replaced
    for _ in 1 .. ${#1}
    do
        xdotool key BackSpace
    done

    # Insert the new text
    xdotool type "$2"
}

case "$1" in
    /shrug)
        replace "$1" '¯\_(ツ)_/¯'
        ;;
    *)
        xdotool type '??'
        ;;
esac
