#!/usr/bin/env bash

# Need to give i3 a beat to release input. see https://bbs.archlinux.org/viewtopic.php?pid=1897899#p1897899
# Keydown/keyup has to be faster than this, but the longer we sleep the more
# noticeable the lag. 0.15 feels good to me
sleep 0.15

capfile="$(mktemp --suffix .png)"

import "$capfile"

# preprocessing helps tesseract. see https://unix.stackexchange.com/a/422527/404334
convert -colorspace gray -fill white -resize 480% -sharpen 0x1 "$capfile" "${capfile}-processed.jpg"

tesseract "${capfile}-processed.jpg" stdout | xsel --clipboard --trim --input

rm "$capfile" "${capfile}-processed.jpg"
