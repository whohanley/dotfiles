#!/usr/bin/env bash

# Configure USB keyboard/mouse on connection.
#
# Adapted from https://unix.stackexchange.com/a/464647

# udev rule writes to a pipe when HID USB devices are connected. Needs to be in
# a subdir rather than directly in /tmp because https://unix.stackexchange.com/questions/687608/permission-denied-in-tmp
PIPEDIR="/tmp/hid-usb"
PIPE="${PIPEDIR}/pipe"

trap "rm -f ${PIPE} && rmdir ${PIPEDIR}" EXIT

if [[ ! -p ${PIPE} ]]; then
    mkdir -p ${PIPEDIR}
    mkfifo ${PIPE}
fi

# on write to pipe, run config script

while true
do
    if read line <${PIPE}; then
        if [[ "$line" == "connected" ]]
        then
            ${HOME}/scripts/configure-kbd-mouse.sh
            echo "HID USB devices configured"
        else
            echo "Unrecognized message from pipe: $line"
        fi
    fi
done
