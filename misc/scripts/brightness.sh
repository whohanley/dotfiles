#!/usr/bin/env bash

set -e

# Adjust brightness of internal and external screens.
# Acceptable input: 'inc' or 'dec'

if [ "$1" = 'inc' ]; then
    xbacklight -inc 5
elif [ "$1" = 'dec' ]; then
    xbacklight -dec 5
fi

# ddcutil is slow, so don't run it until finished with adjustments on the
# internal screen. Very stupid debouncing seems to work fine, but surely there's
# something in coreutils to do this more elegantly...

if [ ! -f /tmp/brightness_adj_count ]; then
    echo 0 > /tmp/brightness_adj_count
fi

waiting_count=$(cat /tmp/brightness_adj_count)
echo $((waiting_count + 1)) | tee /tmp/brightness_adj_count

sleep 2s

waiting_count=$(cat /tmp/brightness_adj_count)
echo $((waiting_count - 1)) | tee /tmp/brightness_adj_count

if [ "$(cat /tmp/brightness_adj_count)" -le 0 ]; then
    rm /tmp/brightness_adj_count
    # this might fail if the external monitor isn't connected. no problem
    ddcutil setvcp 10 "$(xbacklight -get)"
fi
