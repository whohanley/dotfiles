#!/usr/bin/env bash

# Could set up udev to do this automatically I guess, but then I'd have to learn
# how udev works

# Natural scroll direction

# a lot of mouse devices come out of xinput --list and I'm not sure which to
# change settings for. why not all of them

xinput set-prop 'ASUE120A:00 04F3:319B Touchpad' 'libinput Natural Scrolling Enabled' 1
xinput set-prop 'DLL0945:00 04F3:311C Mouse' 'libinput Natural Scrolling Enabled' 1
xinput set-prop 'PS/2 Generic Mouse' 'libinput Natural Scrolling Enabled' 1
xinput set-prop 'pointer:MOSART Semi. 2.4G Keyboard Mouse' 'libinput Natural Scrolling Enabled' 1
xinput set-prop 'pointer:MOSART Semi. 2.4G Keyboard Mouse Consumer Control' 'libinput Natural Scrolling Enabled' 1
xinput set-prop 'Newmen Tech.,LTD 2.4G Wireless Mouse' 'libinput Natural Scrolling Enabled' 1

# tap to click
xinput set-prop 'ASUE120A:00 04F3:319B Touchpad' 'libinput Tapping Enabled' 1

# Swap left ctrl and caps lock
setxkbmap -option 'ctrl:swapcaps'
