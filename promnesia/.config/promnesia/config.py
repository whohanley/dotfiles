from promnesia.common import Source
from promnesia.sources import (
    auto, # plaintext stuff
    guess, # HTTP and GitHub links
    hypothesis,
    twitter
)

'''
List of sources to use.

You can specify your own, add more sources, etc.
See https://github.com/karlicoss/promnesia#setup for more information
'''
SOURCES = [
    Source(
        auto.index,
        '/home/wohanley/org',
        name='org'
    ),

    Source(
        guess.index,
        '/home/wohanley/org',
        name='org-guess'
    ),

    Source(hypothesis.index)
]

FILTERS = [
    'mail.google.com',
    '192.168.0.',
    '10.0.0.'
]

OUTPUT_DIR = '/home/wohanley/.local/share/promnesia'
CACHE_DIR = '/home/wohanley/.cache/promnesia/'
