#!/usr/bin/env bash

THEME="dark"
if [[ $(gsettings get org.gnome.desktop.interface color-scheme) = "'prefer-light'" ]]; then
    THEME="light"
fi

feh --bg-tile --randomize "${BASH_SOURCE%/*}/images/small/${THEME}/"*
