#!/bin/bash
# Rotate the NITW constellations image to a random angle and make it the desktop
# background. Preserves the original; important because convert is lossy

# Pick a random angle of rotation from 0 through 359
ANGLE=$(shuf -i 0-359 -n 1)

# Single thread so I don't get hiccups while this is happening; doesn't matter if it takes time
MAGICK_THREAD_LIMIT=1 convert -rotate "$ANGLE" -gravity center NITW-constellations-colourized.png +repage NITW-constellations-rotated.png
MAGICK_THREAD_LIMIT=1 convert NITW-constellations-rotated.png -gravity center -crop '1920x1080+0-500' +repage NITW-constellations-cropped.png

feh --bg-center NITW-constellations-cropped.png
