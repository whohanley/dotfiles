#!/usr/bin/env bash

set -e

# Quirk: duplicity depends on the Python package b2sdk, but installing through
# pacman doesn't seem to account for this dependency. Installed both manually with
# pip in a venv
source "${BASH_SOURCE%/*}"/venv/bin/activate

source "${HOME}/.config/duplicity/.secrets"

SRC="${HOME}" # Local directory to back up
B2_DEST="b2://${B2_ACCOUNT}:${B2_KEY}@${B2_BUCKET}"
LOG_FILE="${HOME}/.local/state/duplicity/duplicity.log"

mkdir -p "${HOME}/.local/state/duplicity"
rm -f "${LOG_FILE}"

if [[ -z "$1" ]]; then
    # Perform the backup. Usually incremental, but full if >120 days old
    duplicity backup \
        --verbosity notice --log-file "${LOG_FILE}" \
        --progress --progress-rate 60 \
        --encrypt-key $GPG_KEY --use-agent \
        --full-if-older-than 120D \
        --exclude "${HOME}/Videos/**" \
        --exclude "${HOME}/Music/**" \
        --exclude "${HOME}/.local/share/Steam/**" \
        --exclude "${HOME}/.local/share/Trash/**" \
        --exclude "${HOME}/.cache/**" \
        "${SRC}" \
        "${B2_DEST}"

    # Remove files older than 180 days. This gives us 60 days of overlap between
    # backup sets.
    duplicity remove-older-than 180D \
        --force \
        --verbosity notice --log-file "${LOG_FILE}" \
        --encrypt-key $GPG_KEY --use-agent \
        "${B2_DEST}"
fi

# I don't really know what "cleanup" does, but if something's borked it
# sometimes fixes it
if [[ "$1" == "cleanup" ]]; then
    duplicity cleanup \
        --force \
        --verbosity notice --log-file "${LOG_FILE}" \
        --encrypt-key $GPG_KEY --use-agent \
        "${B2_DEST}"
fi

# Show status
duplicity collection-status \
    --encrypt-key $GPG_KEY --use-agent \
    "${B2_DEST}"
