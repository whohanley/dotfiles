#!/usr/bin/env bash

oauth2l fetch --refresh --credentials "${HOME}/.oauth2l/creds-gmail.json" --scope 'https://mail.google.com/' --cache "${HOME}/.oauth2l/cache-gmail"
