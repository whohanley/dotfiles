#!/usr/bin/env bash

# Notify of new messages in notmuch (those tagged "notify"). Will block until
# the notification is interacted with!
#
# USAGE:
#   --show-none Show a notification even if there are no new messages.
#
# Adapted from https://github.com/natmey/dotfiles/blob/master/notmuch/notmuch-notification.sh

exec > >(tee "${HOME}/.local/state/mail/notify.log") 2>&1

NOTIFICATION_ICON='/usr/share/icons/breeze/mimetypes/16/message-rfc822.svg'
SEARCH='tag:notify'

# See if we have any new messages
NOTIFY_COUNT=$(notmuch count --output=messages "$SEARCH")

if [[ $NOTIFY_COUNT -gt 0 ]]; then
  # We do have new messages. Search them and extract subject text
  TXT_SUBS=$(notmuch search --format=text --output=summary --limit='10' --sort='newest-first' "$SEARCH" | sed 's/^[^;]*; //' | sed 's/$/\n'/)

  # We're done searching, so we can remove tag:notify. Best to do it now, not
  # sure how long dunstify might block
  notmuch tag -notify -- tag:notify

  ACTION=$(dunstify --action="default,Show" -i "$NOTIFICATION_ICON" "$NOTIFY_COUNT unread messages." "$TXT_SUBS")
  if [ "$ACTION" == "default" ]; then
      emacsclient --eval '(who/open-email-inbox)'
  fi
elif [ "$1" == "--show-none" ]; then
  notify-send -t 4000 -i "$NOTIFICATION_ICON" "No unread messages."
fi

exit 0
