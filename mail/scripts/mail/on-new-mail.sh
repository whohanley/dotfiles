#!/usr/bin/env bash

set -e

BASEDIR="${BASH_SOURCE%/*}"

${BASEDIR}/sync.sh "$1"
${BASEDIR}/notify.sh &
