#!/usr/bin/env bash

set -e

exec > >(tee "${HOME}/.local/state/mail/sync.log") 2>&1

BASEDIR="${BASH_SOURCE%/*}"

if [ -z "$1" ]; then
    parallel --line-buffer "$HOME/bin/mbsync" ::: me gmail gmail-william
else
    "$HOME/bin/mbsync" "$1"
fi
printf '%s: mbsync exited with status %s\n' "$(date)" "$?"

notmuch new # see home-manager for post-new hook reading notmuch-post-new-tags
printf '%s: notmuch exited with status %s\n' "$(date)" "$?"

python "${BASEDIR}/spam-filter.py"
printf '%s: spam-filter.py exited with status %s\n' "$(date)" "$?"
