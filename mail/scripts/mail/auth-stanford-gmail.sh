#!/usr/bin/env bash

oauth2l fetch --refresh --credentials "${HOME}/.oauth2l/creds-stanford-law.json" --scope 'https://mail.google.com/' --cache "${HOME}/.oauth2l/cache-stanford-law"
