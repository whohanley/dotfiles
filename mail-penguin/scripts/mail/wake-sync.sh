#!/usr/bin/env bash

set -e

# Explicitly sourcing from /home/wohanley because we might be running from a
# system unit (in order to depend on resume). Can't wait for systemd to fix its
# user sessions so I can depend on wake in a user unit :/
export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}
export NIX_REMOTE=daemon
# include home-manager variables
. "/home/wohanley/.nix-profile/etc/profile.d/hm-session-vars.sh"

# Add to PATH:
#   ~/bin, which contains a custom isync binary
#   ~/code/go/bin, which contains oauth2l
export PATH="$HOME/bin:$HOME/code/go/bin:$PATH"

/home/wohanley/scripts/mail/on-new-mail.sh
