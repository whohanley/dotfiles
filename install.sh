#!/usr/bin/env bash

# What to do from the project root to set things up

home-manager switch

stow -t ~ autokey autorandr backup bash battery blender clojure darkman dunst dunst emacs firefox git godot guestbook home-manager i3 kitty mail misc msmtp-queue picom redshift rofi root vim wallpaper xsettings zsh

# ORB needs this and it isn't conveniently packaged anywhere
gem install --user-install anystyle-cli

systemctl --user enable backup guestbook

# for pigeon
stow -t ~ pigeon
stow -t ~ mail-pigeon

# for penguin
stow -t ~ penguin
stow -t ~ mail-penguin

# need root for some stuff
sudo su
cd root

stow -t / autologin i3 mail

# penguin-specific
cd penguin
stow -t / peripheral
